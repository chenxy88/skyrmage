import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import os
import math
import re
from scipy import interpolate
from dataclasses import dataclass
from copy import deepcopy


def listdir_filtered (path: str, extension: str):

	filename_list = os.listdir(path)
	filename_list_filtered = []

	# filter based on criteria
	for filename in filename_list:
		if filename.endswith(extension):
			filename_list_filtered.append(filename)

	return filename_list_filtered

@dataclass
class InputOptions:
	data_path: str = r'C:\Users\Xiaoye\OneDrive\ASTAR-Skyrmions\Projects\FMR\Simulations\Ad hoc'
	forc_path: str = None
	labels: str = None
	experimental_results_path_filename: str = ''
	title: str = ''
	results_path: str = ''
	input_file_ext: str = '.txt'
	legend: bool = True
	time_column_num: int = 0
	field_column_num: int = 6
	mag_column_num: int = 2

def analysis_main():

	# This analysis routine read in FMR simulations

	in_opt = InputOptions()

	data_file_list = listdir_filtered(in_opt.data_path, in_opt.input_file_ext)

	# loop thru all data files
	for filename in data_file_list:

		data = np.genfromtxt(os.path.join(in_opt.data_path, filename), delimiter='\t', skip_header=2, comments='#', invalid_raise=False)
		# data is a 2d ndarray

		# extract the field values
		field_arr = data[:, in_opt.field_column_num]
		# offset the field_arr by one
		field_arr_offset = np.concatenate([field_arr[1:],[np.inf]])
		# find the positions where fields changed
		split_positions = (np.abs(field_arr - field_arr_offset) > 1e-9)
		split_positions = np.arange(0, len(split_positions))[split_positions]
		split_positions = np.insert(split_positions, 0, -1)

		for i in range(0, len(split_positions)-1):
			# split up the data file accordingly
			data_part = data[split_positions[i]+1:split_positions[i+1], :]
			field_val = round(field_arr[split_positions[i]+1] *1e3)
			# write to output
			np.savetxt(os.path.join(in_opt.data_path, 'FMR_%d_mT.dat'%field_val), data_part, fmt='%.10E', delimiter='\t', header='')



# run the main function
if __name__ == '__main__':
	analysis_main()
	# test()
