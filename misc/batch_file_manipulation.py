import os, shutil, re

# This is a simple script to copy and rename ovf files at different Hr to a common location

# ---------------Input parameters---------------

local_mode = True
source_dir = r'C:\Users\Xiaoye\Tmp\11dFORC_st60'
destination_dir = r'C:\Users\Xiaoye\Tmp\11dFORC_st60\results'
source_folders_regex = r'Hr_(\+?-?\d+)\s*mT'
items_regex = r'sliced_mag_relaxed.+_0mT\.ovf'
out_folder_regex = r'\.out'
table_file_str = 'table.txt'


# ---------------End of Input parameters---------------

# to gather files in NSCC and local cluster
def main():

	# find all folders in source_dir that has 'Hr'
	folder_list = [folder for folder in os.listdir(source_dir) if (os.path.isdir(os.path.join(source_dir, folder))
																   and re.search(source_folders_regex,  folder))]

	folder_list.sort(key = lambda name : int(re.search(source_folders_regex, name).group(1)))

	out_folder = None

	for ind,folder in enumerate(folder_list):

		curr_path = os.path.join(source_dir, folder)

		# find all files in a single Hr folder
		filename_list = os.listdir(curr_path)

		if local_mode:

			# performing this on local cluster
			out_folder = next((name for name in filename_list if re.search(out_folder_regex, name)), None)

			if out_folder is not None:

				curr_path = os.path.join(source_dir, folder, out_folder)

				# find all files in a single Hr folder
				filename_list = os.listdir(curr_path)

		# number in front
		# prefix = '{0:03d}_'.format(ind)

		# find zero field ovf file
		zf_ovf = next((name for name in filename_list if re.search(items_regex, name)), None)

		if zf_ovf is not None:
			# copy file to destination
			shutil.copyfile(os.path.join(curr_path, zf_ovf), os.path.join(destination_dir, folder + '_' + zf_ovf))

		if local_mode and out_folder is not None:

			# copy the table file also
			shutil.copyfile(os.path.join(curr_path, table_file_str), os.path.join(destination_dir, folder + '_' + table_file_str))


# run the main function
if __name__ == '__main__':
	main()

