import numpy as np
import matplotlib.pyplot as plt
import os
from tkinter import filedialog

from core.data_parser import DataParser
from core.plotter import Plotter
from core.util import Vector, ParamsHL


def process_main():

	files = filedialog.askopenfilenames(title='Select .ovf files')
	num_files = len(files)
	grid_spacing = 0.004 # in um, usually 0.004 um
	z_layer_number = 0
	
	for ind, pathandfilename in enumerate(files):
		# progress
		print('Processing... %0.1f%% complete.  Loading %s.' % (float(ind / num_files) * 100, pathandfilename))

		data = DataParser.import_ovf(path_and_filename = pathandfilename, z_layer_number=z_layer_number, grid_spacing=grid_spacing)
		# header_text = "ovf image"
		# data.metadata.label = header_text
		# data.metadata.axes_labels = Vector('x (um)', 'y (um)')
		# Plotter.pcolormesh(data=data, equal_aspect=True, vlim=ParamsHL(1, -1))
		fig = plt.pcolormesh(data.data, cmap='viridis')
		plt.gca().set_aspect('equal')
		plt.axis('off')
		fig.axes.get_xaxis().set_visible(False)
		fig.axes.get_yaxis().set_visible(False)

		image_filename = os.path.splitext(pathandfilename)[0] + '.png'
		plt.savefig(image_filename, dpi=300,bbox_inches='tight', pad_inches = 0)
		plt.close()

# run the main function
if __name__ == '__main__':
	process_main()  # test()