#!/usr/bin/env python
import sys
import os
import numpy as np

sys.path = ['..']+sys.path
from pyvtk import *

# This code generates VTK files for various magnetic orders for visualisation using ParaView

def GenerateMagPhase(nx, ny, nz, theta, phi, wavelength, start_phase = 0):

	"""
	This functions generates phase of one planewave of arbitrary wavevector.
	It uses loops for convenience. If performance is crucial, it can be compiled with cython.

	nx, ny, nz: number of units grid points. Uniform grid is assumed.
	theta: wavevector angle w.r.t. z, in degrees from 0 to 180
	phi: wavevector in-plane angle w.r.t x, in degrees from 0 360
	wavelength: in units of grid spacing
	start_phase: phase at the origin, in degrees from 0 360

	"""

	phase_output = np.ndarray(shape = (nx,ny,nz), dtype=np.double)
	wavevector = 2*np.pi/wavelength

	#convert from degrees to radians
	theta = theta / 180 * np.pi
	phi = phi / 180 * np.pi
	start_phase = start_phase/ 180 * np.pi

	# set phi to zero for zero theta
	if theta == 0:
		phi = 0

	for x in range(0,nx):
		for y in range(0,ny):
			for z in range(0, nz):
				phase = wavevector*(x*np.sin(theta)*np.cos(phi) + y*np.sin(theta)*np.sin(phi) + z*np.cos(theta))+start_phase
				phase_output[x, y, z] = phase

	# return as 1D list of 3-list
	return phase_output


def GenerateMagVectors(phase_arr, nx, ny, nz, theta, phi, bloch_texture=True):

	"""
	This functions generates unit magnetisation vectors based on the phase and spin rotation direction
	It uses loops for convenience. If performance is crucial, it can be compiled with cython.

	nx, ny, nz: number of units grid points. Uniform grid is assumed.
	theta: wavevector angle w.r.t. z, in degrees from 0 to 180
	phi: wavevector in-plane angle w.r.t x, in degrees from 0 360
	bloch_texture: Bloch or Neel texture, which defines angle between wavevector and the vector defining the plane of spin rotation

	"""

	vectors_output = np.ndarray(shape = (nx,ny,nz), dtype=object)

	#convert from degrees to radians
	theta = theta / 180 * np.pi
	phi = phi / 180 * np.pi

	# set phi to zero for zero theta
	if theta == 0:
		phi = 0

	bloch = int(bloch_texture)
	neel = int(not bloch_texture)
	# calculate and coefficients to rotate plane to align with k wavevector
	alpha_x = np.cos(phi)*(bloch*np.cos(theta) - neel*np.sin(theta))
	beta_x = -np.sin(phi)
	alpha_y = np.sin(phi)*(bloch*np.cos(theta) - neel*np.sin(theta))
	beta_y = np.cos(phi)
	alpha_z = -(neel*np.cos(theta) + bloch*np.sin(theta))

	for x in range(0,nx):
		for y in range(0,ny):
			for z in range(0, nz):
				phase = phase_arr[x,y,z]
				cos_phase = np.cos(phase) # times alpha coefficient
				sin_phase = np.sin(phase) # times beta coefficient
				# this includes rotation to wavevector direction
				vectors_output[x, y, z] = [alpha_x*cos_phase + beta_x*sin_phase, alpha_y*cos_phase + beta_y*sin_phase, alpha_z*cos_phase]

	# return as 1D list of 3-list
	return vectors_output.ravel(order='F').tolist()
	
def generate_texture_main():

	# define parameter
	path = ''
	name = 'AF domain wall'
	nx = 12
	ny = 4
	nz = 3
	theta = 90
	phi = 0
	wavelength = 10
	start_phase = 90

	# Example: Sum of two wavevectors
	# phase = GenerateMagPhase(nx, ny, nz, theta, phi, wavelength, start_phase)+GenerateMagPhase(nx, ny, nz, 0, 0, 2, 0)
	# phase_scalar = np.sin(phase).ravel(order='F').tolist()
	# vectors = GenerateMagVectors(phase, nx, ny, nz, theta, phi, bloch_texture=False)
	# vtk = VtkData(StructuredPoints([nx,ny,nz]),PointData(Scalars(phase_scalar),Vectors(vectors)))
	# vtk.tofile(os.path.join(path,name))

	# Example: Domain wall
	nx1 = 6
	nx2 = nx - nx1
	phase1 = GenerateMagPhase(nx1, ny, nz, theta, phi, wavelength=np.sqrt(2), start_phase=0)
	phase2 = GenerateMagPhase(nx2, ny, nz, theta, phi, wavelength=np.sqrt(2), start_phase=90)
	phase = np.concatenate((phase1, phase2), axis=0)
	phase_scalar1 = np.sin(phase1-0.5)
	phase_scalar2 = np.sin(phase2)
	phase_scalar = np.concatenate((phase_scalar1, phase_scalar2), axis=0).ravel(order='F').tolist()
	vectors = GenerateMagVectors(phase, nx, ny, nz, theta, phi, bloch_texture=False)
	vtk = VtkData(StructuredPoints([nx, ny, nz]), PointData(Scalars(phase_scalar), Vectors(vectors)))
	vtk.tofile(os.path.join(path, name))

# run the main function
if __name__ == '__main__':
	generate_texture_main()
	# test()