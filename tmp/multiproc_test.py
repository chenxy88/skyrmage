from multiprocessing import Pool
import random
import time, os

def f(x):
	print("%g started"%x)
	time.sleep(random.randrange(0,10))
	print("%g ended" % x)
	return x*x

if __name__ == '__main__':
	print(os.path.abspath(os.path.join(os.getcwd(),'..','misc')))
	with Pool(processes=4) as p:
		result = p.map(f, range(0,10))
		print("Result:")
		print(result)