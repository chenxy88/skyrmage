import numpy as np
import matplotlib.pyplot as plt

from core.data_parser import DataParser
from core.plotter import Plotter
from core.util import Vector
from core.shuju import Shuju

def import_data():
	# import data from hardcoded path and filename
	path = r'C:\Users\Xiaoye\OneDrive\DSI-Skyrmions\Projects\LTEM\agm_data\20 Feb 2018 t20t t21a AGM'
	filename='20Feb2018 t21a OP.txt'
	filename2 = '20Feb2018 t20t OP.txt'

	my_data = DataParser.import_plaintxt(path=path, filename=filename, delimiter=',',skip_header=88, process_data_fn=lambda x: x[:,3], process_axes_fn=lambda x:x[:,2], skip_footer=3)
	my_data2 = DataParser.import_plaintxt(path=path, filename=filename2, delimiter=',',skip_header=88, process_data_fn=lambda x: x[:,3], process_axes_fn=lambda x:x[:,2], skip_footer=3)

	my_data.metadata.axes_labels = Vector('H (Oe)','M (emu)')
	my_data.metadata.label = 't21a'

	my_data2.metadata.axes_labels = Vector('H (Oe)','M (emu)')
	my_data2.metadata.label = 't20t'

	return my_data, my_data2

def plot_togther(data_list):

	for my_data in data_list:
		Plotter.plot(my_data)

	plt.legend()

def plot_as_subplots(data_list):
	n = len(data_list)
	
	for ind, my_data in enumerate(data_list):
		plt.subplot(n,1,ind+1)
		Plotter.plot(my_data)
		plt.legend()

def plot_2d():
	x = np.arange(0,10,0.1)
	y = np.arange(0,20,0.1)
	z = np.outer(x,y)

	data = Shuju(z)
	data.metadata.axes = Vector(x,y)
	data.metadata.label = 'z'
	data.metadata.axes_labels = Vector('x','y')

	Plotter2D.pcolormesh(data)

def main():
	#my_data, my_data2 = import_data()

	#plt.figure(1)
	#plot_togther([my_data, my_data2])
	#plt.figure(2)
	#plot_as_subplots([my_data, my_data2])

	plot_2d()

	plt.show()



# run the main function
if __name__ == '__main__':
	main()
	# test()
