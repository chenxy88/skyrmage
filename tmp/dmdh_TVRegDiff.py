from algorithms.tvregdiff import TVRegDiff
from core.data_parser import DataParser
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

#  CONSTANTS
experimental_results_path_filename_load = 'C:\\Users\\Xiaoye\\OneDrive\\ASTAR-Skyrmions\\Projects\\FORC\\preliminary MH\\11d_experimental_MH.txt'
plot_save = 'C:\\Users\\Xiaoye\\OneDrive\\ASTAR-Skyrmions\\Projects\\FORC\\preliminary MH\\11d_experimental_MH_dMdH'



def main():

	# import data
	data = DataParser.import_plaintxt(path_and_filename=experimental_results_path_filename_load, delimiter='\t', skip_header=0).data

	# simple method
	dmdh_y_np = np.gradient(data[:, 1], data[:, 0])

	# grid spacing
	dx = -(np.max(data[:, 0]) - np.min(data[:, 0])) / (len(data[:, 0]) - 1)
	dmdh_y_TVRegDiff = TVRegDiff(data[:, 1], 50, 1e-4, dx=dx, ep=1e-2, scale='large', plotflag=0, diagflag=1)

	m_H_Hr = np.rot90(np.vstack((dmdh_y_TVRegDiff, data[:,0])), 3)

	# prepare colormap
	cmap = matplotlib.cm.get_cmap('viridis')  # nipy_spectral')

	plt.style.use(r'C:\Users\Xiaoye\OneDrive\ASTAR-Skyrmions\Projects\skyrmage\misc\default.mplstyle')

	plt.plot(data[:, 0], dmdh_y_np, 'o-', color=cmap(0.1), label='np.gradient')
	plt.plot(data[:, 0], dmdh_y_TVRegDiff, '^-', color=cmap(0.5), label='TVRegDiff')

	plt.xlabel('External field (T)')
	plt.ylabel('dM/dH')

	plt.savefig(plot_save+'_1e-4')

	np.savetxt(plot_save+'.txt', m_H_Hr, fmt='%.5f', delimiter='\t')

# run the main function
if __name__ == '__main__':
	main()