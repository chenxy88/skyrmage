import numpy as np
import matplotlib.pyplot as plt
import os
import pickle
from scipy.interpolate import interp1d
from dataclasses import dataclass

from core.plotter import Plotter
from core.util import Vector, ParamsHL, save_obj, UI_load_json_file
from core.shuju import Shuju


@dataclass
class InputOptions:
	path: str = None
	file_extension: str = None
	grid_spacing: float = None
	title: str = ''
	output_path: str = ''
	output_file_extension: str = ''
	show_intermediate_steps: bool = False
	coercive_field: float = 0
	invert_contrast: bool = False
	divide_background_blur_radius: int = 10
	open_close_rep: int = 2
	median_filter_radius: int = 3
	local_threshold: bool = False
	manual_threshold_percentile: float = None
	global_thresh_finetune_list: list = None
	counts_threshold: int = 100
	max_domain_width: float = 300


def main():
	# --- CHOOSE INPUT OPTION --- #
	input_dict_list = UI_load_json_file()

	# "MTXM_26aa.json"   "MTXM_26t2.json"  "MTXM_26j1.json" "MTXM_26m1.json" for input
	if not input_dict_list:
		# user chose to cancel
		return

	# load and plot average domain width
	f1 = plt.figure(figsize=(15,10))

	title = ['Pt(1) Co(1) Pt(1)','Ir(1) Co(1) Pt(1)','Ir(1) Fe(0.2) Co(0.8) Pt(1)','Ir(1) Fe(0.3) Co(0.7) Pt(1)']
	# --- BIG OUTSIDE LOOP THRU LIST OF INPUT JSON FILES --- #
	for ind, input_dict in enumerate(input_dict_list):

		in_opt = InputOptions(**input_dict)

		# load and plot average domain width
		obj_input_path = os.path.join(in_opt.output_path, in_opt.title+' mean_domain_width')
		# load pickle
		with open(obj_input_path, 'rb') as pickleFile:
			data1 = pickle.load(pickleFile)
		data1.metadata.label = title[ind]

		plt.figure(f1.number)
		Plotter.plot(data1,'.-', markersize=12)

	plt.figure(f1.number)
	plt.title('Average domain width')
	plt.legend(loc='upper right')
	plt.savefig(os.path.join(r'C:\Users\Xiaoye\OneDrive\ASTAR-Skyrmions\Projects\Textures\Analysis\Domain width\results'))


# run the main function
if __name__ == '__main__':
	main()  # test()
