import numpy as np
import matplotlib.pyplot as plt
import os
import re
from copy import copy
from scipy.stats import gaussian_kde
from copy import deepcopy

from core.data_parser import DataParser
from core.plotter import Plotter
from core.util import Vector, ParamsHL
from core.shuju import Shuju


def import_data(path):

	filename_list = os.listdir(path)

	filename_list_filtered = []
	imported_list = []
	data_list = []
	field_arr = []
	extreme_vals = ParamsHL(-np.inf,np.inf)

	# filter base on criteria
	for filename in filename_list:
		if filename.endswith('.csv'):
			filename_list_filtered.append(filename)

	# loop through all files in list
	for filename in filename_list_filtered:

		# get field value from filename
		field_val = int(re.search('(\+?-?\d+)\s*Oe', filename).group(1))
		field_arr.append(field_val)

		tmp_data = DataParser.import_plaintxt(path=path, filename=filename, delimiter=',', skip_header=1,
											  process_data_fn=lambda x: x[:, 1])

		tmp_data.data = np.log10(tmp_data.data*1e6/30) #30nm is the average width

		extreme_vals.update_extreme_value(np.max(tmp_data.data))
		extreme_vals.update_extreme_value(np.min(tmp_data.data))

		imported_list.append(tmp_data)

	for log_data in imported_list:
		x_axis = np.linspace(extreme_vals.low, extreme_vals.high, 100)
		hist_kernel = gaussian_kde(log_data.data, bw_method=0.25)
		hist_shuju = Shuju(hist_kernel(x_axis))
		hist_shuju.metadata.axes = x_axis
		hist_shuju.metadata.axes_labels = Vector('log(domain length(nm))', 'Distribution density')

		data_list.append(deepcopy(hist_shuju))

		# remove extension for label
		# file_label, file_extension = os.path.splitext(filename)
		# tmp_data.metadata.label = file_label
		#
		# # copy to mydata
		# data_list.append(copy(tmp_data))

	return data_list, field_arr


def plot_togther(data_list):
	offset = 0
	for my_data in data_list:
		plot_data = deepcopy(my_data)
		offset += np.max(plot_data.data)/5
		plot_data.data += offset
		Plotter.plot(plot_data)

	plt.legend()


def plot_as_subplots(data_list):
	n = len(data_list)

	for ind, my_data in enumerate(data_list):
		plt.subplot(n, 1, ind + 1)
		Plotter.plot(my_data)
		plt.legend()


def plot_in_separate_figs(data_list):
	n = len(data_list)

	for ind, my_data in enumerate(data_list):
		plt.figure(ind)
		Plotter.plot(my_data)
		plt.legend()


def plot_main():

	# import data from hardcoded path and filename
	path = r'C:\Users\Xiaoye\Downloads'
	name = 'cycle 4'
	path = os.path.join(path,name)

	data_list, field_arr = import_data(path)
	# plot_togther(data_list)

	# plot combined pseudocolour plot
	combined_data = Shuju.shuju1d_list_to_2d(data_list, y_axis=field_arr)
	# combined_data.data = np.log10(combined_data.data)
	combined_data.metadata.axes_labels = Vector('Magnetic field (Oe)','log(domain length(nm))',  'Distribution density (A.U.)')
	combined_data.metadata.label = name

	# rotate the data
	combined_data.data = np.rot90(combined_data.data)
	tmp_x = np.rot90(combined_data.metadata.axes.x)
	combined_data.metadata.axes.x = np.rot90(combined_data.metadata.axes.y)
	combined_data.metadata.axes.y = tmp_x

	Plotter.pcolormesh(data=combined_data, equal_aspect=False)

	plt.savefig(os.path.join(path, name+' domain length distribution'), dpi=300)


# run the main function
if __name__ == '__main__':
	plot_main()  # test()
