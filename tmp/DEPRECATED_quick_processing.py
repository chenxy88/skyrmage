import numpy as np
import matplotlib.pyplot as plt
import os as os
from scipy.interpolate import interp1d

from core.data_parser import DataParser
from core.plotter import Plotter
from core.shuju import MetaFile, Shuju
from core.util import Vector, ParamsHL


def import_linecuts(root_path='', filename=''):

	metafile = MetaFile()
	metafile.set_attrs(path=root_path, filename=filename, delimiter=' ', comments='#', skip_header=3)
	data_parser = DataParser(metafile)
	data_tmp = data_parser.load_data()
	# remove nan entries due to repeated delimiter
	data_tmp.data = data_tmp.data[:, ~np.isnan(data_tmp.data[0, :])]

	data_shape = data_tmp.data.shape
	assert data_shape[1]%2==0

	num_cols = data_shape[1]
	# 2 columns per dataset
	num_datasets = int(num_cols/2)
	# convert from m to nm
	x_columns = data_tmp.data[:,range(0,num_cols,2)]
	y_columns = data_tmp.data[:,range(1,num_cols,2)]

	return data_shape, num_datasets, num_cols, x_columns, y_columns

def sk_rotation():
	r = np.linspace(0,10,1000)
	r2 = r**2
	w = 2
	w2 = w**2
	mz = 2*(np.exp(-r2/w2)-0.5)
	mx = (1-np.abs(mz))
	theta = np.arctan(mz/mx)/np.pi*180
	plotter = Plotter(legend=True, axeslim=Vector(),
								axes_label=Vector('Distance (A.U.)', 'Angle (deg)'))
	tmp_shuju = Shuju(theta)
	tmp_shuju.metadata.set_attrs(axes=r)
	plotter.plot(tmp_shuju)
	plt.show()

def subplot_gwy_linecuts_sim():
	root_path = 'C:\\Users\\Xiaoye\\OneDrive\\DSI-Skyrmions\\Projects\\LTEM\\Analysis\LTEM_sim_Neel_n_Bloch'
	bloch_files_ch1 = ['LTEM_sim_Neel_n_Bloch_st_01_loop_00_0deg_mat512_linecut.txt', 'LTEM_sim_Neel_n_Bloch_st_01_loop_00_10deg_mat512_linecut.txt', 'LTEM_sim_Neel_n_Bloch_st_01_loop_00_20deg_mat512_linecut.txt']
	bloch_files_ch2 = ['LTEM_sim_Neel_n_Bloch_st_02_loop_00_0deg_mat512_linecut.txt',
					   'LTEM_sim_Neel_n_Bloch_st_02_loop_00_10deg_mat512_linecut.txt',
					   'LTEM_sim_Neel_n_Bloch_st_02_loop_00_20deg_mat512_linecut.txt']
	neel_files = ['LTEM_sim_Neel_n_Bloch_st_01_loop_01_0deg_mat512_linecut.txt','LTEM_sim_Neel_n_Bloch_st_01_loop_01_10deg_mat512_linecut.txt','LTEM_sim_Neel_n_Bloch_st_01_loop_01_20deg_mat512_linecut.txt']

	files = [bloch_files_ch1, bloch_files_ch2, neel_files]
	labels = [['0 deg','10 deg','20 deg'],['0 deg','10 deg','20 deg'],['0 deg','10 deg','20 deg']]
	# titles = ['Bloch', 'Bloch', 'Néel']
	# dataset_subplot = Plotter1D(legend=True, axeslim=Vector())
	# dataset_subplot.subplot_axes_labels(Vector('Distance (nm)', 'LTEM intensity (A.U.)'), fontsize=12, labelPos= Vector(Vector(0.5,  0.02),Vector(0.02, 0.5)))
	# dataset_subplot.share_ax = Vector('col','row')
	subplot_nrow=1
	subplot_ncol=3
	# dataset_subplot.subplot_init(Vector(subplot_ncol,subplot_nrow))
	for i in range(0,subplot_ncol):
		# dataset_subplot.subplot_set_axes(Vector(i))
		dataset_subplot = Plotter(legend=True, axeslim=Vector(), axes_label=Vector('Distance (nm)', 'LTEM intensity (A.U.)'))
		# dataset_subplot.title = titles[i]
		for ind,filename in enumerate(files[i]):
			data_shape, num_datasets, num_cols, x_columns, y_columns = import_linecuts(root_path=root_path, filename=filename)
			# make y-axis of order unity
			y_columns = y_columns*1e8
			tmp_shuju = Shuju(y_columns)
			# invert x-axis, to match chosen direction for Neel skyrmions
			tmp_shuju.metadata.set_attrs(axes=np.flip(x_columns,0))
			dataset_subplot.plot(tmp_shuju, label=labels[i][ind])

	plt.show()

def subplot_gwy_linecuts():
	root_path = 'C:\\Users\\Xiaoye\\OneDrive\\DSI-Skyrmions\\Projects\\LTEM\\Analysis\\2017-03-02 21a focus'
	# selected_filename = [file for file in all_filenames if file.endswith('.gwylc')]
	filename = '24 02 -100um.txt'
	data_shape, num_datasets, num_cols, x_columns, y_columns = import_linecuts(root_path=root_path, filename=filename)

	dataset_subplot = Plotter(legend=False, axeslim=Vector(), fig_dim = Vector(3, 2.8))
	dataset_subplot.subplot_axes_labels(Vector('Position (nm)', ''), fontsize=12, labelPos= Vector(Vector(0.55,  0.02),Vector(0.02, 0.5)))

	n_sets = [3,3]
	subplot_nrow=1
	subplot_ncol=2
	dataset_subplot.subplot_init(Vector(subplot_ncol, subplot_nrow))
	ind = 0
	for i in range(0,subplot_ncol):
		for j in range(0,n_sets[i]):
			dataset_subplot.subplot_set_axes(Vector(i,0))
			tmp_y_col = y_columns[:, ind]
			tmp_y_col = tmp_y_col/np.max(np.abs(tmp_y_col))
			tmp_shuju = Shuju(tmp_y_col)
			tmp_shuju.metadata.set_attrs(axes=x_columns[:, ind], label=str(ind))
			dataset_subplot.plot(tmp_shuju)
			ind = ind+1
	plt.show()

def average_gwy_linecuts():
	"""
	Given a series of linecut data, import them, interpolate to common basis, average them, then plot
	:return:
	"""
	root_path = 'C:\\Users\\Xiaoye\\OneDrive\\DSI-Skyrmions\\Projects\\LTEM\\Analysis\\2017-08-28 20s tilt defocus'
	# selected_filename = [file for file in all_filenames if file.endswith('.gwylc')]
	filename = '02 -200um_line_cuts_2.txt'

	data_shape, num_datasets, num_cols, x_columns, y_columns = import_linecuts(root_path=root_path, filename=filename)

	# use the first column as the new basis
	new_basis_len = 200
	x_basis = np.linspace(0,np.min(x_columns[-1,:]),new_basis_len)
	y_interp = np.zeros(new_basis_len)

	dataset_plot = Plotter(axes_label=Vector('Position (nm)', ''),  legend=False, axeslim=Vector(),fig_dim = Vector(3, 3))

	for i in range(0,num_datasets):
		tmp_y_col = y_columns[:, i]
		interp_fn = interp1d(x_columns[:, i], tmp_y_col,fill_value='extrapolate')
		tmp_y = interp_fn(x_basis)
		y_interp = y_interp + tmp_y
		tmp_y_col = tmp_y_col/np.max(np.abs(tmp_y_col))
		tmp_shuju = Shuju(tmp_y_col)
		tmp_shuju.metadata.set_attrs(axes=x_columns[:,i], label=str(i))
		dataset_plot.plot(tmp_shuju, label='cut'+str(i))

	y_interp = y_interp/num_datasets
	# normalise
	y_interp = y_interp/np.max(np.abs(y_interp))
	tmp_shuju = Shuju(y_interp)
	tmp_shuju.metadata.set_attrs(axes=x_basis, label='Averaged')


	dataset_plot = Plotter(axes_label=Vector('Position (nm)', ''), legend=False, axeslim=Vector(), fig_dim = Vector(3, 3))
	dataset_plot.plot(tmp_shuju, label='Averaged')
	# dataset_plot.save('C:\\Users\\Xiaoye\\OneDrive\\DSI-Skyrmions\\Projects\\LTEM\Analysis\\2017-08-28 20s tilt defocus\\Linecuts2_averaged.png')

	plt.show()

	print('Hello!')

def show_all_linecuts():
	# in 1/um
	freq_lims = ParamsHL(20,3)
	root_path = 'C:\\Users\\Xiaoye\\OneDrive\\DSI-Skyrmions\\Projects\\LTEM\\Analysis\\2017-12-27 13d recalibrate'
	filenames = [file for file in os.listdir(root_path) if file.endswith('SAC.txt')]
	dataset_plotter = Plotter(legend=True, axeslim=Vector(), fig_dim=Vector(3, 2.8), axes_label=Vector('Spatial frequency (1/um)','Intensity (A.U.)'))
	for file in filenames:
		data_shape, num_datasets, num_cols, x_columns, y_columns = import_linecuts(root_path=root_path, filename=file)
		# convert from m to um
		x_columns = x_columns*1e6
		y_columns = y_columns[(x_columns > 1/freq_lims.high) & (x_columns < 1/freq_lims.low)]
		x_columns = x_columns[(x_columns > 1/freq_lims.high) & (x_columns < 1/freq_lims.low)]
		x_columns = 1/x_columns
		# y_columns = np.abs(y_columns)
		y_columns = y_columns/np.max(np.abs(y_columns))
		tmp_shuju = Shuju(y_columns)
		tmp_shuju.metadata.set_attrs(axes=x_columns, label=file)
		dataset_plotter.plot(tmp_shuju, label=file)

	filenames = [file for file in os.listdir(root_path) if file.endswith('FFT.txt')]
	# dataset_plotter = Plotter1D(legend=True, axeslim=Vector(), fig_dim=Vector(3, 2.8), axes_label=Vector('Spatial frequency (1/um)','Intensity (A.U.)'))
	dataset_plotter = Plotter(legend=True, axeslim=Vector(), fig_dim=Vector(3, 2.8), axes_label=Vector('Spatial frequency (1/um)','Intensity (A.U.)'))
	for file in filenames:
		data_shape, num_datasets, num_cols, x_columns, y_columns = import_linecuts(root_path=root_path, filename=file)
		# convert from 1/m to 1/um
		x_columns = x_columns*1e-6
		y_columns = y_columns[(x_columns < freq_lims.high) & (x_columns > freq_lims.low)]
		x_columns = x_columns[(x_columns < freq_lims.high) & (x_columns > freq_lims.low)]
		y_columns = y_columns / np.max(np.abs(y_columns))
		tmp_shuju = Shuju(y_columns)
		tmp_shuju.metadata.set_attrs(axes=x_columns, label=file)
		dataset_plotter.plot(tmp_shuju, label=file)

	plt.show()

def main():
	# average_gwy_linecuts()
	# subplot_gwy_linecuts()
	# subplot_gwy_linecuts_sim()
	# sk_rotation()
	show_all_linecuts()

# run the main function
if __name__ == '__main__':
	main()
	# test()
