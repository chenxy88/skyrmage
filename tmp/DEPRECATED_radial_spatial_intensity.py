from matplotlib import pyplot as plt

from core.shuju import Shuju
from image.DEPRECATED_microscopy_analysis import AnalyseLTEMExp
from core.util import ParamsHL, Vector
from core.plotter import Plotter


def ltem_mfm_compare_periodicity():
	ltem_analysis = AnalyseLTEMExp()

	ltem_analysis.flow_ctrl = dict(fft_subplot=0, real_space_subplot=0, accept_zero_tilt=0, exclude_tilt=0, imag_crop=0, export_image=0, real_space_plot=0, do_fft=1, fft_filter=0, fft_plot=0, radial_plot=1, plot_magnification=1, single_test_file=0)
	ltem_analysis.root_path='C:\\Users\\Xiaoye\\Tmp\\LTEM\\2017-12-01 13d defocus at zero field 300kV 9000x'
	ltem_analysis.magn_defocus = {200:0.0022614, 150:0.00230292,100:0.0023323,50:0.00238294,0:0.00241913, -50:0.0024684,-100:0.00250365,-150: 0.002523, -200:0.00254756}
	ltem_analysis.tilt_axis_dict = {-200:-40.89, -150:-40.5, -100:-40.34, -50:-39.95, 50:-39.4,  100:-38.56, 150:-38.02, 200:-37.81}
	ltem_analysis.radial_profile_arr_len=400
	ltem_analysis.freq_max_min = ParamsHL(high=30, low=2)
	ltem_analysis.plotter_radial_profile = Plotter(title='Radial profile', axes_label=Vector('Spatial frequency (1/um)', 'Power spectrum (A.U.)'), legend=True, axeslim=Vector())
	ltem_analysis.abs_defocus_short_list = [150]

	ltem_analysis = AnalyseLTEMExp()

	ltem_analysis.flow_ctrl = dict(fft_subplot=0, real_space_subplot=0, accept_zero_tilt=0, exclude_tilt=0, imag_crop=0, export_image=0, real_space_plot=0, do_fft=1, fft_filter=0, fft_plot=0, radial_plot=1, plot_all_files=1, plot_magnification=0, single_test_file=0)
	ltem_analysis.root_path='C:\\Users\\Xiaoye\\Tmp\\LTEM\\2017-12-27 13d recalibrate'
	ltem_analysis.magn_defocus = {-200:0.0025175, -150:0.002456098, -100:0.002397619, -50:0.00234186, 50:0.002227876, 100:0.00218913, 150:0.002142553, 200:0.002097917}
	ltem_analysis.tilt_axis_dict = {-200:-40, -150:-40, -100:-40, -50:-40, 50:-40,  100:-40, 150:-40, 200:-40}
	ltem_analysis.radial_profile_arr_len=400
	ltem_analysis.freq_max_min = ParamsHL(high=30, low=2)
	ltem_analysis.plotter_radial_profile = Plotter(title='Radial profile', axes_label=Vector('Spatial frequency (1/um)', 'Power spectrum (A.U.)'), legend=True, axeslim=Vector())
	ltem_analysis.abs_defocus_short_list = [150]

	ltem_analysis.analyse()
	plt.show()

def ltem_defocus_magn_compare_plot():
	magn1 = {200:0.0022614, 150:0.00230292,100:0.0023323,50:0.00238294,0:0.00241913, -50:0.0024684,-100:0.00250365,-150: 0.002523, -200:0.00254756}
	# divide magn1 by 2, since this is with pixel binning
	multip_factor = [0.5, 1, 1]
	magn2a = {-150:0.00136527, 0:0.00130664, 150:0.00128115}
	magn2b = {-150:0.00129510, 0:0.00126930, 150:0.00124183}
	magn_list = [magn1, magn2a, magn2b]
	label_list = ['2017-12-01','2017-12-27 (TR,BL)', '2017-12-27 (TL,BR)']
	plotter = Plotter(axes_label=Vector('Defocus (um)','Magnification (um/px)'), legend=True)
	for ind, magn_defocus in enumerate(magn_list):
		x = [float(i) for i in magn_defocus]
		y = [magn_defocus[i]*multip_factor[ind] for i in magn_defocus]
		yerr = [yi*0.01 for yi in y]
		tmp_Shuju = Shuju(y)
		tmp_Shuju.metadata.axes = x
		plotter.get_axes()
		plotter.axes.errorbar(x,y, fmt='.-',yerr=yerr,label=label_list[ind])
		plotter.fig_dim = Vector(5, 4)
		plotter.config_fig()

	plt.show()

def test():

	print('whats my data?')

# run the main function
if __name__ == '__main__':
	# ltem_mfm_compare_periodicity()
	ltem_defocus_magn_compare_plot()
	# test()
