import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import os
import math
import re
from scipy import interpolate
from dataclasses import dataclass
from copy import deepcopy
from skimage.measure import label, regionprops

from core.data_parser import DataParser
from core.shuju import Shuju, MetaData
from image.pixel_analysis import PixelAnalysis
from core.plotter import Plotter, PlotSettings
from core.util import Vector, ParamsHL, save_obj, UI_load_json_file, listdir_filtered, Timer, update_obj_from_dict_recursively
from image.pixel_analysis_cythonized import filter_by_labels

class Const:
	sk_density_str: str = ' sk density'
	sk_size_str: str = ' sk size'
	datafile_ext: str = '.txt'


@dataclass
class InputOptions:
	ovf_path: str = None
	ovf_ext: str = '.ovf'
	area_threshold_nm2: float = 10000
	aspect_ratio_threshold: float = 0.7
	intermediate_output_path: str = ''
	grid_spacing: float = 0.004 # X,Y size of each simulation cell in um
	title: str = ''
	results_path: str = ''
	simulation_area_um2: float = 2.25
	do_analysis: bool = True
	do_plotting: bool = True

def analysis_main():

	cmap = matplotlib.cm.get_cmap('viridis')
	const = Const()

	# --- CHOOSE INPUT OPTION --- #
	input_dict_list = UI_load_json_file()

	if not input_dict_list:
		# user chose to cancel
		return

	# --- BIG OUTSIDE LOOP THRU LIST OF INPUT JSON FILES --- #
	for input_dict in input_dict_list:

		in_opt = InputOptions()
		update_obj_from_dict_recursively(in_opt, input_dict)

		# import files based on filter
		ovf_list = listdir_filtered(in_opt.ovf_path, in_opt.ovf_ext)

		# prepare to plot
		data_to_show_2D_list = []
		H_field_arr = []
		Up_domains_arr = []
		Down_domains_arr = []
		Up_domains_size_arr = []
		Down_domains_size_arr = []

		phy_len_cell = in_opt.grid_spacing*1e3 # in nm
		phy_area_cell = (phy_len_cell)**2
		counts_threshold = int(in_opt.area_threshold_nm2/phy_area_cell)

		if in_opt.do_analysis:

			# --- loop through all files in list --- #
			for ind, filename in enumerate(ovf_list):

				# capture the field or reversal field
				re_search = re.search('Hr_(\+?-?\d+)\s*mT', filename)
				if re_search:
					H_field_arr.append(float(re_search.group(1))*1e-3)
				else:
					re_search = re.search('_(\+?-?\d+)\s*mT', filename)
					H_field_arr.append(float(re_search.group(1)) * 1e-3)

				#load ovf file
				data = DataParser.import_ovf(path= in_opt.ovf_path,filename=filename, z_layer_number=0, grid_spacing= in_opt.grid_spacing)

				thresholded_data = PixelAnalysis.skimage_filters_threshold_wrapper(data, invert=False, local_threshold=False, global_thresh_finetune=0)

				thresholded_data.metadata.set_attrs(label=filename + ' 01 Thresholded data')
				data_to_show_2D_list.append(deepcopy(thresholded_data))

				connectedness_filtered_data, number_of_up_domains, number_of_down_domains, ave_len_up_domains, ave_len_down_domains = filter_by_aspect_ratio(
					thresholded_data, counts_threshold=counts_threshold, aspect_ratio_threshold= in_opt.aspect_ratio_threshold)

				connectedness_filtered_data.metadata.set_attrs(label=filename + ' 02 Connectedness-filtered data')
				data_to_show_2D_list.append(deepcopy(connectedness_filtered_data))

				Up_domains_arr.append(number_of_up_domains)
				Down_domains_arr.append(number_of_down_domains)
				Up_domains_size_arr.append(ave_len_up_domains*phy_len_cell)
				Down_domains_size_arr.append(ave_len_down_domains*phy_len_cell)

			# plot image-processed pseudocolour plots
			for data_to_show in data_to_show_2D_list:
				# normalisation
				if (np.max(np.abs(data_to_show.data)) > 1.5):
					data_to_show.data = (data_to_show.data+2)/4
				Plotter.saveasimage(data=data_to_show, pathnfilename=os.path.join(in_opt.intermediate_output_path, data_to_show.metadata.label) + '.tif',
									normalise=False)

			# convert to np arrays to sort
			sk_density = np.rot90(np.vstack((np.array(Down_domains_arr),np.array(Up_domains_arr), np.array(H_field_arr))),3)
			sk_size =  np.rot90(np.vstack((np.array(Down_domains_size_arr),np.array(Up_domains_size_arr), np.array(H_field_arr))),3)

			sort_order = np.array(H_field_arr).argsort()

			sk_density = sk_density[sort_order]
			sk_size = sk_size[sort_order]

			# Hr_arr = Hr_arr[sort_order]
			# Up_domains_arr = Up_domains_arr[sort_order]/in_opt.simulation_area_um2
			# Down_domains_arr = Down_domains_arr[sort_order]/in_opt.simulation_area_um2
			# Up_domains_size_arr = Up_domains_size_arr[sort_order]
			# Down_domains_size_arr = Down_domains_size_arr[sort_order]

			# save data
			np.savetxt(os.path.join(in_opt.results_path, in_opt.title + const.sk_density_str+const.datafile_ext), sk_density, fmt='%.5f', delimiter='\t',
					   header='Field(T)\tUp domain\tDown domain')
			np.savetxt(os.path.join(in_opt.results_path, in_opt.title + const.sk_size_str+const.datafile_ext), sk_size, fmt='%.5f', delimiter='\t',
					   header='Field(T)\tUp domain\tDown domain')

		else:
			# just load data
			sk_density = np.genfromtxt(os.path.join(in_opt.results_path, in_opt.title + const.sk_density_str+const.datafile_ext), delimiter='\t')
			sk_size = np.genfromtxt(os.path.join(in_opt.results_path, in_opt.title + const.sk_size_str+const.datafile_ext), delimiter='\t')

		if in_opt.do_plotting:

			# Plotting
			color_ind_arr = np.linspace(0, 1, 2 + 2)
			plt.style.use(r'C:\Users\Xiaoye\OneDrive\ASTAR-Skyrmions\Projects\skyrmage\misc\default.mplstyle')

			# Sk density
			tmp_fig = plt.figure(figsize=(10,8))
			plt.plot(sk_density[:,0], sk_density[:,1], 'o-', color=cmap(color_ind_arr[0]), label='up domain')
			plt.plot(sk_density[:,0], sk_density[:,2], '^-', color=cmap(color_ind_arr[1]), label='down domain')

			plt.xlabel('Reversal field (T)')
			plt.ylabel('Skyrmion density/um^2')

			plt.legend(loc='upper right')

			plt.savefig(os.path.join(in_opt.results_path, in_opt.title + const.sk_density_str))
			plt.close(tmp_fig)

			# Sk size
			tmp_fig = plt.figure(figsize=(10, 8))
			plt.plot(sk_size[:,0], sk_size[:,1], 'o-', color=cmap(color_ind_arr[0]), label='up domain')
			plt.plot(sk_size[:,0], sk_size[:,2], '^-', color=cmap(color_ind_arr[1]), label='down domain')

			plt.xlabel('Reversal field (T)')
			plt.ylabel('Skyrmion diameter (nm)')

			plt.legend(loc='lower left')

			plt.savefig(os.path.join(in_opt.results_path, in_opt.title + const.sk_size_str))
			plt.close(tmp_fig)



# run the main function
if __name__ == '__main__':
	analysis_main()
	# test()
