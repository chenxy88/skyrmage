import numpy as np
import matplotlib.pyplot as plt
import os
from copy import copy

from core.data_parser import DataParser
from core.plotter import Plotter
from core.util import Vector
from core.shuju import Shuju

def import_data():
	# import data from hardcoded path and filename
	magnetic_material_volume = 0.3*0.3*1e-7*10 # volume of magnetic materials in cc for N number of repeats samples
	path = r'C:\Users\Xiaoye\OneDrive\DSI-Skyrmions\Projects\MTXM\March 2018\AGM data\180308 MTXM batch 25 Textures 10 repeats'
	filename_list = os.listdir(path)

	filename_list_filtered = []
	data_list = []

	# filter base on criteria
	for filename in filename_list:
		if filename.endswith('.txt'):
			filename_list_filtered.append(filename)

	# loop through all files in list
	for filename in filename_list_filtered:
		tmp_data = DataParser.import_plaintxt(path=path, filename=filename, delimiter=',',skip_header=88, process_data_fn=lambda x: x[:,3]/magnetic_material_volume, process_axes_fn=lambda x:x[:,2], skip_footer=3)

		tmp_data.metadata.axes_labels = Vector('H (Oe)','M (emu/cc)')
		
		# remove extension for label
		file_label, file_extension = os.path.splitext(filename)
		tmp_data.metadata.label = file_label

		# copy to mydata
		data_list.append(copy(tmp_data))

	return data_list

def plot_togther(data_list):

	for my_data in data_list:
		Plotter.plot(my_data)

	plt.legend()

def plot_as_subplots(data_list):
	n = len(data_list)
	
	for ind, my_data in enumerate(data_list):
		plt.subplot(n,1,ind+1)
		Plotter.plot(my_data)
		plt.legend()

def plot_in_separate_figs(data_list):
	n = len(data_list)
	
	for ind, my_data in enumerate(data_list):
		plt.figure(ind)
		Plotter.plot(my_data)
		plt.legend()

def plot_main():

	my_data = import_data()
	plot_in_separate_figs(my_data)

	plt.show()



# run the main function
if __name__ == '__main__':
	plot_main()
	# test()
