import numpy as np
from scipy.stats import gaussian_kde
from sklearn.neighbors import KernelDensity  # KDE
from sklearn.model_selection import GridSearchCV # Bandwidth estimation for KDE

from core.shuju import Shuju, MetaData


class Stats:

	"""
	Statistical methods
	"""

	@staticmethod
	def kde_wrapper(data, data_grid, bw_method='silverman'):

		if bw_method == 'scott' or bw_method == 'silverman':

			# find estimated pdf using Kernel density estimator
			kde_fn = gaussian_kde(data, bw_method=bw_method) # used to be 0.3
			pdf = kde_fn.evaluate(data_grid)

		elif bw_method == 'cross_validation':

			# from: https://jakevdp.github.io/blog/2013/12/01/kernel-density-estimation/
			# bandwidth estimation through cross-validation is very slow and yield unreliable results for datasets with many outliers
			grid = GridSearchCV(KernelDensity(), {'bandwidth': np.linspace(0.1, 10, 50) * data.std(ddof=1)}, cv=10)
			grid.fit(data[:, None])  # this function expects a 2D array
			# print(data_grid.best_params_)
			kde_fn = grid.best_estimator_
			pdf = np.exp(kde_fn.score_samples(data_grid[:, None]))

		else:

			raise ValueError('Invalid bw_method')

		pdf_shuju = Shuju(pdf)
		pdf_shuju.metadata.set_attrs(axes=data_grid, label='Estimated PDF')

		return pdf_shuju