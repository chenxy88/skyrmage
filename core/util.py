import time
import numpy as np
import pickle
import json
import tkinter as tk
from tkinter import filedialog
import os
from copy import deepcopy
# Helper classes

class Vector:
	def __init__(self, x=None, y=None, z=None):
		self.x = x
		self.y = y
		self.z = z

	def get_dimension(self):
		return [self.x is not None, self.y is not None, self.z is not None]

	def distance(self, pt = None):
		"""
		Find the Euclidean distance between 2 points
		:param pt: another Vector object
		:return: float, distance
		"""
		dim = self.get_dimension()

		if pt is None:
			# return L2 norm of this vector
			d2 = 0
			if dim[0]:
				d2 = d2 + self.x ** 2
			if dim[1]:
				d2 = d2 + self.y ** 2
			if dim[2]:
				d2 = d2 + self.z ** 2
			return np.sqrt(d2)
		else:
			# check that the dimensions of the self and pt are the same
			assert isinstance(pt, Vector)
			assert dim == pt.get_dimension()

			# distance squared
			d2 = 0
			if dim[0]:
				d2 = d2 + (self.x - pt.x) ** 2
			if dim[1]:
				d2 = d2 + (self.y - pt.y) ** 2
			if dim[2]:
				d2 = d2 + (self.z - pt.z) ** 2
			return np.sqrt(d2)

	def length(self, n=2):
		"""
		Find the length of the vector
		:return: L2 length of the vector
		"""

		dim = self.get_dimension()
		# distance squared
		result = 0
		if dim[0]:
			result = result + self.x ** n
		if dim[1]:
			result = result + self.y ** n
		if dim[2]:
			result = result + self.z ** n
		return result**(1/n)

	def two_Vectors_arb_fun (vec1, vec2, arb_fun):
		"""
		Apply arb_fun to not None components of two vectors
		"""
		result = Vector()
		
		if not vec1.x is None and not vec2.x is None:
			result.x = arb_fun(vec1.x, vec2.x)

		if not vec1.y is None and not vec2.y is None:
			result.y = arb_fun(vec1.y, vec2.y)

		if not vec1.z is None and not vec2.z is None:
			result.z = arb_fun(vec1.z, vec2.z)

		return result

	def any_component_greater(vec1, vec2):
		"""
		True if any component of vec1 is greater than vec2
		"""
		result = Vector.two_Vectors_arb_fun(vec1, vec2, lambda a,b: a>b)
		return result.x or result.y or result.z


	def __add__(self, vec2):
		"""
		Simple vector addition
		"""
		return Vector.two_Vectors_arb_fun(self, vec2, lambda a,b: a+b)

	def __sub__(self, vec2):
		"""
		Simple vector subtraction
		"""
		return Vector.two_Vectors_arb_fun(self, vec2, lambda a,b: a-b)

	def __mul__(self, other):
		"""
		Either vector dot product or vector multipled by scalar
		:param other:
		:return:
		"""
		if isinstance(other, Vector):
			# Vector dot product
			result_vec = Vector.two_Vectors_arb_fun(self, other, lambda a,b: a*b)
			return result_vec.length(n=1)
		else:
			# other should be a scalar, either int or float
			vec2 = Vector(other, other, other)
			return Vector.two_Vectors_arb_fun(self, vec2, lambda a,b: a*b)

class ParamsHL:
	def __init__(self, high=None, low=None):
		# Commonly used to set axes limits. Default to None is ideal.
		self.high = high
		self.low = low

	def update_extreme_value(self, val):
		# update self if val is more extreme then high or low
		if val > self.high:
			self.high = val

		if val < self.low:
			self.low = val

class Timer(object):

	# timer = Timer()
	# timer.display_time_reset('Imported data')

	def __init__(self, name=None, disabled = False):
		self.name = name
		self.tstart = time.time()
		self.disabled = disabled

	def display_time_reset(self, comment=''):
		if not self.disabled:
			if self.name:
				print('[%s]' % self.name)
			print(comment+' Elapsed: %.3f s' % (time.time() - self.tstart))
			self.tstart = time.time()

# Helper functions

def meshgrid_list(*lists):
	"""
	Given a number of lists (maybe of different lengths, say n1, n2,... ), generate a single list (of length n1*n2*...) such that each element is a list that contains elements from input lists. Useful for looping through several list without nested loops.
	:param lists: a tuple that contains all the input list
	:return: meshgrid of lists
	"""
	# number of lists as input
	num_list = len(lists)
	# list of lengths of in list of lists
	lists_lens = []
	lists_ind = []
	output_list_len = 1
	for l in lists:
		assert isinstance(l, list)
		assert len(l) >= 1
		tmp_len = len(l)
		lists_lens.append(tmp_len)
		lists_ind.append(np.arange(0,tmp_len))
		output_list_len = output_list_len*tmp_len

	output_list = [None]*output_list_len
	inds_tmp = np.meshgrid(*lists_ind)
	inds = []
	for i in range(0,num_list):
		inds.append(np.reshape(inds_tmp[i],(-1,1)))

	for j in range(0,output_list_len):
		tmp_list = []
		for i in range(0,num_list):
			tmp_ind = inds[i][j][0]
			tmp_list.append(lists[i][tmp_ind])
		output_list[j] = tmp_list

	return output_list

def fun_inputs_to_dict(**kwargs):
	"""
	Convert keyword function inputs into dictionary
	:param kwargs:
	:return:
	"""
	return kwargs

def UI_load_json_file():
	"""
	load input json file
	:return:
	"""
	# root = tk.Tk()
	files = filedialog.askopenfilenames(title='Select json file')

	data_loaded = []
	if files == '':
		return data_loaded

	for file in files:
		# if the user did selected something
		with open(file) as data_file:
			data_loaded.append(json.load(data_file))

	return data_loaded



def update_obj_from_dict_recursively(some_obj, some_dict):
	"""
	Useful to convert nested json files into nested dataclasses

	Example
	-------
	@dataclass
	class InputOptions:
		path: str = None
		n1: float = 0
		some_thing: Opt2 = Opt2()

	@dataclass
	class Opt2:
		some_string:str = None
		some_num:float = 0.0

	...

	input_dict_list = UI_load_json_file()

	...

	in_opt = InputOptions()
	update_obj_from_dict_recursively (in_opt, input_dict)

	:param some_obj:
	:param some_dict:
	:return:
	"""
	for k in some_dict:
		tmp_v = some_dict[k]
		if isinstance(tmp_v, dict):
			tmp_obj = some_obj.__dict__[k]
			update_obj_from_dict_recursively(tmp_obj, tmp_v)
		else:
			some_obj.__dict__.update({k:tmp_v})

def flatten(some_list):
	"""Given a list, possibly nested to any level, return it flattened."""
	new_list = []
	for item in some_list:
		if isinstance(item, list):
			new_list.extend(flatten(item))
		else:
			new_list.append(item)
	return new_list


# this function is the heart of one_sim
def outer_product_object_list(obj, start_ind = 0):
	# given an object which may contain some lists, return a list of objects that
	# holds individuals items of the list instead
	# if the object contains multiple lists, a list of objects equivalent to the outerproduct of
	# those lists are returned
	# this works with nest objects and but does NOT work with NESTED LISTS by design (let's not use nested lists)

	assert (hasattr(obj, '__dict__'))
	obj_dict = obj.__dict__
	dict_list = list(enumerate(obj_dict))

	# scan through each member of the object
	for ind in range(start_ind, len(dict_list)):

		key = dict_list[ind][1]
		val = obj_dict[key]

		# if found a nested object
		if hasattr(val, '__dict__'):
			# lets open up this object and explore
			sub_obj = outer_product_object_list(val)
			# if it turns out that this object contain lists
			if isinstance(sub_obj, list):
				# flatten any nested lists of sub objects
				sub_obj = flatten(sub_obj)
				result = [None] * len(sub_obj)

				for (ind_i, val_i) in enumerate(sub_obj):
					obj_tmp = deepcopy(obj)
					setattr(obj_tmp, key, val_i)
					result[ind_i] = outer_product_object_list(obj_tmp, start_ind+1) #plus one here is impt

				return result

			# if the object does not contain any lists, don't bother with it
			else:
				pass

		# if found a list, stop and flatten list
		elif isinstance(val, list):

			result = [None]*len(val)

			for (ind_i, val_i) in enumerate(val):
				obj_tmp = deepcopy(obj)
				# replace list with a single value
				setattr(obj_tmp, key, val_i)
				result[ind_i] = outer_product_object_list(obj_tmp, start_ind+1) #plus one -> do not open up nested list

			return  result

	# if no objects or list are found, just return this object
	return obj


def get_attr_from_obj (obj):
	"""
	Given an object, return a list of strings of the names of all non-callable and non hidden attributes
	:param obj:
	:return: list of attribute names
	"""
	return [attr for attr in dir(obj) if not callable(getattr(obj, attr)) and not attr.startswith("__")]

def save_obj(obj, path_and_filename):
	with open(path_and_filename, 'wb') as output:  # Overwrites any existing file.
		pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)


def listdir_filtered (path: str, extension: str):

	filename_list = os.listdir(path)
	filename_list_filtered = []

	# filter based on criteria
	for filename in filename_list:
		if filename.endswith(extension):
			filename_list_filtered.append(filename)

	return filename_list_filtered

def main():
	# for testing purpose
	a = Vector(3,4)
	b = a*-1
	c = a*b

	d=3

# run the main function
if __name__ == '__main__':
	main()  # test()