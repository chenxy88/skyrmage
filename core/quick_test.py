import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy

from core.shuju import Shuju, MetaData
from core.plotter import Plotter
from dataclasses import dataclass

@dataclass
class Point:
	x: float = 0.0
	y: float = 0.0
	z: float = 0.0


def main():
	# for testing purpose
	p = Point(y=2.5)
	print(p)
	print(p.z)

# run the main function
if __name__ == '__main__':
	main()  # test()