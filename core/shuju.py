import numpy as np
from copy import copy, deepcopy
from scipy.interpolate import interp1d

from core.util import get_attr_from_obj, Vector
from core.plotter import Plotter

class Meta:
	"""
	Base class for all Meta objects
	"""
	def set_attrs(self, **kwargs):
		"""
		Example:
			data.metadata.axes_meta.x.set_attrs(label='Spatial frequency', unit='1/um')
		"""
		for k,v in kwargs.items():
			setattr(self, k, v)

	def update_attrs(self, new_meta):
		"""
		Overwrite self with not-None attributes of new_meta
		:param meta_obj:
		:return: updated meta object
		"""
		assert isinstance(new_meta, Meta)

		new_attr = get_attr_from_obj(new_meta)
		self_attr = get_attr_from_obj(self)

		for attr in new_attr:
			if new_meta.__dict__[attr] is not None and attr in self_attr:
				self.__dict__[attr] = new_meta.__dict__[attr]

class MetaData(Meta):
	"""
	Holds basic metainfo for data objects
	"""
	def __init__(self):
		# string, label for the dataset
		self.label = None
		# string, units of this physical quantity
		self.unit = None
		# usually 1d ndarrays or 2d meshgrids
		self.axes = None
		# Vector of strings
		self.axes_labels = None
		# import and export metafiles
		self.import_metafile = None
		self.export_metafile = None
		# scale of the axes, in physical unit/data point
		self.axes_scale = None
		# any other info
		self.misc_dict = {}

class MetaFile(Meta):
	"""
	Holds basic metainfo for file import and exporting
	"""
	def __init__(self):
		# filename, excluding path
		self.filename = None
		# path to file
		self.path = None
		# combined path and filename
		self.path_and_filename = None
		# character to delimitate data columns and rows
		self.delimiter = ','
		# number of header lines
		self.skip_header = 0
		# comment character
		self.comments = '#'
		# process the data after import, or before export
		self.processing_fn = None
		# string, defines the export format
		self.export_format = 'txt'

class MetaImage(MetaFile):
	"""
	Holds additional metainfo for image processing
	"""
	def __init__(self):
		MetaFile.__init__(self)
		self.rotation_angle = None
		self.crop_frac = None
		self.zero_padding = None

class Shuju:
	"""
	A Shuju object contains the data, experimental metadata, and methods that describe how data and
	metadata can be wrapped.
	data: a ndarray.
	metadata: a MetaBasic or derived object.
	"""
	# IMPORTANT. DO NOT USE  metadata = MetaData(). SHUJU OBJECTS WILL END UP SHARING THE SAME METADATA OBJ.
	def __init__(self, data=None, metadata=None):
		"""
		Constructor
		:param data: A ndarray, either 1D, 2D or 3D
		"""
		# assert isinstance(metadata, MetaData)
		self.data = data
		# a MetaBasic or derived object
		if isinstance(metadata, MetaData):
			self.metadata = metadata
		else:
			self.metadata = MetaData()

	def arb_fn(self, fn):
		"""
		Apply an arbitrary function of data
		:param fn:
		:return: none
		"""
		self.data = fn(self.data)

	def generate_axes(self, scale=None):
		"""
		Automatically generate axes for Shuju object, starting from 0, up to 3d data sets
		:param scale: In real units/ data point
		:return: none
		"""
		assert isinstance(scale, Vector)

		data_shape = self.data.shape
		self.metadata.axes = Vector()
		len_data_shape = len(data_shape)
		if len_data_shape >= 1:
			self.metadata.axes.x = np.arange(0,data_shape[0])*scale.x
		if len_data_shape >= 2:
			# by conventional, first index represents row and that is 'y' instead of 'x'
			self.metadata.axes.x = np.arange(0, data_shape[1])*scale.x
			self.metadata.axes.y = np.arange(0,data_shape[0])*scale.y
		if len_data_shape >= 3:
			self.metadata.axes.z = np.arange(0,data_shape[2])*scale.z

	# utility functions

	@staticmethod
	def shuju1d_list_to_2d (shuju_list, y_axis = None):
		"""
		Convert a list of 1D shuju objects, with the same axes, to a 2D shuju object
		input: 
			shuju_list: a list of 1D shuju objects
		output:
			a 2D shuju object
		"""

		assert isinstance(shuju_list[0], Shuju)

		list_len = len(shuju_list)
		data1d_len = len(shuju_list[0].data)

		new_data2d = np.zeros((list_len, data1d_len))
		tmp_x = shuju_list[0].metadata.axes
		if y_axis is None:
			y_axis = np.arange(0, list_len)
		else:
			#sort according to y_axis
			sort_order = sorted(range(len(y_axis)), key=lambda k: y_axis[k])
			shuju_list = [shuju_list[i] for i in sort_order]
			y_axis = [y_axis[i] for i in sort_order]

		for ind, element in enumerate(shuju_list):
			new_data2d[ind, :] = copy(element.data)

		new_shuju = Shuju(data = new_data2d)
		# new functionality: shift_axis_for_pcolor
		mesh_x, mesh_y =  np.meshgrid(Plotter.shift_axis_for_pcolor(tmp_x), Plotter.shift_axis_for_pcolor(y_axis))
		new_shuju.metadata.axes = Vector(mesh_x, mesh_y)

		return new_shuju

	@staticmethod
	def shuju1d_list_averaged(shuju_list, new_x, do_average = True):
		"""
		Interpolate all shuju in list onto new x axis. Do not extrapolate.
		Average overlapping parts of data appropriately. Removed empty parts.
		:param shuju_list:
		:return:
		"""

		len_x = len(new_x)
		new_y = np.zeros(len_x)
		# count the number of datasets that overlap at every point.
		# used for normalisation
		data_counter = np.zeros(len_x)

		# loop through shuju_list
		for this_shuju in shuju_list:
			fn = interp1d(this_shuju.metadata.axes, this_shuju.data, bounds_error = False, fill_value = np.nan)
			tmp_y = fn(new_x)
			nan_arr = np.isnan(tmp_y)
			data_counter = data_counter + np.invert(nan_arr).astype(int)
			# set NaNs to zero
			tmp_y[nan_arr] = 0
			# add data to consolidated arr
			new_y = new_y + tmp_y

		# delete non-overlapping, empty parts
		ind_to_keep = data_counter > 0
		new_x = new_x[ind_to_keep]
		new_y = new_y[ind_to_keep]
		data_counter = data_counter[ind_to_keep]

		#normalisation
		if do_average:
			new_y = new_y/data_counter
		shuju_result = Shuju(new_y)
		shuju_result.metadata.axes = new_x

		return shuju_result


