from unittest import TestCase
import numpy as np

from core.util import Vector, meshgrid_list
from core.shuju import MetaData, Shuju

# unit testing for all classes

class TestCore(TestCase):
	# Vector class tests
	def test_vector(self):
		a = Vector(3,4,0)
		b = Vector(0,0,0)
		self.assertEqual(a.distance(b),5)

		# Vectors with different dimension
		b = Vector(0,0)
		with self.assertRaises(AssertionError):
			a.distance(b)

		self.assertEqual(b.get_dimension(),[True, True, False])
		self.assertEqual(a.length(),5)

	# meshgrid_list function tests
	def test_meshgrid_list(self):
		a = ['a', 1, 3.2]
		b = ['b', None]
		self.assertEqual(meshgrid_list(a,b),[['a', 'b'], [1, 'b'], [3.2, 'b'], ['a', None], [1, None], [3.2, None]])

	def test_meta(self):
		a = MetaData()
		a.quantity='Length'
		a.unit='m'

		b= MetaData()
		b.unit='cm'
		b.label='updated'

		c = MetaData()
		c.quantity='Length'
		c.unit = 'cm'
		c.label = 'updated'

		a.update_attrs(b)
		self.assertEqual(a.__dict__, c.__dict__)

	def test_shuju(self):
		a = Shuju(np.array([[1,2],[3,4],[5,6]]))
		a.generate_axes(Vector(1, 1))
		self.assertTrue(np.array_equal(a.metadata.axes.x, np.array([0, 1])))
		self.assertTrue(np.array_equal(a.metadata.axes.y, np.array([0,1,2])))