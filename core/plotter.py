from matplotlib import pyplot as plt

from copy import deepcopy
# from core.shuju import Shuju
from core.util import Vector, ParamsHL
from PIL import Image
import numpy as np

# The purpose of this class should be just to wrap around matplotlib functions and work with Shuju objects, with good default settings.
# It should be more intuitive and less pendantic.

class PlotSettings:
	dpi = 300
	#plt.style.use(r'C:\Users\Xiaoye\OneDrive\ASTAR-Skyrmions\Projects\skyrmage\misc\default.mplstyle')
	cmap = 'viridis'

class Plotter:
	
	@staticmethod
	def plot(data=None, *args, **kwargs):
		"""Plot a Shuju object"""
		#change style
		plt.style.use(r'C:\Users\Xiaoye\OneDrive\ASTAR-Skyrmions\Projects\skyrmage\misc\default.mplstyle')

		kwargs_combined = dict()

		# line labels
		if data.metadata.label:
			kwargs_combined.update({'label':data.metadata.label})

		kwargs_combined.update(kwargs)

		# ACTUAL PLOTTING HERE
		plt.plot(data.metadata.axes, data.data, *args, **kwargs_combined)

		# set labels and title
		Plotter.set_axes_labels_and_title(data)

		#formatting
		plt.tight_layout()

	@staticmethod
	def plot_multiple(shuju_list=None, title = '', offset = 0, *args, **kwargs):
		"""
		Plot multiple Shuju objects
		:param shuju_list: [Shuju]
		:param title: Overall title
		:param offset: amount to offset each plot by
		:param args:
		:param kwargs:
		:return:
		"""
		tmp_fig = plt.figure()
		for (ind, tmp_shuju) in enumerate(shuju_list):
			tmp_shuju.data += offset * ind
			Plotter.plot(tmp_shuju)

		plt.legend(loc='upper right')
		plt.title(title)

		return tmp_fig

	@staticmethod
	def shift_axis_for_pcolor(vec):
		# shift the vec and add one more element to display all the data
		# assume vec is in ascending order, and it is numeric
		# example:
		# mesh_x, mesh_y = np.meshgrid(shift_axis_for_pcolor(in_opt.dmi_interface_array), shift_axis_for_pcolor(in_opt.exchange_array))
		vec = np.array(vec).astype(float)
		delta = 0.5
		if len(vec) >= 2:
			delta = vec[1]-vec[0]

		result = deepcopy(vec)
		result -= delta/2
		result = np.append(result, result[-1]+delta)

		return result

	@staticmethod
	def pcolormesh(data=None, *args, vlim=None, vlim_fn=None, equal_aspect = True, image_only=False, **kwargs):
		
		kwargs_combined = dict()

		# generate vlim if such a function is provided
		if callable(vlim_fn):
			vlim = vlim_fn(data)
		
		# check if vlim is set
		if isinstance(vlim, ParamsHL):
			kwargs_combined.update({'vmin':vlim.low,'vmax':vlim.high})
			kwargs_combined.update(kwargs)
		
		# ACTUAL PLOTTING HERE
		if isinstance(data.metadata.axes, Vector):
			plt.pcolormesh(data.metadata.axes.x, data.metadata.axes.y, data.data, cmap=PlotSettings.cmap, **kwargs_combined)
		else:
			plt.pcolormesh(data.data, cmap=PlotSettings.cmap, **kwargs_combined)

		if image_only:
			# tmp_fig = plt.gcf()
			# ax = plt.Axes(tmp_fig, [0., 0., 1., 1.])
			# ax.set_axis_off()
			# tmp_fig.add_axes(ax)
			plt.axis('off')
		else:
			# set labels and title
			Plotter.set_axes_labels_and_title(data)
			# plt.colorbar()

		if equal_aspect:
			a = plt.gca()
			a.set_aspect('equal')

	@staticmethod
	def saveasimage(data=None, pathnfilename = '', mode = 'L', normalise = True):
		'''
		Given a shuju object, export it as an image file
		:param data: Shuju object
		:param pathnfilename: string
		:param mode: 'L'-8bit black and white
		:return: none		'''

		tmp_data = deepcopy(data.data)
		if normalise:
			tmp_data = (tmp_data-np.min(tmp_data))/(np.max(tmp_data) - np.min(tmp_data))

		tmp_data = np.flipud(tmp_data)

		if mode == 'L':
			tmp_data = (tmp_data*255).astype(np.uint8)

		if mode == 'I':
			tmp_data = (tmp_data*65535).astype(np.int32)

		im = Image.fromarray(tmp_data, mode=mode)

		im.save(pathnfilename)


	# helpers
	@staticmethod
	def set_axes_labels_and_title(data=None):
		
		# axes labels
		if isinstance(data.metadata.axes_labels, Vector):
			if data.metadata.axes_labels.x:
				plt.xlabel(data.metadata.axes_labels.x)
			if data.metadata.axes_labels.y:
				plt.ylabel(data.metadata.axes_labels.y)

		# create title, if label is set
		if data.metadata.label:
			plt.title(data.metadata.label)


	@staticmethod
	def plot_n_save(shuju_obj, path_n_name):
		"""
		Plot shuju object and save as default file format
		:return:
		"""
		tmp_fig = plt.figure()
		Plotter.plot(shuju_obj)
		plt.savefig(path_n_name, dpi=300)
		plt.close(tmp_fig)