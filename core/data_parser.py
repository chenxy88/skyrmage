import numpy as np
from PIL import Image
from copy import copy
import os
import re

from core.shuju import MetaData, MetaFile, MetaImage, Shuju
from core.util import Vector

# similar changes to plotter are needed here. This should be a simple utility class, without too much complications. This purpose should be to wrap around built-in functions with good defaults, and interface using Shuju objects.

class DataParser:

	@staticmethod
	def import_plaintxt (path_and_filename=None, path=None, filename=None, delimiter='	', skip_header=0, comments='#', process_data_fn=None, process_axes_fn = None, **kwargs) -> Shuju:
		"""
		Import the data
		:return: Shuju object, data
		"""
		
		path_and_filename = DataParser.path_helper(path_and_filename, path, filename)

		# WARNING: Since invalid_raise=false, rows with wrong number of columns are skipped. 
		# User must be responsible from checking the validity of imported data!

		data = np.genfromtxt(path_and_filename, delimiter=delimiter, skip_header=skip_header, comments=comments, invalid_raise=False, **kwargs)

		if callable(process_data_fn):
			processed_data = process_data_fn(copy(data))
		else:
			processed_data = copy(data)

		# wrap data inside a Shuju
		shuju_obj = Shuju(processed_data, MetaData())

		if callable(process_axes_fn):
			shuju_obj.metadata.axes = process_axes_fn(copy(data))

		return shuju_obj

	@staticmethod
	def import_image(path_and_filename=None, path=None, filename=None, grid_spacing=None):
		"""
		Import the image
		:return: Shuju object, data
		"""
		path_and_filename = DataParser.path_helper(path_and_filename, path, filename)

		im = Image.open(path_and_filename)
		# flip up down such that the image, when plotted, will look like that displayed by most
		# image viewers
		data = np.flipud(np.array(im))

		# wrap inside a Shuju object
		shuju_obj = Shuju(data, MetaData())

		if isinstance(grid_spacing, Vector):
			# set axes if grid_spacing is available
			y,x = data.shape
			shuju_obj.metadata.axes = Vector(np.arange(0,x) * grid_spacing.x, np.arange(0, y) * grid_spacing.y)

		return shuju_obj

	@staticmethod
	def import_ovf(path_and_filename=None, path=None, filename=None, z_layer_number = 0, mag_direction = 2, header_bytes = 1000,  grid_spacing = 1, **kwargs):
		"""
		Import the data
		mag_direction: 0-2: x,y,z. Direction of mag to output. Negative to output all
		:return: Shuju object, data
		"""

		# only outputs one direction
		assert(mag_direction >=0 and mag_direction <=2)

		path_and_filename = DataParser.path_helper(path_and_filename, path, filename)

		# First read the header byte and find the the number of xnodes, ynodes, znodes
		with open(path_and_filename) as fin:
			header_data = fin.read(header_bytes)

		num_nodes = re.search('# xnodes: (\+?-?\d+)\s*# ynodes: (\+?-?\d+)\s*# znodes: (\+?-?\d+)', header_data)
		x_len = int(num_nodes.group(1))
		y_len = int(num_nodes.group(2))
		z_len = int(num_nodes.group(3))

		# number of comment lines
		num_comment_lines = header_data.count('#')

		single_layer_size = x_len*y_len
		read_line_start = z_layer_number*single_layer_size+num_comment_lines
		read_line_end = read_line_start+single_layer_size
		line_index= 0
		data_index = 0

		data = np.zeros(single_layer_size, dtype=np.double)

		# User must be responsible for checking the validity of imported data!

		# data = np.genfromtxt(path_and_filename, comments='#', **kwargs)
		with open(path_and_filename) as fin:
			for line in enumerate(fin):
				line_str = line[1]
				# check if it is within read_line_start and read_line_end
				if line_index >= read_line_start:
					if line_index < read_line_end:
						tmp_list = np.fromstring(line_str,dtype=np.double,sep=' ')
						data[data_index] = tmp_list[mag_direction]
						data_index += 1
					else:
						break

				line_index+=1


		# reshape into original shape
		data = data.reshape(x_len, y_len)

		# wrap data inside a Shuju
		shuju_obj = Shuju(data, MetaData())
		mesh1, mesh2 = np.meshgrid(np.arange(0,y_len)*grid_spacing, np.arange(0,x_len)*grid_spacing)
		shuju_obj.metadata.axes = Vector(mesh1, mesh2)

		return shuju_obj

	@staticmethod
	def export_data (data, path_and_filename=None, path=None, filename=None, delimiter='	',header = '', fmt='%.10f', processing_fn=None):
		# assertions
		assert isinstance(data, Shuju)

		if callable(self.metafile.processing_fn):
			data.data = self.metafile.processing_fn(data.data)

		if path_and_filename is None:
			path_and_filename = os.path.join(path, filename)

		np.savetxt(path_and_filename, data.data, fmt=fmt, delimiter=delimiter, header=header)

	# helper functions
	@staticmethod
	def path_helper(path_and_filename, path, filename):
		if path_and_filename is None:
			path_and_filename = os.path.join(path, filename)
		
		return path_and_filename

class ImageParser(DataParser):
	def __init__(self, metaimage = MetaImage()):
		assert isinstance(metaimage, MetaImage)

		DataParser.__init__(self, metaimage)
		self.metaimage = metaimage

	def load_data (self, metadata = MetaData()):
		"""
		Import the image
		:return: the image in data form
		"""
		im = Image.open(self.metaimage.path_and_filename)

		# rotate and crop image such that the tilt axis is the x-axis
		rotation_angle_degrees = self.metaimage.rotation_angle
		# need to rotate?
		if rotation_angle_degrees != 0:
			# TODO: generalise this to rectangles
			# convert to radians
			rotation_angle_radians = rotation_angle_degrees / 180 * np.pi
			# BICUBIC interpolation makes not much difference
			im = im.rotate(rotation_angle_degrees, expand=True, resample=Image.BILINEAR)
			# im.show()
			# len_big is the length of the square after rotation, with expansion
			len_big, tmp = im.size
			# len_small is the largest square that can be inscribed in rotated square
			len_small = np.floor(len_big / (np.abs(np.sin(rotation_angle_radians)) + np.abs(np.cos(rotation_angle_radians))) ** 2)
			len_diff = np.ceil((len_big - len_small) / 2)
			im = im.crop((len_diff, len_diff, len_diff + len_small, len_diff + len_small))
			# im.show()

		# need to crop?
		if self.metaimage.crop_frac is not None:
			assert isinstance(self.metaimage.crop_frac, Vector)
			im_len_x, im_len_y = im.size
			crop_duple = (np.ceil((0.5-self.metaimage.crop_frac.x/2)*im_len_x), np.ceil((0.5-self.metaimage.crop_frac.y/2)*im_len_y), np.ceil((0.5+self.metaimage.crop_frac.x/2)*im_len_x), np.ceil((0.5+self.metaimage.crop_frac.y/2)*im_len_y))
			im = im.crop(crop_duple)

		# Ensure data has even number of elements
		data = np.array(im)

		# need to zero pad?
		if self.metaimage.zero_padding is not None:
			assert isinstance(self.metaimage.zero_padding, Vector)
			y, x = data.shape
			new_data = np.zeros((y*self.metaimage.zero_padding.y, x*self.metaimage.zero_padding.x))
			new_data[0:y,0:x] = data
			data = new_data.copy()

		y, x = data.shape
		if y % 2 == 1:
			data = np.delete(data, (0), axis=0)
		if x % 2 == 1:
			data = np.delete(data, (0), axis=1)

		# wrap inside a Shuju object
		data = Shuju(data, MetaData())
		if isinstance(metadata, MetaData):
			data.metadata.update_attrs(metadata)
		# data.write_metadata({MetaField.import_location: self.filename,						 MetaField.shuju_type: ShujuType.one_or_more_2D_array})

		return  data


