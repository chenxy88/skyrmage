import numpy as np
import matplotlib.pyplot as plt
import os
import math
import re
from scipy.interpolate import interp1d
from dataclasses import dataclass
from copy import deepcopy
import json

from core.data_parser import DataParser
from core.plotter import Plotter
from core.util import Vector, ParamsHL, save_obj, UI_load_json_file, listdir_filtered, Timer, update_obj_from_dict_recursively
from core.shuju import Shuju
from image.fourier_analysis import PowerSpectrumCalc
from scipy.optimize import curve_fit


@dataclass
class InputOptions:
	path: str = None
	title: str = ''
	results_path: str = ''
	json_file_ext: str = 'json'
	ovf_file_ext: str = 'ovf'
	out_folder_ext: str = 'out'
	z_layer_number: int = 0
	grid_spacing: float = 1e-9
	freq_cutoff: ParamsHL = ParamsHL(np.inf, -1)
	freq_remove: float = 0.0
	experimental_wavevector: float = 0 # the experimentally measured wavevector, in 1/um
	dmi_interface_array: [float] = None
	exchange_array: [float] = None

# material parameters
@dataclass
class MaterialParameters:
	landau_damping: float = 0.1
	mag_sat: float = 1 # magnetisation saturation in MA/m
	exchange: float = 10  # exchange in pJ/m
	dmi_bulk: float = 0 # bulk DMI mJ/m^2
	dmi_interface: float = 2 # interfacial DMI mJ/m^2
	anistropy_uni: float = 0.5  # 1st order uniaxial anisotropy constant, MJ/m^3. NOTE: This is NOT effective anistropy, which takes into account dipolar energy
	interlayer_exchange: float = 0  # interlayer exchange scaling

@dataclass
class GeometryParameter:
	z_fm_single_thickness: float = 1
	z_nm_single_thickness: float = 2
	z_layer_rep_num: int = 1
	z_cell_size: float = 1
	z_single_rep_thickness: float = 0 # thickness of single repetition
	phy_size: Vector = Vector(2048, 2048, 0) # in nm
	grid_cell_count: Vector = Vector(512, 512) # number of cells in simulation, should be power of 2
	mesh_cell_size: Vector = Vector()
	pbc: Vector = Vector(0, 0, 0)

	effective_medium_scaling: float = 1  # scaling of material parameters = t_FM/t_repeat

	def calc_auto_parameters(self):
		self.z_single_rep_thickness = self.z_fm_single_thickness + self.z_nm_single_thickness
		self.phy_size.z = self.z_single_rep_thickness * self.z_layer_rep_num
		self.grid_cell_count.z = round(self.phy_size.z / self.z_cell_size)
		self.mesh_cell_size = Vector(self.phy_size.x / self.grid_cell_count.x, self.phy_size.y / self.grid_cell_count.y, self.phy_size.z / self.grid_cell_count.z)

@dataclass
class SimulationMetadata:
	commit: str = '' # version control
	stage: int = 0
	loop: int = -1
	sim_name: str = ''
	sim_full_name: str = ''
	walltime: str = '24:00:00'
	sim_id: str = ''
	output_dir: str = ''
	mumax_file: str = ''

	sh_file: str = ''
	production_run: bool = False
	mumax_installed: bool = False
	project_code: str = 'Personal'


@dataclass
class TuningParameters:
	external_Bfield: float = 0

# all experimental parameters
@dataclass
class SimulationParameters:

	# SimulationMetadata
	sim_meta: SimulationMetadata = SimulationMetadata()
	# MaterialParameters
	mat: MaterialParameters = MaterialParameters()
	# MaterialParameters
	mat_scaled: MaterialParameters = MaterialParameters()
	# GeometryParameter
	geom: GeometryParameter = GeometryParameter()
	# TuningParameters
	tune: TuningParameters = TuningParameters()


def analysis_main():

	def lognorm_fun (x, A, sigma, mu):
		return A/(x*sigma*np.sqrt(2*np.pi))*np.exp(-2*((np.log(x)-mu)/sigma)**2)

	# --- CHOOSE INPUT OPTION --- #
	input_dict_list = UI_load_json_file()

	if not input_dict_list:
		# user chose to cancel
		return

	# --- BIG OUTSIDE LOOP THRU LIST OF INPUT JSON FILES --- #
	for input_dict in input_dict_list:

		in_opt = InputOptions()
		update_obj_from_dict_recursively(in_opt, input_dict)

		# import files based on filter
		json_list = listdir_filtered(in_opt.path, in_opt.json_file_ext)
		out_folder_list = listdir_filtered(in_opt.path, in_opt.out_folder_ext)

		json_list.sort()
		out_folder_list.sort()
		num_files = len(json_list)

		# init stuff
		# this is the period in nm
		tuning_params_2D_period = np.zeros((len(in_opt.dmi_interface_array), len(in_opt.exchange_array)))
		tuning_params_2D_diff = np.zeros((len(in_opt.dmi_interface_array), len(in_opt.exchange_array)))
		filename_2D = np.ndarray(shape=(len(in_opt.dmi_interface_array), len(in_opt.exchange_array)), dtype='U100')

		# each json file must have one and only one .out folder
		assert (num_files == len(out_folder_list))

		# read_file_timer = Timer('Read file')
		# --- loop through all files in list --- #
		for ind, filename in enumerate(json_list):

			with open(os.path.join(in_opt.path, filename)) as json_params_file:
				sim_params_dict = json.load(json_params_file)

			# load simulation parameters
			sim_params = SimulationParameters()
			update_obj_from_dict_recursively(sim_params, sim_params_dict)

			# remove the extension
			filename = filename.split('.')[0]

			# check that filename of json and out folder are the same
			#assert(filename == out_folder_list[ind].split('.')[0])

			# progress
			print('Processing... %0.1f%% complete.  Loading %s.' %(float(ind/num_files)*100, filename))

			out_folder_path = os.path.join(in_opt.path, out_folder_list[ind])

			if in_opt.ovf_file_ext != '':
				ovf_list = listdir_filtered(out_folder_path, in_opt.ovf_file_ext)
			else:
				# for the case where the .ovf file is not in a separate folder, but in the main folder
				ovf_list = [out_folder_path]

			# there should be only one ovf file, which is the relaxed one
			assert(len(ovf_list)==1)

			# import data from ovf
			data = DataParser.import_ovf(path=out_folder_path, filename=ovf_list[0], z_layer_number=in_opt.z_layer_number, grid_spacing=in_opt.grid_spacing)
			header_text = "Exchange(pJ/m) = %.1f, DMI (mJ/m^2) = %.1f" %(sim_params.mat.exchange, sim_params.mat.dmi_interface)
			data.metadata.label = header_text
			data.metadata.axes_labels = Vector('x (um)', 'y (um)')
			Plotter.pcolormesh(data=data, equal_aspect=True)
			plt.savefig(os.path.join(in_opt.results_path, filename + ' raw'), dpi=300)
			plt.close()

			# find FFT
			freq_amp, fft_data = PowerSpectrumCalc.calc_power_spectrum(data_in=data, grid_spacing=Vector(in_opt.grid_spacing, in_opt.grid_spacing),freq_remove = in_opt.freq_remove)
			power_spec_data = PowerSpectrumCalc.shift_for_plotting(freq_amp)
			power_spec_data.metadata.label = header_text
			power_spec_data.metadata.axes_labels = Vector('wavevector (1/um)', 'wavevector (1/um)')
			Plotter.pcolormesh(data=power_spec_data, equal_aspect=True)
			plt.xlim(-in_opt.freq_cutoff.high*3, in_opt.freq_cutoff.high*3)
			plt.ylim(-in_opt.freq_cutoff.high*3, in_opt.freq_cutoff.high*3)
			plt.savefig(os.path.join(in_opt.results_path, filename + ' FFT amp'), dpi=300)
			plt.close()

			# find radial profile
			[radial_profile, radial_profile_fit, wavevector_fit] = PowerSpectrumCalc.calc_radial_profile(data_in=freq_amp, distance_resolution=0.2, freq_cutoff = in_opt.freq_cutoff,
														fit_lognormal_curve = True)

			# set labels correctly
			radial_profile.metadata.axes_labels = Vector('Radial spatial frequency (1/um)', 'Radially averaged FFT amplitude (A.U.)')
			radial_profile.metadata.label = filename

			radial_profile_fit.metadata.axes_labels = Vector('Radial spatial frequency (1/um)', 'Radially averaged FFT amplitude (A.U.)')
			radial_profile_fit.metadata.label = filename+' fit'

			# copy to mydata
			tmp_plot_list = [radial_profile, radial_profile_fit]
			#read_file_timer.display_time_reset()

			Plotter.plot_multiple(tmp_plot_list)
			# put the fitted wavevector on the figure
			text_pos_y = np.min(radial_profile_fit.data) + 0.5 * np.std(radial_profile_fit.data)
			plt.text(in_opt.freq_cutoff.low+0.5, text_pos_y, '{:.2f}'.format(wavevector_fit), fontsize=12)

			plt.savefig(os.path.join(in_opt.results_path, filename + ' comparison to fit'), dpi=300)
			# plt.show()
			plt.close()

			# save wavevector_fit to tuning_params_2D
			param_pos1 = np.where(np.array(in_opt.dmi_interface_array) == sim_params.mat.dmi_interface)[0][0]
			param_pos2 = np.where(np.array(in_opt.exchange_array) == sim_params.mat.exchange)[0][0]
			# save the period in nm
			tuning_params_2D_period[param_pos1, param_pos2] = 1000/wavevector_fit
			# percentage difference between experimental period and calculated period
			tuning_params_2D_diff[param_pos1, param_pos2] = np.abs(1/in_opt.experimental_wavevector - 1/wavevector_fit)/(1/in_opt.experimental_wavevector)
			filename_2D[param_pos1, param_pos2] = filename

		# end of loop of all files

		# prepare data to write to text file
		mesh_x, mesh_y = np.meshgrid(in_opt.dmi_interface_array,in_opt.exchange_array)
		text_data = np.stack((mesh_x.ravel(), mesh_y.ravel(),  tuning_params_2D_period.T.ravel(), tuning_params_2D_diff.T.ravel()), axis=-1)
		header = 'DMI (mJ/m^2)\tExchange(pJ/m)\tPeriod(nm)\tFractional difference\tFilename\r\n'
		filename_2D_ravelled = filename_2D.T.ravel()

		# write to text file
		with open(os.path.join(in_opt.results_path, in_opt.title + ' text_data.txt'), 'w') as text_file:
			text_file.write(header)
			for ind, f_name in enumerate(filename_2D_ravelled):
				text_file.write('%.1f\t%.1f\t%.1f\t%.3f\t%s\r\n'%(text_data[ind,0],text_data[ind,1],text_data[ind,2], text_data[ind,3],f_name))

		# prepare for plotting period
		tuning_params_2D_shuju = Shuju(data=tuning_params_2D_period.T)
		mesh_x, mesh_y = np.meshgrid(Plotter.shift_axis_for_pcolor(in_opt.dmi_interface_array), Plotter.shift_axis_for_pcolor(in_opt.exchange_array))
		tuning_params_2D_shuju.metadata.axes = Vector(mesh_x, mesh_y)
		tuning_params_2D_shuju.metadata.axes_labels = Vector('Interfacial DMI (mJ/m^2)','Exchange (pJ/m)')
		tuning_params_2D_shuju.metadata.label = in_opt.title+' period (nm)'
		Plotter.pcolormesh(data=tuning_params_2D_shuju, equal_aspect=False)
		plt.savefig(os.path.join(in_opt.results_path, in_opt.title + ' period'), dpi=300)
		plt.close()

		# prepare for plotting fractional difference
		tuning_params_2D_shuju = Shuju(data=tuning_params_2D_diff.T)
		mesh_x, mesh_y = np.meshgrid(Plotter.shift_axis_for_pcolor(in_opt.dmi_interface_array), Plotter.shift_axis_for_pcolor(in_opt.exchange_array))
		tuning_params_2D_shuju.metadata.axes = Vector(mesh_x, mesh_y)
		tuning_params_2D_shuju.metadata.axes_labels = Vector('Interfacial DMI (mJ/m^2)','Exchange (pJ/m)')
		tuning_params_2D_shuju.metadata.label = in_opt.title+' fractional difference'
		Plotter.pcolormesh(data=tuning_params_2D_shuju, equal_aspect=False)
		plt.savefig(os.path.join(in_opt.results_path, in_opt.title + ' fractional difference'), dpi=300)
		plt.close()


def test():
	# simple test function
	pass

# run the main function
if __name__ == '__main__':
	analysis_main()
	# test()
