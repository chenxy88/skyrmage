import numpy as np
import matplotlib.pyplot as plt
import os
import collections
from copy import copy, deepcopy
import re
from scipy.interpolate import interp1d
from scipy.stats import gaussian_kde
from skimage.measure import label
from dataclasses import dataclass

from core.data_parser import DataParser
from core.plotter import Plotter
from image.fourier_analysis import PowerSpectrumCalc
from image.pixel_analysis import PixelAnalysis
from core.util import Vector, ParamsHL, save_obj, Timer, UI_load_json_file
from core.shuju import Shuju


# TODO: Change the grid_spacing to units to um, need to standardise to this
@dataclass
class InputOptions:
	path: str = None
	file_extension: str = None
	grid_spacing: float = None
	title: str = ''
	show_intermediate_steps:bool = False
	invert_contrast:bool = False
	open_close_rep:int = 2
	threshold_radius: int = 51
	focus_disk: bool = False
	local_threshold: bool = False
	manual_threshold_percentile: float = 85.0
	counts_threshold: int = 100
	extreme_val_high: float = None
	extreme_val_low: float = None

def analysis_main():

	# TIMER
	t1 = Timer('my timer', disabled=True)

	#--- CHOOSE INPUT OPTION ---#
	input_dict = UI_load_json_file()

	if input_dict is None:
		# user chose to cancel
		return

	in_opt = InputOptions(**input_dict)

	# import files based on filter
	path = in_opt.path
	filename_list = os.listdir(path)

	filename_list_filtered = []
	field_arr = []
	data_list = []

	# filter based on criteria
	for filename in filename_list:
		if filename.endswith(in_opt.file_extension):
			filename_list_filtered.append(filename)

	filename_list_filtered.sort()
	num_files = len(filename_list_filtered)

	# loop through all files in list
	for ind, filename in enumerate(filename_list_filtered):
		
		# progress 
		print('Processing... %0.1f%% complete.  Loading %s.' %(float(ind/num_files)*100, filename))

		# prepare to plot
		data_to_show_2D_list = []

		# import data
		raw_data = DataParser.import_image(path=path, filename=filename, grid_spacing=in_opt.grid_spacing)
		t1.display_time_reset('imported file')

		field_val = int(re.search('(\+?-?\d+)\s*Oe', filename).group(1))
		field_arr.append(field_val)

		# remove extension from filename
		filename, ext = os.path.splitext(filename)

		raw_data.metadata.set_attrs(label = filename+' 00 Raw data', axes_labels = Vector('Distance (mm)','Distance (mm)','Intensity'))
		data_to_show_2D_list.append(raw_data)

		thresholded_data = PixelAnalysis.skimage_filters_threshold_wrapper(raw_data, invert= in_opt.invert_contrast,
												r=in_opt.threshold_radius, focus_disk= in_opt.focus_disk, local_threshold=in_opt.local_threshold,
												manual_threshold_percentile = in_opt.manual_threshold_percentile)
		thresholded_data.metadata.set_attrs(label = filename+' 01 Thresholded data')
		data_to_show_2D_list.append(thresholded_data)
		t1.display_time_reset('thresholded')

		connectedness_filtered_data = PixelAnalysis.filter_by_connectedness(thresholded_data, counts_threshold = in_opt.counts_threshold)
		connectedness_filtered_data.metadata.set_attrs(label = filename+' 02a Connectedness-filtered data')
		data_to_show_2D_list.append(connectedness_filtered_data)
		t1.display_time_reset('filtered by connectivity')

		opened_closed_data = PixelAnalysis.skimage_morphology_open_close_wrapper(connectedness_filtered_data, close_first = True, eros_dil_seq=[1,1,-1,-1])
		opened_closed_data.metadata.set_attrs(label=filename + ' 02b Opened-closed data')
		data_to_show_2D_list.append(opened_closed_data)
		t1.display_time_reset('open closed')

		# count domain sizes, in number of pixels
		labelled_data, max_label = label(opened_closed_data.data, background=0, return_num=True, connectivity=2)
		labels, domain_area = np.unique(labelled_data, return_counts=True)
		# always delete [0] element, cos it is the background
		domain_area = domain_area[1:]

		data_list.append(deepcopy(domain_area))

		# show intermediate steps if required
		if in_opt.show_intermediate_steps:

			# plot combined pseudocolour plot
			for data_to_show in data_to_show_2D_list:
				tmp_fig = plt.figure()
				Plotter.pcolormesh(data=data_to_show, equal_aspect=True)
				plt.savefig(os.path.join(in_opt.path, data_to_show.metadata.label+'.tiff'), dpi = 300, format = 'tif')
				plt.close(tmp_fig)

			# other line plots
			# if not domain_width_distr_shuju is None:
			# 	tmp_fig = plt.figure()
			# 	Plotter.plot(domain_width_distr_shuju)
			# 	plt.savefig(os.path.join(in_opt.path, domain_width_distr_shuju.metadata.label), dpi=300)
			# 	plt.close(tmp_fig)

	#--- LOOP ENDED ---#

	extreme_vals = ParamsHL(-np.inf, np.inf)
	data_list2 = []
	for tmp_data in data_list:
		tmp_data2 = np.log10(tmp_data * in_opt.grid_spacing**2)
		extreme_vals.update_extreme_value(np.max(tmp_data2))
		extreme_vals.update_extreme_value(np.min(tmp_data2))
		data_list2.append(deepcopy(tmp_data2))

	# overwrite extreme_vals if both limits are specified in input
	if in_opt.extreme_val_high and in_opt.extreme_val_low:
		extreme_vals = ParamsHL(in_opt.extreme_val_high, in_opt.extreme_val_low)

	log_domain_area_list = []
	for tmp_data in data_list2:
		# generate histogram for domain sizes
		x_axis = np.linspace(extreme_vals.low, extreme_vals.high, 100)
		hist_kernel = gaussian_kde(tmp_data, bw_method=0.25)
		hist_shuju = Shuju(hist_kernel(x_axis))
		hist_shuju.metadata.axes = x_axis
		hist_shuju.metadata.axes_labels = Vector('log(area(nm^2))', 'Distribution density')
		log_domain_area_list.append(deepcopy(hist_shuju))

	# plot combined pseudocolour plot
	combined_data = Shuju.shuju1d_list_to_2d(log_domain_area_list, y_axis=field_arr)
	# combined_data.data = np.log10(combined_data.data)
	combined_data.metadata.axes_labels = Vector('Magnetic field (Oe)', 'log(area(nm^2))', 'Distribution density (A.U.)')
	combined_data.metadata.label = in_opt.title

	# rotate the data
	combined_data.data = np.rot90(combined_data.data)
	tmp_x = np.rot90(combined_data.metadata.axes.x)
	combined_data.metadata.axes.x = np.rot90(combined_data.metadata.axes.y)
	combined_data.metadata.axes.y = tmp_x

	Plotter.pcolormesh(data=combined_data, equal_aspect=False)

	plt.savefig(os.path.join(path, in_opt.title + ' domain area distribution'), dpi=300)
	t1.display_time_reset('plotted and saved figures')

def test():
	# simple test function
	reps = 100000
	# len_arr = 200
	# arr = np.zeros(len_arr)
	# for i in range(0, reps):
	# 	arr_add, len_x = ensure_right_heavy(np.random.rand(len_arr))
	# 	arr = arr+arr_add
	#
	# arr = arr/reps
	#
	# plt.plot(arr)
	# plt.show()

# run the main function
if __name__ == '__main__':
	analysis_main()
	# test()
