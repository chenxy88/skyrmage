import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import os
import math
import re
from scipy import interpolate
from scipy.interpolate import interp1d
from dataclasses import dataclass
from copy import deepcopy
from multiprocessing import Pool
import pickle
from scipy.ndimage.filters import median_filter, gaussian_filter

from core.data_parser import DataParser
from core.plotter import Plotter, PlotSettings
from core.util import Vector, ParamsHL, save_obj, UI_load_json_file, listdir_filtered, Timer, update_obj_from_dict_recursively, outer_product_object_list, flatten
from algorithms.tvregdiff import TVRegDiff
from scipy.signal import savgol_filter

NUM_PROCS = 4

@dataclass
class InputOptions:
	mh_path: [str] = None
	forc_path: str = None
	labels: str = None
	experimental_results_path_filename: str = ''
	title: str = ''
	results_path: str = ''
	json_file_ext: str = '.json'
	input_file_ext: str = '.txt'
	legend: bool = True
	time_column_num: int = 0
	field_column_num: int = 6
	mag_column_num: int = 3
	energy_column_num: int = 7
	topo_column_num: int = 12
	zoom_in_field_region: float = 0.05
	H_Hr_num_points: int = 50
	delta_H_Hr_mT: int = 10
	simulation_area_um2: float = 2.25
	continuous: bool = False # whether it was a continuous B sweep
	continuous_smooth: int = 1 # Smoothing windows
	sweep_and_wait: bool = False # a sim mode where a sweep is performed from one field val to another, followed by a small wait time
	skip_header: int = 3
	delta_quantity: float = 9.8e-12 # use this as definition of no change in quantity when relaxing (used to be delta_time)
	main_result_pickle_filename: str = 'data.pickle'
	dmdh_pickle_filename: str = 'dmdh.pickle'
	do_analysis: bool = True
	do_dmdh: bool = True
	do_plotting: bool = True

def analysis_main():

	# This analysis routine can plot several M(H) in one graph and

	# --- CHOOSE INPUT OPTION --- #
	input_dict_list = UI_load_json_file()

	if not input_dict_list:
		# user chose to cancel
		return
	num_files = len(input_dict_list)

	# GIANT LOOP OVER ALL INPUT OPTIONS
	for ind,input_dict in enumerate(input_dict_list):

		# progress
		print('Processing... %0.1f%% complete.' % float(ind / num_files * 100.0))

		in_opt_list = InputOptions()

		# this analysis only takes a single in_opt, for now
		update_obj_from_dict_recursively(in_opt_list, input_dict)
		# convert a single input dict into an array of input_dicts by flattens and taking outer product of all arrays
		in_opt_list = flatten([outer_product_object_list(in_opt_list)])

		in_single:InputOptions = in_opt_list[0]
		# split labels string into array
		labels = in_single.labels.split(';')

		# prepare colormap
		cmap = matplotlib.cm.get_cmap('viridis')#nipy_spectral')
		plt.style.use(os.path.abspath(os.path.join(os.getcwd(), '..', 'misc', 'default.mplstyle')))

		matplotlib.rcParams['lines.linewidth'] = 1
		matplotlib.rcParams['lines.markersize'] = 1
		if in_single.continuous:
			matplotlib.rcParams['lines.markersize'] = 0.5

		# plt.style.use(PlotSettings.style)
		all_mh_results = []
		all_dmdh_results = []
		all_energy_results = []
		all_topo_results = []

		# the H,Hr matrix, which will be used to find rho
		m_H_Hr = np.zeros((in_single.H_Hr_num_points, in_single.H_Hr_num_points))
		Energy_H_Hr = np.zeros((in_single.H_Hr_num_points, in_single.H_Hr_num_points))
		Topo_H_Hr = np.zeros((in_single.H_Hr_num_points, in_single.H_Hr_num_points))
		# sim_params_list = []

		forc_run = in_single.forc_path != ''

		if in_single.do_analysis:

			# single file data
			single_mh_array = None
			single_energy_array = None
			single_topo_array = None

			# --- Read in the M(H) loops first --- #
			for input_ind, in_opt in enumerate(in_opt_list):
				# get list of files
				result_file_list = listdir_filtered(in_opt.mh_path, in_opt.input_file_ext)

				# read in all these files as one dataset
				(single_mh_array, single_energy_array, single_topo_array) = read_data_from_mumax_table(in_opt, path= in_opt.mh_path, result_file_list=result_file_list)

			all_mh_results.append(single_mh_array)
			all_energy_results.append(single_energy_array)
			all_topo_results.append(single_topo_array)

			# original mh_loop for comparison with FORC
			# mh_loop_interp = interp1d(single_mh_array[:, 0], single_mh_array[:, 1], kind='linear', fill_value='extrapolate')

			# store MH loop in m_H_Hr, one taking the 1st result, in the diagonal
			# quick method to find index
			def find_m_H_Hr_ind (h_value):
				return int(-np.round(h_value*1e3/in_single.delta_H_Hr_mT))

			for (h_val, m_val) in single_mh_array:
				h_ind = find_m_H_Hr_ind(h_val)
				if 0 <= h_ind < in_single.H_Hr_num_points:
					m_H_Hr[h_ind, h_ind] = m_val

			for (h_val, en_val) in single_energy_array:
				h_ind = find_m_H_Hr_ind(h_val)
				if 0 <= h_ind < in_single.H_Hr_num_points:
					Energy_H_Hr[h_ind, h_ind] = en_val

			for (h_val, tp_val) in single_topo_array:
				h_ind = find_m_H_Hr_ind(h_val)
				if 0 <= h_ind < in_single.H_Hr_num_points:
					Topo_H_Hr[h_ind, h_ind] = tp_val

			# TMP: do not plot MH. Reset data
			# all_mh_results = []
			# all_dmdh_results = []
			# all_energy_results = []
			# all_topo_results = []

			# --- Read in the FORC loops next --- #
			Hr_list = []

			if forc_run:

				forc_files_dict = dict()

				# get list of files
				result_file_list = listdir_filtered(in_single.forc_path, in_single.input_file_ext)
				for filename in result_file_list:
					# find the reversal field
					Hr = int(re.search('_(\+?-?\d+)\s*mT', filename).group(1))
					Hr_list.append(Hr)
					# store it in a List in dict
					if not Hr in forc_files_dict:
						forc_files_dict[Hr] = [filename]
					else:
						forc_files_dict[Hr].append(filename)

				# remove duplicates and sort
				Hr_list = list(set(Hr_list))
				Hr_list.sort()

				pool_input_list = []

				# build pool input list
				for Hr_val in Hr_list:

					pool_input_list.append((in_single, in_single.forc_path, forc_files_dict[Hr_val]))

				# process domains with multiprocessing
				with Pool(processes=NUM_PROCS) as p:
					# read in all these files as one dataset
					pool_output_list = p.starmap(read_data_from_mumax_table, pool_input_list)

				# unpack results
				for ind,pool_output in enumerate(pool_output_list):

					(single_mh_array, single_energy_array, single_topo_array) = pool_output

					# Hr = float(Hr_list[ind])/1e3
					# single_mh_array = np.insert(single_mh_array,0,[Hr, mh_loop_interp(Hr)], axis=0)

					# append to data collections
					all_mh_results.append(single_mh_array)
					all_energy_results.append(single_energy_array)
					all_topo_results.append(single_topo_array)

					Hr_val = Hr_list[ind]
					labels.append('Hr:%smT'%str(Hr_val))

					# store MH loop in m_H_Hr
					hr_ind = find_m_H_Hr_ind(Hr_val*1e-3)

					for (h_val, m_val) in single_mh_array:
						h_ind = find_m_H_Hr_ind(h_val)
						if 0 <= h_ind < in_single.H_Hr_num_points:
							m_H_Hr[h_ind, hr_ind] = m_val

					for (h_val, en_val) in single_energy_array:
						h_ind = find_m_H_Hr_ind(h_val)
						if 0 <= h_ind < in_single.H_Hr_num_points:
							Energy_H_Hr[h_ind, hr_ind] = en_val

					for (h_val, tp_val) in single_topo_array:
						h_ind = find_m_H_Hr_ind(h_val)
						if 0 <= h_ind < in_single.H_Hr_num_points:
							Topo_H_Hr[h_ind, hr_ind] = tp_val


				# save M(H,Hr) data
				np.savetxt(os.path.join(in_single.results_path, in_single.title + '_M_H_Hr.txt'), m_H_Hr, fmt='%.8f', delimiter='\t')
				np.savetxt(os.path.join(in_single.results_path, in_single.title + '_Energy_H_Hr.txt'), Energy_H_Hr, fmt='%g', delimiter='\t')
				np.savetxt(os.path.join(in_single.results_path, in_single.title + '_Topo_H_Hr.txt'), Topo_H_Hr, fmt='%g', delimiter='\t')

				# add a 0 Hr to represent the MH loop
				Hr_list = [0] + Hr_list

				# dump with pickle
				obj_input_path = os.path.join(in_single.results_path, in_single.main_result_pickle_filename)
				with open(obj_input_path, 'wb') as pickleFile:
					pickle.dump((Hr_list, labels, all_mh_results, all_energy_results, all_topo_results), pickleFile)

		else:
			# load data directly with pickle
			obj_input_path = os.path.join(in_single.results_path, in_single.main_result_pickle_filename)
			with open(obj_input_path, 'rb') as pickleFile:
				(Hr_list, labels, all_mh_results, all_energy_results, all_topo_results) = pickle.load(pickleFile)

		if in_single.do_dmdh:
				# find dMdH
				pool_input_list = []
				# build pool input list
				for (ind,mh_result) in enumerate(all_mh_results):
					pool_input_list.append((mh_result, labels[ind], in_single.continuous, in_single.continuous_smooth))

				# process domains with multiprocessing
				with Pool(processes=NUM_PROCS) as p:
					# read in all these files as one dataset
					pool_output_list = p.starmap(find_dmdh, pool_input_list)

				# unpack results
				all_dmdh_results = pool_output_list

				# remove the dmdh of the mh major loop
				forc_dmdh_results = all_dmdh_results[1:]
				H_Hr_axis_len = len(forc_dmdh_results) + 1

				# fill in dmdh matrix
				# last row of dmdh_H_Hr will be zero
				dmdh_H_Hr = np.zeros((H_Hr_axis_len, H_Hr_axis_len))
				for (ind,dmdh_result) in enumerate(forc_dmdh_results):
					dmdh_H_Hr[ind,ind:] = dmdh_result[:,1]

				# set [0,0] element to zero, cos we can't find d/dhr at that h
				dmdh_H_Hr[0,0] = 0

				# find d2mdhrdh
				pool_input_list = []
				# ignore [0] element
				for ind in range(1, H_Hr_axis_len):
					tmp_dmdh = dmdh_H_Hr[0:ind+1, ind]
					tmp_Hr = np.arange(0, ind+1)*in_single.delta_H_Hr_mT*1e-3
					pool_input_list.append((np.rot90(np.vstack((tmp_dmdh, tmp_Hr)),3), labels[ind], in_single.continuous, in_single.continuous_smooth))

				# process domains with multiprocessing
				with Pool(processes=NUM_PROCS) as p:
					# read in all these files as one dataset
					pool_output_list = p.starmap(find_dmdh, pool_input_list)

				# fill in d2mdhrdh_H_Hr matrix
				d2mdhrdh_H_Hr = np.zeros((H_Hr_axis_len, H_Hr_axis_len))
				for (ind,d2mdhrdh) in enumerate(pool_output_list):
					d2mdhrdh_H_Hr[0:ind+2, ind+1] = d2mdhrdh[:,1]


				# reverse the sign of dmdh
				# for dmdh_result in all_dmdh_results:
				# 	dmdh_result[:,1] = -dmdh_result[:,1]

				# dump with pickle
				obj_input_path = os.path.join(in_single.results_path, in_single.dmdh_pickle_filename)
				with open(obj_input_path, 'wb') as pickleFile:
					pickle.dump((all_dmdh_results,d2mdhrdh_H_Hr,H_Hr_axis_len), pickleFile)

		else:
			# load data directly with pickle
			obj_input_path = os.path.join(in_single.results_path, in_single.dmdh_pickle_filename)
			with open(obj_input_path, 'rb') as pickleFile:
				all_dmdh_results,d2mdhrdh_H_Hr,H_Hr_axis_len = pickle.load(pickleFile)


		if in_single.do_plotting:
			# find gradients
			# d1 = np.gradient(m_H_Hr)
			# d2 = np.gradient(d1[0])
			# d2m_dHdHr = d2[1]

			# load the experimentals results
			experimental_data = DataParser.import_plaintxt(path_and_filename=in_single.experimental_results_path_filename, delimiter='\t', skip_header=0)
			experimental_results_path_dMdH = in_single.experimental_results_path_filename.split('.')[0]+'_dMdH.txt'
			experimental_data_dMdH = DataParser.import_plaintxt(path_and_filename=experimental_results_path_dMdH, delimiter='\t', skip_header=0)

			# TMP: do not plot MH. Reset data
			# experimental_data.data = None

			# plot M(H) as folded
			plot_mh(all_mh_results, experimental_data.data, labels, cmap, in_single, in_single.title + ' mh folded', fold_negative_xaxis=True, fold_yaxis_too=True)

			# if not forc_run:
			plot_mh(all_dmdh_results, experimental_data_dMdH.data, labels, cmap, in_single, in_single.title + ' dmdh folded', fold_negative_xaxis=True, y_label='dM/dH')
			plot_mh(all_dmdh_results, experimental_data_dMdH.data, labels, cmap, in_single, in_single.title + ' dmdh', fold_negative_xaxis=False, y_label='dM/dH')

			# plot mh as unfolded
			plot_mh(all_mh_results, experimental_data.data, labels, cmap, in_single, in_single.title + ' mh', forc_run=forc_run, Hr_list=Hr_list)

			plot_mh(all_energy_results, None, labels, cmap, in_single, in_single.title + ' total energy', y_label='Total energy (J)',
					fold_negative_xaxis=False)

			plot_mh(all_topo_results, None, labels, cmap, in_single, in_single.title + ' topological charge', y_label='Topological charge density (1/um^2)',
					fold_negative_xaxis=False, legend_loc ='upper right', forc_run=forc_run, Hr_list=Hr_list)

			energy_diff, energy_fractional_diff = find_energy_difference(all_energy_results)

			plot_mh(energy_diff, None, labels, cmap, in_single, in_single.title + ' total energy difference', y_label='Total energy difference (J)',
					fold_negative_xaxis=False, forc_run=forc_run, Hr_list=Hr_list)

			plot_mh(energy_fractional_diff, None, labels, cmap, in_single, in_single.title + ' total fractional energy difference', y_label='Total fractional energy difference',
					fold_negative_xaxis=False, forc_run=forc_run, Hr_list=Hr_list)

			# plot pcolor M(H,Hr) and related

			# clip very high values
			max_rho = 80
			sat_ind = np.abs(d2mdhrdh_H_Hr)>max_rho
			d2mdhrdh_H_Hr[sat_ind] = np.sign(d2mdhrdh_H_Hr[sat_ind])*max_rho

			# median filter
			d2mdhrdh_H_Hr = -d2mdhrdh_H_Hr / np.max(np.abs(d2mdhrdh_H_Hr))
			d2mdhrdh_H_Hr = gaussian_filter(d2mdhrdh_H_Hr, sigma=1)
			for ind in range(0, H_Hr_axis_len-1):
				d2mdhrdh_H_Hr[ind+1:,ind] = np.zeros(H_Hr_axis_len-1-ind)

			H_Hr_axis = (np.arange(0.0, (H_Hr_axis_len+1.0))-0.5)* in_single.delta_H_Hr_mT
			H_Hr_gridx, H_Hr_gridy = np.meshgrid(H_Hr_axis,H_Hr_axis)

			tmp_fig = plt.figure(figsize=(10, 8))

			plt.pcolormesh(H_Hr_gridx, H_Hr_gridy,np.transpose(np.rot90(d2mdhrdh_H_Hr,2)), cmap='bwr', vmin=-1, vmax=1, )
			plt.xlabel('Hr (mT)')
			plt.ylabel('H (mT)')

			plt.savefig(os.path.join(in_single.results_path, in_single.title + ' rho'))
			plt.close(tmp_fig)


def find_dmdh (data_in: np.ndarray, label:str, simple_method:bool = True, cont_smooth_window = 1):
	data = deepcopy(data_in)

	print('Calculating dM/dH of %s.'%label)

	# simple method
	if simple_method:
		dmdh_y = np.gradient(data[:, 1], data[:, 0])

	# advanced method
	else:
		# grid spacing
		dx = (data[-1,0] - data[0,0])/(len(data[:,0])-1)
		#  alph: Regularization parameter.  This is the main parameter to fiddle with.
		# 1e-4 works ok
		dmdh_y = TVRegDiff(data[:, 1], 20, alph = 2e-4, dx=dx, ep=1e-2, scale='large', plotflag=0, diagflag=0)

		# dmdh_y = np.gradient(data[:, 1], data[:, 0])
		# # apply additional smooothing after gradient
		# dmdh_y = savgol_filter(dmdh_y, cont_smooth_window, 1, mode='mirror')

	# return results
	return np.rot90(np.vstack((dmdh_y, data[:, 0])), 3)


def find_energy_difference (all_energy_results):
	# find the energy different from the [0] results
	# build energy baseline dictionary, with field in mT as key, for quick access
	baseline = dict()
	baseline_data = deepcopy(all_energy_results[0])
	# convert field to mT and int
	baseline_data[:,0] = (np.round(baseline_data[:,0]*1e3)).astype(int)
	for ind in range(0, len(baseline_data)):
		baseline[baseline_data[ind, 0]] = baseline_data[ind, 1]

	energy_diff = deepcopy(all_energy_results)
	energy_fractional_diff = deepcopy(all_energy_results)

	# subtract off baseline
	for outer_ind, energy_result in enumerate(energy_diff):
		for ind in range(0, len(energy_result)):
			energy_result[ind,1] -= baseline[int(round(energy_result[ind,0]*1e3))]
		# find the fractional difference
		energy_fractional_diff[outer_ind][:,1] = energy_result[:,1]/np.abs(all_energy_results[outer_ind][:,1])

	return energy_diff, energy_fractional_diff


def read_data_from_mumax_table(in_opt, path, result_file_list):

	# options from in_opt
	delta_quantity = in_opt.delta_quantity
	cont = in_opt.continuous
	cont_smooth_window = in_opt.continuous_smooth
	delta_H_Hr_mT = in_opt.delta_H_Hr_mT
	skip_header = in_opt.skip_header
	sweep_and_wait = in_opt.sweep_and_wait

	# prepare data structure to store output
	# output_dict = dict()
	single_mh_array = None
	single_energy_array = None
	single_topo_array = None

	# window size for smoothing
	window_size = cont_smooth_window

	# read_file_timer = Timer('Read file')
	# --- loop through all files in list --- #
	for ind, filename in enumerate(result_file_list):

		# print progress
		print('Reading in %s.'%filename)

		# load the data in
		tmp_data = DataParser.import_plaintxt(path=path, filename=filename, delimiter='\t', skip_header=skip_header)

		# copy out arrays for easy reference
		# time_arr = tmp_data.data[:, in_opt.time_column_num]

		mag_arr = np.array(tmp_data.data[:, in_opt.mag_column_num])
		energy_arr = np.array(tmp_data.data[:, in_opt.energy_column_num])
		# find topological charge density
		topo_arr = np.array(tmp_data.data[:, in_opt.topo_column_num])/in_opt.simulation_area_um2
		field_param_arr = tmp_data.data[:, in_opt.field_column_num]

		# setup output variables
		single_mh = []
		single_energy = []
		single_topo = []
		field_param_final = []

		if not cont:

			if not sweep_and_wait:
				# use time_arr to find the positions of the relaxes quantities, since time does not increment when relaxing
				time_arr = np.array(tmp_data.data[:, in_opt.time_column_num])
				time_arr_displaced = np.insert(time_arr[0:-1], 0, np.inf)
				final_positions = (np.abs(time_arr - time_arr_displaced) < delta_quantity) #& (time_arr > small)
				single_mh = mag_arr[final_positions]
				single_energy = energy_arr[final_positions]
				single_topo = topo_arr[final_positions]

				field_param_final = field_param_arr[final_positions]

			else:
				# find positions where field does not change (much)
				field_arr = np.array(tmp_data.data[:, in_opt.field_column_num])
				field_arr_displaced = np.insert(field_arr[0:-1], 0, np.inf)
				field_arr_displaced[-1] = np.inf
				wait_indices =  np.arange(0, len(field_arr))[(np.abs(field_arr - field_arr_displaced) < delta_quantity)]
				# find positions where there are jumps in wait_positions (i.e. a field sweep)
				wait_indices_displaced = np.insert(wait_indices[0:-1], 0, -99)
				wait_indices_displaced2 = np.append(wait_indices[1:], -99)
				wait_start_stop_indices = wait_indices[(np.abs(wait_indices - wait_indices_displaced) > 1) | (np.abs(wait_indices - wait_indices_displaced2) > 1)]
				# check that wait_start_stop_indices come in pairs
				assert (len(wait_start_stop_indices) % 2 == 0)
				# loop thru each pair of start and stop indices
				for ind in range(0, int(round(len(wait_start_stop_indices)/2))):
					# average the later half of the wait time
					start_ind = int(round((wait_start_stop_indices[ind*2] + wait_start_stop_indices[ind*2+1])/2))
					stop_ind = wait_start_stop_indices[ind*2+1]
					average_indices = np.arange(start_ind, stop_ind+1)
					single_mh.append(np.mean(mag_arr[average_indices]))
					single_energy.append(np.mean(energy_arr[average_indices]))
					single_topo.append(np.mean(topo_arr[average_indices]))
					field_param_final.append(np.mean(field_param_arr[average_indices]))

		else:
			# interpolate to nearest delta_H_Hr_mT
			# min (incl) and max (excl) points in mT.
			field_min = (round(np.min(field_param_arr)*1e3/delta_H_Hr_mT)+1)*delta_H_Hr_mT
			field_max = (round(np.max(field_param_arr)*1e3/delta_H_Hr_mT)+1)*delta_H_Hr_mT
			# field_param_final in T
			field_param_final = np.arange(field_min, field_max, delta_H_Hr_mT)*1e-3
			# perform smoothing for continuous mode
			single_mh = savgol_filter(mag_arr, window_size, 1, mode='mirror')
			single_energy = savgol_filter(energy_arr, window_size, 1, mode='mirror')
			single_topo = savgol_filter(topo_arr, window_size, 1, mode='mirror')
			# interpolate to field_param_final field positions
			single_mh = interpolate.interp1d(field_param_arr, single_mh, kind='linear', fill_value='extrapolate')(field_param_final)
			single_energy = interpolate.interp1d(field_param_arr, single_energy, kind='linear', fill_value='extrapolate')(field_param_final)
			single_topo = interpolate.interp1d(field_param_arr, single_topo, kind='linear', fill_value='extrapolate')(field_param_final)

		# append to output arrays
		single_mh_array = np.rot90(np.vstack((single_mh, field_param_final)), 3)
		single_energy_array = np.rot90(np.vstack((single_energy, field_param_final)), 3)
		single_topo_array = np.rot90(np.vstack((single_topo, field_param_final)), 3)

	return single_mh_array, single_energy_array, single_topo_array

# sim_params_list.append(deepcopy(sim_params))

def plot_mh(all_mh_results:[], experimental_data, labels, cmap, in_opt:InputOptions,
			output_name, y_label='mz', fold_negative_xaxis:bool = False, fold_yaxis_too:bool=False, field_range:float = np.inf,
			legend_loc='lower right', forc_run = False, Hr_list=None, cont = False):

	# prepare color
	color_ind_arr = np.linspace(0, 1, len(all_mh_results) + 2)

	tmp_fig = plt.figure(figsize=(10,8))

	if not experimental_data is None:
		# plot experimental data
		x_arr = experimental_data[:, 0]
		y_arr = experimental_data[:, 1]
		y_arr = y_arr[(x_arr < field_range) & (x_arr > -field_range)]
		x_arr = x_arr[(x_arr < field_range) & (x_arr > -field_range)]

		plot_folded(x_arr, y_arr, cmap, color_ind_arr, fold_negative_xaxis, fold_yaxis_too, 0, 0, ['Experimental data'])

	# plot the ZF value as a function of Hr for FORC runs
	ZF_quantity = []
	small = 1e-6
	if forc_run:
		assert (len(all_mh_results) == len(Hr_list))

	# plot simulation data
	for ind, single_mh_array in enumerate(all_mh_results):
		# plot single M(H) loop
		# sort in ascending order by field (0th column)
		single_mh_array = single_mh_array[single_mh_array[:,0].argsort()]
		x_arr = single_mh_array[:,0]
		y_arr = single_mh_array[:,1]

		# collect ZF quantity
		if forc_run:
			ZF_quantity.append(y_arr[np.abs(x_arr) < small])

		y_arr = y_arr[(x_arr < field_range) & (x_arr > -field_range)]
		x_arr = x_arr[(x_arr < field_range) & (x_arr > -field_range)]

		plot_folded(x_arr, y_arr, cmap, color_ind_arr, fold_negative_xaxis, fold_yaxis_too, ind, 1, labels)

		# plt.plot(x_arr_neg, y_arr_neg, 'x-', color=cmap(color_ind_arr[ind + 1]), label='unsat %.1f ns@%d K, D=%.1f' % (
		# sim_params_list[ind].tune.temperature_run_time * 1e9, sim_params_list[ind].tune.temperature, sim_params_list[ind].mat.dmi_interface))

	plt.xlabel('External field (T)')
	plt.ylabel(y_label)

	if in_opt.legend:
		plt.legend(loc=legend_loc)

	plt.savefig(os.path.join(in_opt.results_path, output_name))
	plt.close(tmp_fig)

	# sort ZF quantity
	if forc_run:
		Hr_arr = np.array(Hr_list)*1e-3
		ZF_quantity_arr = np.array(ZF_quantity)
		sort_order = Hr_arr.argsort()
		Hr_arr = Hr_arr[sort_order]
		ZF_quantity_arr = ZF_quantity_arr[sort_order]

		# plot ZF quantity
		tmp_fig = plt.figure(figsize=(10, 8))
		plt.plot(Hr_arr, ZF_quantity_arr, 'o-', color=cmap(color_ind_arr[0]))
		plt.xlabel('Reversal field (T)')
		plt.ylabel(y_label)
		plt.savefig(os.path.join(in_opt.results_path, output_name+' ZF Hr'))
		plt.close(tmp_fig)


def plot_folded(x_arr, y_arr, cmap, color_ind_arr, fold_negative_xaxis, fold_yaxis_too, ind, color_ind_offset, labels):
	if fold_negative_xaxis:
		x_arr_pos = x_arr[x_arr >= 0]
		y_arr_pos = y_arr[x_arr >= 0]

		# invert negative fields
		x_arr_neg = -x_arr[x_arr <= 0]
		if fold_yaxis_too:
			y_arr_neg = -y_arr[x_arr <= 0]
		else:
			y_arr_neg = y_arr[x_arr <= 0]

		try:
			color_ind_grid = color_ind_arr[1] - color_ind_arr[0]
		except:
			color_ind_grid = 0

		plt.plot(x_arr_pos, y_arr_pos, 'o-', color=cmap(color_ind_arr[ind + color_ind_offset]), label=labels[ind])
		# offset the color of the neg from the positive
		plt.plot(x_arr_neg, y_arr_neg, '^-', color=cmap(color_ind_arr[ind + color_ind_offset] + 0.5*color_ind_grid))  # , label=labels[ind]+' unsat')

	else:
		plt.plot(x_arr, y_arr, 'o-', color=cmap(color_ind_arr[ind + 1]), label=labels[ind])


def test():
	# simple test function
	pass

# run the main function
if __name__ == '__main__':
	analysis_main()
	# test()
