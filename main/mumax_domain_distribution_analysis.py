import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import os
import math
import re
from scipy import interpolate
from dataclasses import dataclass
from copy import deepcopy
from skimage.measure import label, regionprops
from multiprocessing import Pool

from core.data_parser import DataParser
from core.shuju import Shuju, MetaData
from image.pixel_analysis import PixelAnalysis
from core.plotter import Plotter, PlotSettings
from core.util import Vector, ParamsHL, save_obj, UI_load_json_file, listdir_filtered, Timer, update_obj_from_dict_recursively
from image.pixel_analysis_cythonized import filter_by_labels
from image.fourier_analysis import PowerSpectrumCalc

NUM_PROCS = 4

class Const:
	sk_density_str: str = ' sk density'
	sk_size_str: str = ' sk size'
	period_str: str = ' period'
	datafile_ext: str = '.txt'

@dataclass
class InputOptions:
	ovf_path: str = None
	ovf_ext: str = '.ovf'
	area_threshold_nm2: float = 10000
	aspect_ratio_threshold: float = 0.7
	circularity_threshold: float = 0.7
	intermediate_output_path: str = ''
	grid_spacing: float = 0.004 # X,Y size of each simulation cell in um
	title: str = ''
	results_path: str = ''
	simulation_area_um2: float = 2.25
	do_analysis: bool = True
	do_plotting: bool = True
	xlabel: str = 'Reversal field (T)'
	freq_remove: float = 0.5
	freq_cutoff_high: float = 15
	freq_cutoff_low: float = 0
	show_intermediate: bool = True
	counts_threshold: int = 10
	median_filter_radius: int = 3

def analysis_main():

	cmap = matplotlib.cm.get_cmap('viridis')
	color_ind_arr = np.linspace(0, 1, 2 + 2)
	const = Const()

	# --- CHOOSE INPUT OPTION --- #
	input_dict_list = UI_load_json_file()

	if not input_dict_list:
		# user chose to cancel
		return

	# --- BIG OUTSIDE LOOP THRU LIST OF INPUT JSON FILES --- #
	for input_dict in input_dict_list:

		in_opt = InputOptions()
		update_obj_from_dict_recursively(in_opt, input_dict)

		if in_opt.do_analysis:
			# import files based on filter
			ovf_list = listdir_filtered(in_opt.ovf_path, in_opt.ovf_ext)

			# prepare to plot
			H_field_arr = []
			Period_arr = []
			Up_domains_density_arr = []
			Down_domains_density_arr = []
			Up_domains_size_arr = []
			Down_domains_size_arr = []


			phy_len_cell = in_opt.grid_spacing*1e3 # in nm
			phy_area_cell = (phy_len_cell)**2
			counts_threshold = int(in_opt.area_threshold_nm2/phy_area_cell)

			num_ovf_files = len(ovf_list)

			pool_input_list = []

			# build pool input list
			for ind, filename in enumerate(ovf_list):
				pool_input_list.append((cmap, color_ind_arr, counts_threshold, filename, in_opt, ind, num_ovf_files, phy_len_cell))

			# process domains with multiprocessing
			with Pool(processes=NUM_PROCS) as p:
				pool_output_list = p.starmap(process_domains, pool_input_list, chunksize=math.ceil(len(ovf_list)/40))

			# unpack results
			for pool_output in pool_output_list:
				(H_field, Period, Up_domains_density, Down_domains_density, Up_domains_size, Down_domains_size) = pool_output
				H_field_arr.append(H_field)
				Period_arr.append(Period)
				Up_domains_density_arr.append(Up_domains_density)
				Down_domains_density_arr.append(Down_domains_density)
				Up_domains_size_arr.append(Up_domains_size)
				Down_domains_size_arr.append(Down_domains_size)


			# convert to np arrays to sort
			sk_density = np.rot90(np.vstack((np.array(Down_domains_density_arr),np.array(Up_domains_density_arr), np.array(H_field_arr))),3)
			sk_size =  np.rot90(np.vstack((np.array(Down_domains_size_arr),np.array(Up_domains_size_arr), np.array(H_field_arr))),3)
			period = np.rot90(np.vstack((np.array(Period_arr), np.array(H_field_arr))),3)

			sort_order = np.array(H_field_arr).argsort()

			sk_density = sk_density[sort_order]
			sk_size = sk_size[sort_order]
			period = period[sort_order]

			# save data
			np.savetxt(os.path.join(in_opt.results_path, in_opt.title + const.sk_density_str+const.datafile_ext), sk_density, fmt='%.5f', delimiter='\t',
					   header='Field(T)\tUp domain density(1/um^2)\tDown domain(1/um^2)')
			np.savetxt(os.path.join(in_opt.results_path, in_opt.title + const.sk_size_str+const.datafile_ext), sk_size, fmt='%.5f', delimiter='\t',
					   header='Field(T)\tUp domain diameter\tDown domain')
			np.savetxt(os.path.join(in_opt.results_path, in_opt.title + const.period_str+const.datafile_ext), period, fmt='%.5f', delimiter='\t',
					   header='Field(T)\tPeriod(nm)')

		else:
			# just load data
			sk_density = np.genfromtxt(os.path.join(in_opt.results_path, in_opt.title + const.sk_density_str+const.datafile_ext), delimiter='\t')
			sk_size = np.genfromtxt(os.path.join(in_opt.results_path, in_opt.title + const.sk_size_str+const.datafile_ext), delimiter='\t')
			period = np.genfromtxt(os.path.join(in_opt.results_path, in_opt.title + const.period_str+const.datafile_ext), delimiter='\t')

		if in_opt.do_plotting:

			plt.style.use(os.path.abspath(os.path.join(os.getcwd(),'..','misc','default.mplstyle')))

			# Sk density
			tmp_fig = plt.figure(figsize=(10,8))
			plt.plot(sk_density[:,0], sk_density[:,1], 'o-', color=cmap(color_ind_arr[0]), label='up domain')
			plt.plot(sk_density[:,0], sk_density[:,2], '^-', color=cmap(color_ind_arr[1]), label='down domain')

			plt.xlabel(in_opt.xlabel)
			plt.ylabel('Skyrmion density/um^2')

			# plt.legend(loc='upper right')
			plt.legend(loc='upper left')

			plt.savefig(os.path.join(in_opt.results_path, in_opt.title + const.sk_density_str))
			plt.close(tmp_fig)

			# Sk size
			tmp_fig = plt.figure(figsize=(10, 8))
			plt.plot(sk_size[:,0], sk_size[:,1], 'o-', color=cmap(color_ind_arr[0]), label='up domain')
			plt.plot(sk_size[:,0], sk_size[:,2], '^-', color=cmap(color_ind_arr[1]), label='down domain')

			plt.xlabel(in_opt.xlabel)
			plt.ylabel('Skyrmion diameter (nm)')

			plt.legend(loc='lower left')

			plt.savefig(os.path.join(in_opt.results_path, in_opt.title + const.sk_size_str))
			plt.close(tmp_fig)

			# period
			tmp_fig = plt.figure(figsize=(10, 8))
			plt.plot(period[:, 0], period[:, 1], 'o-', color=cmap(color_ind_arr[0]))

			plt.xlabel(in_opt.xlabel)
			plt.ylabel('Period (nm)')

			plt.savefig(os.path.join(in_opt.results_path, in_opt.title + const.period_str))
			plt.close(tmp_fig)


def process_domains(cmap, color_ind_arr, counts_threshold, filename, in_opt, ind, num_ovf_files, phy_len_cell):

	# progress
	print('Processing... %0.1f%% complete.  Loading %s.' % (float(ind / num_ovf_files) * 100, filename))
	# capture the field or reversal field
	re_search = re.search('Hr_(\+?-?\d+)\s*mT', filename)
	if re_search:
		H_field = float(re_search.group(1)) * 1e-3
	else:
		re_search = re.search('_(\+?-?\d+)\s*mT', filename)
		H_field = float(re_search.group(1)) * 1e-3

	# load ovf file
	data = DataParser.import_ovf(path=in_opt.ovf_path, filename=filename, z_layer_number=0, grid_spacing=in_opt.grid_spacing)
	thresholded_data = PixelAnalysis.skimage_filters_threshold_wrapper(data, invert=False, local_threshold=False, global_thresh_finetune=0)
	thresholded_data.metadata.set_attrs(label=filename + ' 01 Thresholded data')
	# data_to_show_2D_list.append(deepcopy(thresholded_data))\
	connectedness_filtered_data = PixelAnalysis.filter_by_connectedness(thresholded_data, counts_threshold=in_opt.counts_threshold)
	connectedness_filtered_data = PixelAnalysis.filter_by_connectedness(connectedness_filtered_data, counts_threshold=in_opt.counts_threshold,
																		invert=True)
	connectedness_filtered_data.metadata.set_attrs(label=filename + ' 02 Connectedness-filtered data')
	median_filtered_data = PixelAnalysis.skimage_filter_median_wrapper(connectedness_filtered_data, median_filter_radius=in_opt.median_filter_radius)
	median_filtered_data.metadata.set_attrs(label=filename + ' 03 Median-filtered data')
	shape_filtered_data, number_of_up_domains, number_of_down_domains, ave_len_up_domains, ave_len_down_domains = filter_by_shape_factor(
		median_filtered_data, counts_threshold=counts_threshold, aspect_ratio_threshold=in_opt.aspect_ratio_threshold,
		circularity_threshold=in_opt.circularity_threshold)
	shape_filtered_data.metadata.set_attrs(label=filename + ' 04 Shape-filtered data')

	# data_to_show_2D_list.append(deepcopy(connectedness_filtered_data))
	data_to_show_2D_list = [thresholded_data, connectedness_filtered_data, median_filtered_data, shape_filtered_data]

	Up_domains_density = number_of_up_domains / in_opt.simulation_area_um2
	Down_domains_density = number_of_down_domains / in_opt.simulation_area_um2
	Up_domains_size = ave_len_up_domains * phy_len_cell
	Down_domains_size = ave_len_down_domains * phy_len_cell

	# find FFT
	freq_amp, fft_data = PowerSpectrumCalc.calc_power_spectrum(data_in=thresholded_data,
															   grid_spacing=Vector(in_opt.grid_spacing, in_opt.grid_spacing),
															   freq_remove=in_opt.freq_remove)
	# save FFT
	Plotter.pcolormesh(data=PowerSpectrumCalc.shift_for_plotting(freq_amp), equal_aspect=True)
	plt.xlim(-in_opt.freq_cutoff_high * 3, in_opt.freq_cutoff_high * 3)
	plt.ylim(-in_opt.freq_cutoff_high * 3, in_opt.freq_cutoff_high * 3)
	plt.savefig(os.path.join(in_opt.intermediate_output_path, filename + ' FFT amp.png'), dpi=300)
	plt.close()
	# create ParamHL freq_cutoff
	freq_cutoff = ParamsHL(in_opt.freq_cutoff_high, in_opt.freq_cutoff_low)

	# try fit radial profile, will fail if it is too uniform
	try:
		[radial_profile, radial_profile_fit, wavevector_fit] = PowerSpectrumCalc.calc_radial_profile(data_in=freq_amp, distance_resolution=0.2,
																									 freq_cutoff=freq_cutoff,
																									 freq_norm=ParamsHL(np.inf, -1),
																									 fit_lognormal_curve=True)
	except:
		# default value for wavevector_fit
		wavevector_fit = 1e3

	# save to output arr
	Period = 1e3 / wavevector_fit # period in nm
	# show intermediate results
	if in_opt.show_intermediate:
		# plot and save the comparison to fit
		tmp_fig = plt.figure(figsize=(10, 8))
		plt.plot(radial_profile.metadata.axes, radial_profile.data, 'o', color=cmap(color_ind_arr[0]), label='data')
		plt.plot(radial_profile_fit.metadata.axes, radial_profile_fit.data, '-', color=cmap(color_ind_arr[0]), label='fit')
		plt.xlabel('Radial spatial frequency (1/um)')
		plt.ylabel('Radially averaged FFT amplitude (A.U.)')
		# put the fitted wavevector on the figure
		text_pos_y = np.min(radial_profile_fit.data) + 0.5 * np.std(radial_profile_fit.data)
		plt.text(freq_cutoff.low + 0.5, text_pos_y, '{:.3f}'.format(wavevector_fit), fontsize=12)
		plt.savefig(os.path.join(in_opt.intermediate_output_path, filename + ' FFT comparison to fit.png'), dpi=300)
		# plt.show()
		plt.close(tmp_fig)

		# plot image-processed pseudocolour plots
		for data_to_show in data_to_show_2D_list:
			# normalisation
			if (np.max(np.abs(data_to_show.data)) > 1.5):
				data_to_show.data = (data_to_show.data + 2) / 4
			Plotter.saveasimage(data=data_to_show, pathnfilename=os.path.join(in_opt.intermediate_output_path, data_to_show.metadata.label) + '.tif',
								normalise=False)

	return H_field, Period, Up_domains_density, Down_domains_density, Up_domains_size, Down_domains_size


def do_filtering(labelled_data, counts_threshold, circularity_threshold):
	labels, counts = np.unique(labelled_data, return_counts=True)
	# remove 0th label because it will be treated as bg for regionprops
	if labels[0] == 0:
		labels = np.array(labels[1:])
		counts = np.array(counts[1:])
	# find properties like centroid, major_axis_length, minor_axis_length
	regions = regionprops(labelled_data)
	label_list = np.array([prop.label for prop in regions])
	# ensure that the two labels array are equal
	assert (np.array_equal(labels, label_list))

	# aspect_ratio_arr = np.array([prop.minor_axis_length/prop.major_axis_length for prop in regions])
	circularity = np.array([ 4*np.pi*prop.area / prop.perimeter**2  for prop in regions])

	# filter = np.multiply(counts < counts_threshold, circularity > circularity_threshold)
	filter = circularity > circularity_threshold
	if np.sum(filter > 0 ):
		ave_count = np.mean(counts[filter])
	else:
		ave_count = 0

	return filter, ave_count

def filter_by_shape_factor(data_in=None, counts_threshold = 100, aspect_ratio_threshold = 0.7, circularity_threshold = 0.7):
	"""
	Threshold the data by the total amount of connected pixels, which is called counts
	"""

	# TIMER
	# t1 = Timer('filter_by_connectedness')

	assert (isinstance(data_in, Shuju))

	data_out = deepcopy(data_in)
	data_arr = data_out.data

	# do labelling
	labelled_data, max_label = label(data_arr+1, background = 0, return_num= True)
	# plus 1 because 0 will be treated as bg for regionprops

	labels, counts = np.unique(labelled_data, return_counts=True)

	filter, ave_area_up_domains = do_filtering(np.multiply(labelled_data, data_arr), counts_threshold, circularity_threshold)
	number_of_up_domains = np.sum(filter)
	filter, ave_area_down_domains = do_filtering(np.multiply(labelled_data, 1 - data_arr), counts_threshold, circularity_threshold)
	number_of_down_domains = np.sum(filter)

	filter, tmp = do_filtering(labelled_data, counts_threshold, circularity_threshold)
	labels_to_keep = labels[filter]

	data_out_arr = np.zeros(np.shape(labelled_data))
	label_lookup = np.zeros([max_label + 1], dtype='int64')

	# convert all to int64 type
	labelled_data = labelled_data.astype('int64')
	data_out_arr = data_out_arr.astype('int64')
	labels_to_keep = labels_to_keep.astype('int64')

	# call the cythonized function
	data_out_arr = filter_by_labels(labelled_data, data_out_arr, labels_to_keep, label_lookup)
	# data_out.data = data_out_arr.astype(int)

	# t1.display_time_reset('filter_by_labels')
	data_arr_pos_neg = (deepcopy(data_arr)-0.5)*2

	data_out_arr  = np.multiply(data_out_arr, data_arr_pos_neg)*2
	data_out.data = data_out_arr

	return data_out, number_of_up_domains, number_of_down_domains, np.sqrt(4*ave_area_up_domains)/np.pi, np.sqrt(4*ave_area_down_domains)/np.pi


# run the main function
if __name__ == '__main__':
	analysis_main()
	# test()
