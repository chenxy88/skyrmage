import numpy as np
import matplotlib.pyplot as plt
import os
import collections
from copy import deepcopy
import re
from dataclasses import dataclass

from core.data_parser import DataParser
from core.plotter import Plotter
from image.fourier_analysis import PowerSpectrumCalc
from core.util import Vector, ParamsHL, UI_load_json_file
from core.shuju import Shuju

@dataclass
class InputOptions:
	path: str = None
	file_extension: str = None
	grid_spacing: float = None
	freq_cutoff_high: float = 15
	freq_cutoff_low: float = 0
	freq_remove: float = 0.5
	B_field_high: float = 2000
	B_field_low: float = -2000
	# max_FT_amp: float = np.inf
	title: str = ''
	output_path: str = ''
	output_file_extension: str = ''
	fit_lognormal_curve: bool = False


def plot_togther(data_list):

	for my_data in data_list:
		Plotter.plot(my_data)

	# plt.legend(loc='upper right')

def main():

	# --- CHOOSE INPUT OPTION --- #
	input_dict_list = UI_load_json_file()

	if not input_dict_list:
		# user chose to cancel
		return

	# --- BIG OUTSIDE LOOP THRU LIST OF INPUT JSON FILES --- #
	for input_dict in input_dict_list:

		in_opt = InputOptions(**input_dict)

		# create ParamHL freq_cutoff
		freq_cutoff = ParamsHL(in_opt.freq_cutoff_high, in_opt.freq_cutoff_low)

		# import files based on filter
		path = in_opt.path
		filename_list = os.listdir(path)

		filename_list_filtered = []
		data_list = []
		field_arr = []

		# filter based on criteria
		for filename in filename_list:
			if filename.endswith(in_opt.file_extension):
				filename_list_filtered.append(filename)

		filename_list_filtered.sort()
		num_files = len(filename_list_filtered)

		# loop through all files in list
		for ind, filename in enumerate(filename_list_filtered):

			# progress
			print('Processing... %0.1f%% complete.  Loading %s.' %(float(ind/num_files)*100, filename))

			raw_data = DataParser.import_image(path=path, filename=filename, grid_spacing= Vector(in_opt.grid_spacing, in_opt.grid_spacing))

			# remove the extension
			filename = filename.split('.')[0]

			# save raw image
			Plotter.pcolormesh(data=raw_data, equal_aspect=True)
			plt.savefig(os.path.join(in_opt.output_path, filename + ' raw'), dpi=300)
			plt.close()

			reg_expr = re.search('(\+?-?\d+)\s*Oe', filename)
			if reg_expr is not None:
				field_arr.append(int(reg_expr.group(1)))
			else:
				# defaults to zero field
				field_arr.append(0)

			# find FFT
			freq_amp, fft_data = PowerSpectrumCalc.calc_power_spectrum(data_in = raw_data, grid_spacing = Vector(in_opt.grid_spacing, in_opt.grid_spacing),
																	   freq_remove = in_opt.freq_remove)

			# save FFT
			Plotter.pcolormesh(data=PowerSpectrumCalc.shift_for_plotting(freq_amp), equal_aspect=True)
			plt.xlim(-in_opt.freq_cutoff_high * 3, in_opt.freq_cutoff_high * 3)
			plt.ylim(-in_opt.freq_cutoff_high * 3, in_opt.freq_cutoff_high * 3)
			plt.savefig(os.path.join(in_opt.output_path, filename + ' FFT amp'), dpi=300)
			plt.close()

			# TEST DataParser
			# Plotter.pcolormesh(data = PowerSpectrumCalc.shift_for_plotting(freq_amp))
			# plt.figure()
			# plt.show()

			if not in_opt.fit_lognormal_curve:
				# find radial profile
				radial_profile = PowerSpectrumCalc.calc_radial_profile(data_in = freq_amp, distance_resolution = 0.2, freq_cutoff = freq_cutoff,
																	   freq_norm = ParamsHL(np.inf, -1),
																	   fit_lognormal_curve = in_opt.fit_lognormal_curve)
			else:
				[radial_profile, radial_profile_fit, wavevector_fit] = PowerSpectrumCalc.calc_radial_profile(data_in=freq_amp, distance_resolution=0.2,
																		freq_cutoff=freq_cutoff,freq_norm=ParamsHL(np.inf, -1),
																		fit_lognormal_curve=in_opt.fit_lognormal_curve)
				radial_profile_fit.metadata.axes_labels = Vector('Radial spatial frequency (1/um)', 'Radially averaged FFT amplitude (A.U.)')
				radial_profile_fit.metadata.label = filename + ' fit'

			# set labels correctly
			radial_profile.metadata.axes_labels = Vector('Radial spatial frequency (1/um)','Radially averaged FFT amplitude (A.U.)')
			radial_profile.metadata.label = str(field_arr[ind]) + ' Oe'

			# copy to mydata
			data_list.append(deepcopy(radial_profile))

			# plot and save the comparison to fit
			if in_opt.fit_lognormal_curve:
				Plotter.plot_multiple([radial_profile, radial_profile_fit])
				# put the fitted wavevector on the figure
				text_pos_y = np.min(radial_profile_fit.data)+0.5*np.std(radial_profile_fit.data)
				plt.text(freq_cutoff.low+0.5, text_pos_y, '{:.3f}'.format(wavevector_fit), fontsize=12)
				plt.savefig(os.path.join(in_opt.output_path, filename + ' comparison to fit'), dpi=300)
				# plt.show()
				plt.close()

		# make a copy of data_list and field_arr for line plots later
		data_list_lineplot = deepcopy(data_list)
		field_arr_lineplot = deepcopy(field_arr)

		# do field pcolor plots if possible
		if len(field_arr)>1:
			#---------------- Pcolor plot ----------------
			# create zero radial_profile to fill in the gaps in B field range
			zero_radial_profile = deepcopy(radial_profile)
			zero_radial_profile.data = zero_radial_profile.data*0
			max_field = np.max(field_arr)
			min_field = np.min(field_arr)
			ave_field_step = (max_field - min_field)/(len(field_arr)-1)
			if max_field < in_opt.B_field_high:
				field_arr.append(max_field+ave_field_step)
				field_arr.append(in_opt.B_field_high)
				data_list.append(zero_radial_profile)
				data_list.append(zero_radial_profile)
			if min_field > in_opt.B_field_low:
				field_arr.append(min_field - ave_field_step)
				field_arr.append(in_opt.B_field_low)
				data_list.append(zero_radial_profile)
				data_list.append(zero_radial_profile)

			# plot combined pseudocolour plot
			combined_data = Shuju.shuju1d_list_to_2d(data_list, y_axis= field_arr)
			# cap FT amplitude at max
			# more_than_max_ind = combined_data.data > in_opt.max_FT_amp
			# combined_data.data[more_than_max_ind] = in_opt.max_FT_amp
			# combined_data.data = np.log10(combined_data.data)
			combined_data.metadata.axes_labels = Vector('Radial spatial frequency (1/um)','Magnetic field (Oe)','Radially averaged FFT amplitude (A.U.)')
			combined_data.metadata.label = in_opt.title
			Plotter.pcolormesh(data = combined_data, equal_aspect = False)
			plt.ylim(in_opt.B_field_low, in_opt.B_field_high)

			# save figure
			plt.savefig(os.path.join(in_opt.output_path, in_opt.title), dpi=300)

			#---------------- Line plots ----------------
			# plot and save line plots
			plt.figure()
			# make offsetted line plots
			amp_factor = 0.3 * (np.max(field_arr_lineplot) - np.min(field_arr_lineplot))
			for ind, tmp_data in enumerate(data_list_lineplot):
				tmp_data.arb_fn(lambda x: x * amp_factor + field_arr_lineplot[ind])
			# set title
			data_list_lineplot[-1].metadata.label = in_opt.title
			plot_togther(data_list_lineplot)
			plt.savefig(os.path.join(in_opt.output_path, in_opt.title + ' lines'), dpi=300)

			# close all figures
			plt.close('all')

# run the main function
if __name__ == '__main__':
	main()
	# test()

