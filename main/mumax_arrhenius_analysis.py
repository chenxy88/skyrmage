import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import os
import math
import re
from scipy.interpolate import interp1d
from dataclasses import dataclass
from copy import deepcopy
import json
import random as rand
import string
import subprocess

from core.data_parser import DataParser
from core.plotter import Plotter, PlotSettings
from core.util import Vector, ParamsHL, save_obj, UI_load_json_file, listdir_filtered, Timer, update_obj_from_dict_recursively
from core.shuju import Shuju
from image.fourier_analysis import PowerSpectrumCalc
from scipy.optimize import curve_fit

# material parameters
@dataclass
class MaterialParameters:
	landau_damping: float = 0.1
	mag_sat: float = 1 # magnetisation saturation in MA/m
	exchange: float = 10  # exchange in pJ/m
	dmi_bulk: float = 0 # bulk DMI mJ/m^2
	dmi_interface: float = 2 # interfacial DMI mJ/m^2
	anistropy_uni: float = 0.5  # 1st order uniaxial anisotropy constant, MJ/m^3. NOTE: This is NOT effective anistropy, which takes into account dipolar energy
	interlayer_exchange: float = 0 # interlayer exchange scaling

	def calc_effective_medium (self, prescaled_mat_params, scaling: float):
		# scale the effective medium, which includes the FM and spacer layers
		mu0 = 4e-7 * math.pi

		# no scaling
		self.landau_damping = prescaled_mat_params.landau_damping
		self.interlayer_exchange = prescaled_mat_params.interlayer_exchange

		# scaling according to Woo, S. et al. Nature Materials 15, 501 (2016).
		self.mag_sat = prescaled_mat_params.mag_sat * scaling
		self.exchange = prescaled_mat_params.exchange * scaling
		self.dmi_bulk = prescaled_mat_params.dmi_bulk * scaling
		self.dmi_interface = prescaled_mat_params.dmi_interface * scaling
		self.anistropy_uni = scaling*(prescaled_mat_params.anistropy_uni - mu0*1e6*prescaled_mat_params.mag_sat**2/2) + mu0*1e6*self.mag_sat**2/2


@dataclass
class GeometryParameter:
	z_fm_single_thickness: int = 1 # thickness in number of cells in z
	z_nm_single_thickness: int = 2 # thickness in number of cells in z
	z_layer_rep_num: int = 1 # number of repetitions
	z_single_rep_thickness: int = 3  # thickness of single repetition in number of cells in z

	z_cell_size: float = 1 # physical size of single cell in z, in nm

	phy_size: Vector = Vector(2048, 2048, 0) # in nm
	grid_cell_count: Vector = Vector(512, 512) # number of cells in simulation, should be power of 2
	pbc: Vector = Vector(0, 0, 0)

	effective_medium_scaling: float = 1 # scaling of material parameters = t_FM/t_repeat

	def calc_auto_parameters(self):
		self.z_single_rep_thickness = self.z_fm_single_thickness + self.z_nm_single_thickness
		self.grid_cell_count.z = self.z_single_rep_thickness * self.z_layer_rep_num - self.z_nm_single_thickness
		self.phy_size.z = self.grid_cell_count.z * self.z_cell_size


@dataclass
class SimulationMetadata:
	commit: str = '' # version control
	stage: int = 0
	loop: int = -1
	loop_start: int = 0
	sim_name: str = ''
	sim_name_full: str = ''
	walltime: str = '24:00:00'
	sim_id: str = ''
	output_dir: str = ''
	output_subdir: str = ''
	mumax_file: str = ''
	previous_jobid: str = ''
	config_ovf_name: str = 'config.ovf'

	sh_file: str = ''
	production_run: bool = False
	mumax_installed: bool = False
	project_code: str = 'Personal'

	def calc_auto_parameters(self):
		if self.output_dir == '':
			self.output_dir = os.path.join(os.getcwd(), self.sim_name)
		# convert to system specific paths
		# e.g. /scratch/users/astar/dsi/chenxy1/textures/mumax_sim_outputs
		self.output_dir = os.path.abspath(self.output_dir)

		self.sim_id = ''.join([rand.random.choice(string.ascii_letters+string.digits) for ch in range(8)])
		self.sim_name_full = self.sim_name + '_st%02d_%02d' % (self.stage, self.loop)+'_'+self.sim_id

		# output subdirectory contains all the results from this series of M(H)
		# e.g. /scratch/users/astar/dsi/chenxy1/textures/mumax_sim_outputs/26m1_ar_st64
		self.output_subdir = os.path.join(self.output_dir, (self.sim_name + '_st%02d' % self.stage))
		self.mumax_file = os.path.join(self.output_subdir, self.sim_name_full + '.mx3')
		self.sh_file = os.path.join(self.output_subdir, 'one_sim.sh')

		self.production_run = not os.path.isfile('./not_production_run.txt')  # this will be set automatically by checking if file not_production_run.txt exist in current dir
		status, outputstr = subprocess.getstatusoutput('mumax3')
		self.mumax_installed = status == 0

@dataclass
class TuningParameters:
	external_Bfield: float = 0
	# whether or not the first run of the M(H) loop starts with previous mag. Useful for minor loops.
	start_series_with_prev_mag: bool = False
	thermal_fluctuation: bool = False
	uniform_mag_initial: bool = True
	# whether or not previous config is copied out to common, and next run starts with prev config
	m_h_loop_run: bool = True
	temperature: float = 300
	temperature_run_time: float = 5e-10
	temperature_run_dt: float = 1e-15
	temperature_stop_mz: float = 0
	mag_autosave_period: float = 0 # zero disable autosave
	table_autosave_period: float = 1e-11 # 100 to 1000 points for 1ns to 10ns run

# all experimental parameters
@dataclass
class SimulationParameters:

	# SimulationMetadata
	sim_meta: SimulationMetadata = SimulationMetadata()
	# MaterialParameters
	mat: MaterialParameters = MaterialParameters()
	# MaterialParameters
	mat_scaled: MaterialParameters = MaterialParameters()
	# GeometryParameter
	geom: GeometryParameter = GeometryParameter()
	# TuningParameters
	tune: TuningParameters = TuningParameters()

@dataclass
class InputOptions:
	path: str = None
	title: str = ''
	results_path: str = ''
	json_file_ext: str = '.json'
	input_file_ext: str = '.txt'
	time_column_num: int = 0
	# the quantity that we are watching for crossing of a defined threshold
	quantity_column_num: int = 3
	quantity_threshold: float = 0
	# either watching for low-to-high or high-to-low crossing
	threshold_high_to_low: bool = True

def analysis_main():

	# def lognorm_fun (x, A, sigma, mu):
	# 	return A/(x*sigma*np.sqrt(2*np.pi))*np.exp(-2*((np.log(x)-mu)/sigma)**2)

	# --- CHOOSE INPUT OPTION --- #
	input_dict_list = UI_load_json_file()

	if not input_dict_list:
		# user chose to cancel
		return

	# prepare colormap
	cmap = matplotlib.cm.get_cmap('viridis')

	# --- BIG OUTSIDE LOOP THRU LIST OF INPUT JSON FILES --- #
	for input_dict in input_dict_list:

		in_opt = InputOptions()
		update_obj_from_dict_recursively(in_opt, input_dict)

		# import files based on filter
		json_list = listdir_filtered(in_opt.path, in_opt.json_file_ext)
		result_file_list = listdir_filtered(in_opt.path, in_opt.input_file_ext)

		json_list.sort()
		result_file_list.sort()
		num_files = len(result_file_list)

		# each json file must have one and only one result file
		assert (num_files == len(json_list))

		# prepare data structure to store output
		output_dict = dict()

		tmp_fig = plt.figure()
		plt.style.use(PlotSettings.style)

		# prepare color
		color_ind_arr = np.linspace(0, 1, int(len(result_file_list) *1.3))

		# read_file_timer = Timer('Read file')
		# --- loop through all files in list --- #
		for ind, filename in enumerate(result_file_list):

			# all json and result files should match
			# assert (json_list[ind].split('.')[0] == filename.split('.')[0])

			# open json files first
			with open(os.path.join(in_opt.path, json_list[ind])) as json_params_file:
				sim_params_dict = json.load(json_params_file)

			# load simulation parameters
			sim_params = SimulationParameters()
			update_obj_from_dict_recursively(sim_params, sim_params_dict)

			# load the data in
			tmp_data = DataParser.import_plaintxt(path=in_opt.path, filename=filename, delimiter='\t', skip_header=3)

			# copy out arrays for easy reference
			time_arr = tmp_data.data[:, in_opt.time_column_num]
			quantity_arr = tmp_data.data[:,in_opt.quantity_column_num]

			# plot it
			plt.plot(np.log10(time_arr), quantity_arr, color = cmap(color_ind_arr[ind]))

			# compare with threshold
			thresholded = quantity_arr < in_opt.quantity_threshold

			if thresholded[0]:
				# if the first element is true, i.e. start with true, invert
				thresholded = thresholded == False

			if not thresholded.any():
				# no threshold crossing detected
				break

			# find time of crossing of threshold
			crossing_time = time_arr[thresholded.argmax()]

			# store output in output_dict
			key = sim_params.tune.external_Bfield
			if key in output_dict:
				output_dict[key].append([sim_params.tune.temperature, crossing_time])
			else:
				output_dict[key] = [[sim_params.tune.temperature, crossing_time]]


		plt.xlabel('log10(time(s))')
		plt.ylabel('mz')
		plt.title('mz (t)')
		plt.savefig(os.path.join(in_opt.results_path, in_opt.title+' mz'), dpi=300)
		plt.close(tmp_fig)


		# end of loop of all files

		# plot and save

		# unpacks all the keys into a list
		tmp_fig = plt.figure()
		plt.style.use(PlotSettings.style)
		color_ind_arr = np.linspace(0,1,len(output_dict)+1)

		fit_results = []
		for ind,external_Bfield in enumerate([*output_dict]):
			tmp_data_arr = np.array(output_dict[external_Bfield])
			tmp_x = 1/tmp_data_arr[:,0]
			tmp_y = np.log10(tmp_data_arr[:,1])
			# use log base e for fitting
			tmp_y2 = np.log(tmp_data_arr[:,1])
			# plot the raw data
			plt.plot(tmp_x, tmp_y, '.', label=external_Bfield, color=cmap(color_ind_arr[ind]))
			# do polynominal fitting
			tmp_fit = np.polyfit(tmp_x, tmp_y2, 1)
			p = np.poly1d(tmp_fit)
			# fitted data
			fitted_x = [np.min(tmp_x), np.max(tmp_x)]
			fitted_y = np.log10(np.exp(p(fitted_x)))
			# plot in same figure the fit
			plt.plot(fitted_x, fitted_y, '-', color=cmap(color_ind_arr[ind]))
			fit_results.append(tmp_fit)

		plt.legend(loc='upper right')
		plt.xlabel('1/Temperature (1/K)')
		plt.ylabel('log10(time(s))')
		plt.title(in_opt.title)
		plt.savefig(os.path.join(in_opt.results_path, in_opt.title), dpi=300)
		plt.close(tmp_fig)

		fit_results_arr = np.array(fit_results)
		# plot the energy barrier as a function of field
		tmp_fig = plt.figure()
		plt.style.use(PlotSettings.style)
		plt.plot([*output_dict], fit_results_arr[:,0],'.-')
		plt.xlabel('B field (T)')
		plt.ylabel('Energy barrier (K)')
		plt.title(in_opt.title+' energy barrier')
		plt.savefig(os.path.join(in_opt.results_path, in_opt.title+' energy barrier'), dpi=300)
		plt.close(tmp_fig)


	# END OF BIG OUTSIDE LOOP THRU LIST OF INPUT JSON FILES


def test():
	# simple test function
	pass

# run the main function
if __name__ == '__main__':
	analysis_main()
	# test()
