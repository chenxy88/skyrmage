import numpy as np
import matplotlib.pyplot as plt
import os
import collections
from copy import copy, deepcopy
import re
from scipy.interpolate import interp1d
from dataclasses import dataclass

from core.data_parser import DataParser
from core.plotter import Plotter
from image.fourier_analysis import PowerSpectrumCalc
from image.pixel_analysis import PixelAnalysis
from core.util import Vector, ParamsHL, save_obj, UI_load_json_file
from core.shuju import Shuju

@dataclass
class InputOptions:
	path: str = None
	file_extension: str = None
	grid_spacing: float = None
	title: str = ''
	output_path: str = ''
	show_intermediate_steps: bool = False
	coercive_field: float = 0
	invert_contrast: bool = False
	divide_background_blur_radius: int = 10
	open_close_rep: int = 2
	median_filter_radius: int = 3
	local_threshold: bool = False
	manual_threshold_percentile: float = None
	global_thresh_finetune_list: list = None
	counts_threshold: int = 100
	max_domain_width: float = 300


def main():

	# --- CHOOSE INPUT OPTION --- #
	input_dict_list = UI_load_json_file()

	if not input_dict_list:
		# user chose to cancel
		return

	# --- BIG OUTSIDE LOOP THRU LIST OF INPUT JSON FILES --- #
	for input_dict in input_dict_list:

		in_opt = InputOptions(**input_dict)

		# import files based on filter
		path = in_opt.path
		filename_list = os.listdir(path)

		filename_list_filtered = []
		field_arr = []

		# filter based on criteria
		for filename in filename_list:
			if filename.endswith(in_opt.file_extension):
				filename_list_filtered.append(filename)

		filename_list_filtered.sort()
		num_files = len(filename_list_filtered)

		# measurement of domain width distribution
		domain_width_distr_arr = []
		mean_domain_width_arr = []

		# init global_thresh_finetune_list
		if in_opt.global_thresh_finetune_list is None:
			in_opt.global_thresh_finetune_list = [1] * num_files

		# --- loop through all files in list --- #
		for ind, filename in enumerate(filename_list_filtered):

			# progress
			print('Processing... %0.1f%% complete.  Loading %s.' %(float(ind/num_files)*100, filename))

			# prepare to plot
			data_to_show_2D_list = []
			linecut_figs = None
			domain_width_distr_shuju = None

			# import data
			raw_data = DataParser.import_image(path=path, filename=filename)

			field_val = int(re.search('(\+?-?\d+)\s*Oe', filename).group(1))
			field_arr.append(field_val)

			# remove extension from filename
			filename, ext = os.path.splitext(filename)

			raw_data.metadata.set_attrs(label = filename+' 00 Raw data', axes_labels = Vector('Pixels','Pixels','Intensity'))
			# data_to_show_2D_list.append(raw_data)

			flattened_data = PixelAnalysis.divide_background(raw_data, blur_radius=in_opt.divide_background_blur_radius)
			flattened_data.metadata.set_attrs(label=filename + ' 00b Flattened data', axes_labels=Vector('Pixels', 'Pixels', 'Intensity'))
			# data_to_show_2D_list.append(flattened_data)

			# XOR operation
			invert_contrast = in_opt.invert_contrast ^ (field_val < in_opt.coercive_field)
			print('invert_contrast: %r'%invert_contrast)

			thresholded_data = PixelAnalysis.skimage_filters_threshold_wrapper(flattened_data, invert=invert_contrast,
					local_threshold=in_opt.local_threshold, global_thresh_finetune = in_opt.global_thresh_finetune_list[ind])
			thresholded_data.metadata.set_attrs(label = filename+' 01 Thresholded data', axes_labels = Vector('Pixels','Pixels','Intensity'))
			data_to_show_2D_list.append(thresholded_data)

			connectedness_filtered_data = PixelAnalysis.filter_by_connectedness(thresholded_data, counts_threshold=in_opt.counts_threshold, connectivity = 1)
			connectedness_filtered_data.metadata.set_attrs(label=filename + ' 02a Connectedness-filtered data')
			data_to_show_2D_list.append(connectedness_filtered_data)
			# print('open_first = %d' % (open_first))

			opened_closed_data = PixelAnalysis.skimage_morphology_open_close_wrapper(connectedness_filtered_data, eros_dil_seq=[1, -1, -1, 1])
			opened_closed_data.metadata.set_attrs(label=filename + ' 02b Opened-closed data')
			# data_to_show_2D_list.append(opened_closed_data)

			median_filtered_data = PixelAnalysis.skimage_filter_median_wrapper(opened_closed_data, median_filter_radius = in_opt.median_filter_radius)
			median_filtered_data.metadata.set_attrs(label=filename + ' 02c Median-filtered data', axes_labels=Vector('Pixels', 'Pixels', 'Intensity'))
			data_to_show_2D_list.append(median_filtered_data)

			distance_map_data = PixelAnalysis.distance_map_wrapper(median_filtered_data)
			distance_map_data.metadata.set_attrs(label = filename+' 03 Distance map data', axes_labels = Vector('Pixels','Pixels','Intensity'))
			# data_to_show_2D_list.append(distance_map_data)

			skeletonised_data = PixelAnalysis.skimage_morphology_skeletonize_wrapper(median_filtered_data)
			skeletonised_data.metadata.set_attrs(label = filename+' 04 Skeletonised map data', axes_labels = Vector('Pixels','Pixels','Intensity'))
			# data_to_show_2D_list.append(skeletonised_data)

			domain_width_distr_shuju, mean_domain_width = PixelAnalysis.calc_average_domain_width(distance_map_data_in=distance_map_data,
														skeleton_data_in=skeletonised_data, scaling_factor=2 * in_opt.grid_spacing * 1000)
			domain_width_distr_shuju.metadata.label = filename+' 08 Domain width distribution'
			domain_width_distr_arr.append(deepcopy(domain_width_distr_shuju))
			mean_domain_width_arr.append(mean_domain_width)

			tmp_fig = plt.figure()
			Plotter.plot(data= domain_width_distr_shuju)
			plt.savefig(os.path.join(in_opt.output_path, domain_width_distr_shuju.metadata.label), dpi=300)
			plt.close(tmp_fig)

			# show intermediate steps if required
			if in_opt.show_intermediate_steps:

				# plot combined pseudocolour plot
				for data_to_show in data_to_show_2D_list:
					tmp_fig = plt.figure()
					Plotter.pcolormesh(data=data_to_show, equal_aspect=True)
					plt.savefig(os.path.join(in_opt.output_path, data_to_show.metadata.label), dpi=300)
					plt.close(tmp_fig)

				# save figures from find_linecut_profiles function
				if not linecut_figs is None:
					for (j, fig_to_save) in enumerate(linecut_figs):
						plt.figure(fig_to_save.number)
						plt.savefig(os.path.join(in_opt.output_path, filename+' linecut fig ' + str(j)), dpi=300)
						plt.close(fig_to_save)


		# plot domain distribution in series of line plots AND a combined pcolor plot
		pcolor_shuju_list = []
		y_offset = np.max(domain_width_distr_arr[0].data)/4
		tmp_fig = plt.figure()
		for (i,tmp_shuju) in enumerate(deepcopy(domain_width_distr_arr)):
			# combined pcolor plot
			tmp_shuju2 = deepcopy(tmp_shuju)
			xn = np.linspace(0, in_opt.max_domain_width, num=300, endpoint=True)
			interp_fn = interp1d(tmp_shuju2.metadata.axes, tmp_shuju2.data, bounds_error=False, fill_value=0)
			tmp_shuju2 = Shuju(interp_fn(xn))
			tmp_shuju2.metadata.axes = xn
			pcolor_shuju_list.append(tmp_shuju2)

			# line plots
			tmp_shuju.arb_fn(lambda y:y+i*y_offset)
			tmp_shuju.metadata.label = str(field_arr[i])+' Oe'
			Plotter.plot(tmp_shuju)

		# line plots
		plt.title('Domain width distribution')
		plt.legend(loc='upper right')
		plt.savefig(os.path.join(in_opt.output_path, in_opt.title+' Domain distribution'), dpi=300)
		plt.close(tmp_fig)

		#pcolor plot
		combined_data = Shuju.shuju1d_list_to_2d(pcolor_shuju_list, y_axis=field_arr)
		combined_data.metadata.axes_labels = Vector('Domain distribution (nm)', 'Magnetic field (Oe)', 'Distribution amplitude (A.U.)')
		combined_data.metadata.label = 'Domain width distribution'
		f2 = plt.figure()
		Plotter.pcolormesh(data=combined_data, equal_aspect=False)
		plt.savefig(os.path.join(in_opt.output_path, in_opt.title+' Domain distribution pcolor'), dpi=300)
		plt.close(f2)


		# plot average domain size
		ave_domain_width = Shuju(mean_domain_width_arr)
		ave_domain_width.metadata.label = 'Mean domain width'
		ave_domain_width.metadata.axes = field_arr
		ave_domain_width.metadata.axes_labels = Vector('Field (Oe)','Mean domain width (nm)')
		tmp_fig = plt.figure()
		Plotter.plot(ave_domain_width)
		plt.savefig(os.path.join(in_opt.output_path, in_opt.title+' Mean domain distribution'), dpi=300)
		plt.close(tmp_fig)

		# save important shuju objects
		save_obj(domain_width_distr_arr, os.path.join(in_opt.output_path, in_opt.title+' domain_width_distr_arr'))
		save_obj(ave_domain_width, os.path.join(in_opt.output_path, in_opt.title+' mean_domain_width'))

		plt.close('all')

# run the main function
if __name__ == '__main__':
	main()
	# test()
