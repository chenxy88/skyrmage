import numpy as np
import matplotlib.pyplot as plt
import os
import math
import re
from scipy.interpolate import interp1d
from dataclasses import dataclass
from copy import deepcopy

from core.data_parser import DataParser
from core.plotter import Plotter
from image.pixel_analysis import PixelAnalysis, LineCut
from core.util import Vector, ParamsHL, save_obj, UI_load_json_file, Timer
from core.shuju import Shuju
from core.stats import Stats

@dataclass
class InputOptions:

	# file input parameters
	path: str = None  # path of input data
	intermediate_output_path: str = None # output path of intermediate output like thresholded image
	results_path: str = None # output path of averaged linecuts
	file_extension: str = None # file extension to scan for input path
	angle_re_string: str = 'alpha'

	# physical parameters
	grid_spacing: float = None # pixel size in um
	tilt_axis_angle: float = 0 # angle of the tilt axis w.r.t the horizontal

	# output parameters
	title: str = '' # title on output plots
	linecut_stack_offset: float = 3 # num of stddev offset for linecut stack
	angle_sweep: bool = True
	field_sweep: bool = False
	pcolor_plot: bool = False
	output_data_filename: str = ''

	# flow control parameters
	do_analysis: bool = True
	test_image_proc_only: bool = False # do not do linecut yet
	show_intermediate_steps: bool = False # output intermediate results like thresholded image
	determine_linecut_from_first_image: bool = True # find the positions of linecuts from the first image and use for the rest

	# image processing parameters
	invert_contrast: bool = False
	divide_background_blur_radius: int = 30
	threshold_radius: int = 51
	global_thresh_finetune: float = 0
	median_filter_radius: int = 4
	local_threshold: bool = False
	counts_threshold: [int] = np.array([100])
	connectivity: int = 1
	segment_size_threshold: int = 9
	threshold_segment_len: int = 0
	pcolor_plot_threshold: float = 0.2 # max diff from 1

	# line cut parameters
	linecut_len_px: int = 3
	spacing_btw_linecuts: float = 5


	# unused
	# pos_field_invert_contrast: bool = True
	# open_close_rep: int = 2
	# max_domain_width: float = 700
	# manual_threshold_percentile: float = None


def linecut_binning_n_averaging(linecut_list: [LineCut], in_opt: InputOptions): #type hinting - this is very useful
	"""
	sort, bin and average linecuts
	:param linecut_list:
	:return:
	"""
	# linecuts has been sorted in ascending order of length
	# linecut_lengths_unique, counts = np.unique(linecut_lengths, return_counts=True)

	def bin_helper (angle,	num_angle_bins, angle_start = -90.0, angle_end = 90.0):
		angle_step = (angle_end - angle_start)/num_angle_bins
		bin_index = int((angle-angle_start)/angle_step)
		# bin_centre = angle_start+angle_step*bin_index+angle_step/2
		return bin_index

	shuju_LOL = [] #list of list
	averaged_shuju = []
	angle_list = []
	lengths_list = []

	num_angle_bins = 9
	delta = 0.001
	angle_start = 0
	angle_end = 90
	angle_step = (angle_end - angle_start) / num_angle_bins
	bin_centres = np.round(np.linspace(angle_start+angle_step/2, angle_end-angle_step/2, num_angle_bins))

	# shuju_LOL = [[]]*num_angle_bins does not work because the other list is filled with repetitions of the same object []
	# appending to one will append to all
	for i in range(0,num_angle_bins):
		shuju_LOL.append([])

	# convert to shuju objects list
	for tmp_linecut in linecut_list:
		#convert_wrt_tilt_axis(tmp_linecut, in_opt.tilt_axis_angle)
		angle_list.append(tmp_linecut.angle)
		lengths_list.append(tmp_linecut.length)

		# tmp_profile, len_arr = ensure_right_heavy(tmp_linecut.profile)
		tmp_shuju = Shuju(tmp_linecut.profile)
		tmp_shuju.metadata.axes = np.arange(0,tmp_linecut.length)-tmp_linecut.length/2+0.5
		bin_index = bin_helper(tmp_linecut.angle, num_angle_bins, angle_start - delta, angle_end + delta)
		shuju_LOL[bin_index].append(tmp_shuju)

	max_linecut_len = np.max(lengths_list)
	new_x = np.arange(0,max_linecut_len)-max_linecut_len/2+0.5

	for (ind,shuju_list) in enumerate(shuju_LOL):
		if not shuju_list is []:
			tmp_shuju = Shuju.shuju1d_list_averaged(shuju_list, new_x)
			tmp_shuju.metadata.misc_dict.update({'angle_bin_centre': bin_centres[ind]})
			averaged_shuju.append(tmp_shuju)

	angle_distr_shuju = Stats.kde_wrapper(np.array(angle_list), np.linspace(angle_start, angle_end, 91), bw_method='scott')

	return averaged_shuju, angle_distr_shuju


def analysis_main():

	# --- CHOOSE INPUT OPTION --- #
	input_dict_list = UI_load_json_file()

	# assess performance
	timer = Timer()

	if not input_dict_list:
		# user chose to cancel
		return

	# --- BIG OUTSIDE LOOP THRU LIST OF INPUT JSON FILES --- #
	for input_dict in input_dict_list:

		in_opt = InputOptions(**input_dict)

		# input assertions
		assert (in_opt.tilt_axis_angle >= 0 and in_opt.tilt_axis_angle < 180)

		# convert from um per pixel to nm per pixel, which is a more convenient unit in this analysis
		in_opt.grid_spacing *= 1000

		# import files based on filter
		path = in_opt.path
		filename_list = os.listdir(path)

		filename_list_filtered = []
		field_arr = []
		angle_arr = []
		pcolor_x_axis_label = 'Distance (nm)'
		pcolor_y_axis = None
		pcolor_y_axis_label = ''

		# filter based on criteria
		for filename in filename_list:
			if filename.endswith(in_opt.file_extension):
				filename_list_filtered.append(filename)

		filename_list_filtered.sort()
		num_files = len(filename_list_filtered)

		# Combined figure of all the files, one most perpendicular binned linecut from each
		# combined_perp_linecuts = plt.figure()
		perp_angle_data_previous = None
		perp_angle_distr_shuju_list_displaced = []
		perp_angle_distr_shuju_list_original = []
		lc_positions = None
		lc_gradients = None

		pcolor_y_axis_label = ''
		if in_opt.angle_sweep:
			pcolor_y_axis_label = 'Tilt angle (deg)'
		elif in_opt.field_sweep:
			pcolor_y_axis_label = 'Field (mT)'

		plt.style.use(r'C:\Users\Xiaoye\OneDrive\ASTAR-Skyrmions\Projects\skyrmage\misc\default.mplstyle')

		# --- loop through all files in list --- #
		if in_opt.do_analysis:
			for ind, filename in enumerate(filename_list_filtered):

				# progress
				print('Processing... %0.1f%% complete.  Loading %s.' %(float(ind/num_files)*100, filename))

				# prepare to plot
				data_to_show_2D_list = []

				# import data
				raw_data = DataParser.import_image(path=path, filename=filename, grid_spacing=in_opt.grid_spacing)

				# field_val = int(re.search('(\+?-?\d+)\s*Oe', filename).group(1))
				field_arr.append(int(re.search('(\+?-?\d+)\s*mT', filename).group(1)))
				angle_arr.append(int(re.search(in_opt.angle_re_string+'\s*(\+?-?\d+)', filename).group(1)))

				# remove extension from filename
				filename, ext = os.path.splitext(filename)

				raw_data.metadata.set_attrs(label = filename+' 00 Raw data', axes_labels = Vector('Distance (mm)','Distance (mm)','Intensity'))
				# data_to_show_2D_list.append(raw_data)

				timer.display_time_reset('Imported data')

				flattened_data = PixelAnalysis.divide_background(raw_data, blur_radius=in_opt.divide_background_blur_radius)
				flattened_data.metadata.set_attrs(label=filename + ' 00b Flattened data', axes_labels=Vector('Pixels', 'Pixels', 'Intensity'))

				# --- image processing to determine linecut position and length --- #
				if not in_opt.determine_linecut_from_first_image or ind == 0:

					data_to_show_2D_list.append(deepcopy(flattened_data))

					thresholded_data = PixelAnalysis.skimage_filters_threshold_wrapper(flattened_data, invert= in_opt.invert_contrast,
															r=in_opt.threshold_radius, local_threshold=in_opt.local_threshold,
																					   global_thresh_finetune = in_opt.global_thresh_finetune)
					# thresholded_data = PixelAnalysis.skimage_filters_trinary_threshold(flattened_data, invert=in_opt.invert_contrast,
					# 																   global_thresh_finetune=in_opt.global_thresh_finetune)
					thresholded_data.metadata.set_attrs(label = filename+' 01 Thresholded data')
					data_to_show_2D_list.append(deepcopy(thresholded_data))

					# this process filters by connectivity, that closes gaps between neighbouring regions iteratively
					for ind_counts, counts_threshold_step in enumerate(in_opt.counts_threshold):

						connectedness_filtered_data = PixelAnalysis.filter_by_connectedness(thresholded_data, counts_threshold = counts_threshold_step,
																							connectivity = in_opt.connectivity)
						connectedness_filtered_data.metadata.set_attrs(label = filename+' 02 filtered data '+str(ind_counts)+' a')
						data_to_show_2D_list.append(deepcopy(connectedness_filtered_data))

						open_degree = ind_counts+1
						opened_closed_data = PixelAnalysis.skimage_morphology_open_close_wrapper(connectedness_filtered_data, eros_dil_seq= [1]*open_degree+[-1]*open_degree)
						opened_closed_data.metadata.set_attrs(label=filename + ' 02 filtered data '+str(ind_counts)+' b')
						data_to_show_2D_list.append(deepcopy(opened_closed_data))

						# update thresholded_data after each round of filter and growth
						thresholded_data = opened_closed_data

					if in_opt.median_filter_radius > 0:
						median_filtered_data = PixelAnalysis.skimage_filter_median_wrapper(opened_closed_data, median_filter_radius= in_opt.median_filter_radius)
						median_filtered_data.metadata.set_attrs(label=filename + ' 03a Median-filtered data')
						data_to_show_2D_list.append(median_filtered_data)
					else:
						median_filtered_data = opened_closed_data

					distance_map_data = PixelAnalysis.distance_map_wrapper(median_filtered_data)
					distance_map_data.metadata.set_attrs(label = filename+' 03b Distance map data')
					# data_to_show_2D_list.append(distance_map_data)

					skeletonised_data = PixelAnalysis.skimage_morphology_skeletonize_wrapper(median_filtered_data)
					skeletonised_data.metadata.set_attrs(label = filename+' 04 Skeletonised map data')
					# data_to_show_2D_list.append(skeletonised_data)

					removed_connected_data = PixelAnalysis.remove_multiconnected_pixels(skeletonised_data)
					removed_connected_data.metadata.set_attrs(label = filename+' 05 Removed connected map data')
					# data_to_show_2D_list.append(removed_connected_data)

					labelled_segments_data = PixelAnalysis.skimage_measure_label_wrapper(removed_connected_data)
					labelled_segments_data.metadata.set_attrs(label = filename+' 06 Labelled segments map data')
					# data_to_show_2D_list.append(labelled_segments_data)

					trimmed_segments_data, labels, counts = PixelAnalysis.remove_small_segments(data_in = labelled_segments_data, segment_size_threshold = in_opt.segment_size_threshold)
					trimmed_segments_data.metadata.set_attrs(label = filename+' 07 Trimmed segments map data')
					# data_to_show_2D_list.append(trimmed_segments_data)

				timer.display_time_reset('Processed image')

				# --- take linecuts from original, unprocessed image --- #
				if not in_opt.test_image_proc_only:

					linecut_list, lc_positions, lc_gradients = PixelAnalysis.find_linecut_profiles(
						original_data_in = flattened_data, distance_map_data_in = distance_map_data,
						labelled_segments_data_in = trimmed_segments_data,
						labels = labels, linecut_len_px=in_opt.linecut_len_px,
						tilt_axis_angle=in_opt.tilt_axis_angle,
						lc_positions = lc_positions, lc_gradients = lc_gradients,
						show_intermediate_steps=in_opt.show_intermediate_steps,
						spacing_btw_linecuts = in_opt.spacing_btw_linecuts,
						threshold_segment_len= in_opt.threshold_segment_len,
						savefig_path=os.path.join(in_opt.intermediate_output_path, filename+' linecut fig'),
						scale=Vector(in_opt.grid_spacing,in_opt.grid_spacing))

					if not in_opt.determine_linecut_from_first_image:
						lc_positions = None
						lc_gradients = None

					timer.display_time_reset('Found linecuts')

					# save averaged linecut
					averaged_linecut, angle_distr_shuju = linecut_binning_n_averaging(linecut_list, in_opt)

					# convert from pixels to physical units
					for tmp_shuju in averaged_linecut:
						tmp_shuju.metadata.axes *= in_opt.grid_spacing  # in um per pixel

					# plot combined_perp_linecuts
					perp_angle_distr_shuju: Shuju
					# copy the first averaged linecut profile, which is closest to the tilt axis
					perp_angle_distr_shuju = deepcopy(averaged_linecut[0])

					# set label according to what kind of sweep it is
					if in_opt.angle_sweep:
						perp_angle_distr_shuju.metadata.label = str(angle_arr[ind])#+'deg'
						pcolor_y_axis = angle_arr
					elif in_opt.field_sweep:
						perp_angle_distr_shuju.metadata.label = str(field_arr[ind])+'mT'
						pcolor_y_axis = field_arr

					perp_angle_distr_shuju_list_original.append(deepcopy(perp_angle_distr_shuju))

					# do y offset
					perp_angle_distr_shuju.data -= np.mean(perp_angle_distr_shuju.data)
					# check that there is no overlap between the previous and current plot
					if not perp_angle_data_previous is None:
						tmp_diff = perp_angle_data_previous - perp_angle_distr_shuju.data
						tmp_max_diff = np.max(tmp_diff)
						perp_angle_distr_shuju.data += tmp_max_diff + in_opt.linecut_stack_offset*np.std(perp_angle_distr_shuju.data)

					perp_angle_distr_shuju_list_displaced.append(perp_angle_distr_shuju)
					perp_angle_data_previous = perp_angle_distr_shuju.data

					# all binned averaged linecuts
					offset_factor = 2*np.std(averaged_linecut[0].data)

					# plot line cuts for all linecut angles
					for (ind,tmp_shuju) in enumerate(averaged_linecut):
						angle_bin_centre = tmp_shuju.metadata.misc_dict['angle_bin_centre']
						tmp_shuju.metadata.label = str(angle_bin_centre)
						tmp_shuju.metadata.axes_labels = Vector('Distance (nm)','Image intensity (A.U.)')

					# plot binned and averaged linecuts according to linecut angle (defined from tilt axis angle)
					Plotter.plot_multiple(averaged_linecut, title=filename + ' averaged linecut', offset=offset_factor)
					plt.savefig(os.path.join(in_opt.results_path, in_opt.title + ' ' + filename + ' averaged linecut'), dpi=300)

					# plot and save figures
					Plotter.plot_n_save(angle_distr_shuju, os.path.join(in_opt.results_path, in_opt.title + ' '+filename + ' angle distribution'))

				# show intermediate steps if required
				if in_opt.show_intermediate_steps:

					# plot image-processed pseudocolour plots
					for data_to_show in data_to_show_2D_list:
						Plotter.saveasimage(data=data_to_show, pathnfilename=os.path.join(in_opt.intermediate_output_path , data_to_show.metadata.label)+'.tif')
						#tmp_fig = plt.figure()
						#Plotter.pcolormesh(data=data_to_show, equal_aspect=True)
						#plt.savefig(os.path.join(in_opt.intermediate_output_path , data_to_show.metadata.label), dpi = 300)
						#plt.close(tmp_fig)

				timer.display_time_reset('Plotted and saved output')
				plt.close('all')

			# save data
			final_data = None
			for row in perp_angle_distr_shuju_list_original:

				if final_data is None:
					final_data = row.data
					continue

				final_data = np.vstack([final_data, row.data])

			final_data_output = deepcopy(final_data)
			# append the x axes to the 0th row
			pcolor_x_axis = perp_angle_distr_shuju_list_original[0].metadata.axes
			final_data_output = np.vstack([pcolor_x_axis, final_data_output])
			# append the y axes to the 0th col
			pcolor_y_axis_output = np.array([0]+pcolor_y_axis)
			final_data_output = np.hstack([pcolor_y_axis_output[:,None], final_data_output])

			np.savetxt(os.path.join(in_opt.results_path, in_opt.output_data_filename), final_data_output, fmt='%.10g', delimiter='\t', header='')

		else:
			# simply load data
			final_data = np.genfromtxt(os.path.join(in_opt.results_path, in_opt.output_data_filename), delimiter='\t', skip_header=0, comments='#', invalid_raise=False)
			pcolor_x_axis = final_data[0,1:]
			pcolor_y_axis = final_data[1:,0]
			final_data = final_data[1:,1:]

		# end of loop of all files
		# plot perp_angle_distr_shuju_list
		# do not leave figure open for too long, plot, save and close



		### DO NOT DELETE THE FOLLOWING. IMPORTANT ###

		# tmp_fig = plt.figure()
		# Plotter.plot_multiple(perp_angle_distr_shuju_list_displaced)
		#
		# plt.title('Combined perpendicular linecuts')
		# plt.xlabel('Distance (nm)')
		# plt.ylabel('Image intensity (A.U.)')
		# plt.legend(loc='upper right')
		# plt.tight_layout()
		# plt.savefig(os.path.join(in_opt.results_path, 'Combined perpendicular linecuts'), dpi=300)
		# plt.close(tmp_fig)

		# plot pcolor plot if needed
		if not in_opt.test_image_proc_only and in_opt.pcolor_plot:

			# processing to enhance contrast
			# final_data = np.log10(final_data)
			final_data = final_data**4
			final_data [final_data<(1-in_opt.pcolor_plot_threshold)] = 1-in_opt.pcolor_plot_threshold
			final_data [final_data>(1+in_opt.pcolor_plot_threshold)] = 1+in_opt.pcolor_plot_threshold

			mesh_x, mesh_y = np.meshgrid(Plotter.shift_axis_for_pcolor(pcolor_x_axis), Plotter.shift_axis_for_pcolor(pcolor_y_axis))

			# combined_data = Shuju.shuju1d_list_to_2d(perp_angle_distr_shuju_list_original, y_axis=pcolor_y_axis)

			# set labels and plot
			# combined_data.metadata.axes_labels = Vector('Distance (nm)', pcolor_y_axis_label, 'Image intensity (A.U.)')

			tmp_fig = plt.figure(figsize=(8,8))
			plt.pcolormesh(mesh_x, mesh_y, final_data, cmap='viridis')
			plt.xlabel(pcolor_x_axis_label)
			plt.ylabel(pcolor_y_axis_label)

			# Plotter.pcolormesh(data=combined_data, equal_aspect=False)

			# save figure
			plt.savefig(os.path.join(in_opt.results_path, 'Combined perpendicular linecuts pcolor.jpg'), dpi=300)
			plt.close(tmp_fig)

			# save objects
			# save_obj(combined_data, os.path.join(in_opt.results_path, in_opt.title + ' Combined perpendicular linecuts'))


def test():
	# simple test function
	reps = 100000
	len_arr = 200
	arr = np.zeros(len_arr)

# run the main function
if __name__ == '__main__':
	analysis_main()
	# test()
