from copy import copy, deepcopy
import numpy as np
from scipy import signal
from scipy.optimize import curve_fit

from core.plotter import Plotter
from core.shuju import Shuju, MetaData
from core.util import Vector, ParamsHL

# DOING: modifying the classes to contain only static utility functions, like data_parser
class PowerSpectrumCalc:
	"""
		data: 2d ndarray to fourier transform
		grid_spacing_x: real space distance in x between data points, in um
		grid_spacing_y: real space distance in y between data points, in um
		tilt_angle: angle of tilt, in degrees. Tilt axis is assumed to be the x-axis. Correction to grid_spacing_y.
	"""
	def __init__(self, tilt_angle, freq_min=-1, freq_max=float('inf'), zero_cross_len_scale=None):
		# assertions
		assert isinstance(tilt_angle, (float, int))
		assert isinstance(freq_min, (int, float))
		assert isinstance(freq_max, (int, float))

		# convert degrees to radians
		self.tilt_angle = tilt_angle / 180 * np.pi
		# correction factor for the tilt compression

		self.freq_min = freq_min
		self.freq_max = freq_max
		self.zero_cross_len_scale = zero_cross_len_scale

		# variables to be filled in later
		self.grid_spacing = None

	@staticmethod
	def calc_power_spectrum(data_in = None, grid_spacing = None, tilt_angle = 0., freq_remove= -1.):
		"""
		data: 2d ndarray to fourier transform
		grid_spacing_x: real space distance in x between data points, in um
		grid_spacing_y: real space distance in y between data points, in um
		tilt_angle: angle of tilt, in degrees. Tilt axis is assumed to be the x-axis. Correction to grid_spacing_y.
		freq_remove: remove horizontal and vertical bands about zero wavevector
		"""
		# check if data_in is in the correct form
		assert isinstance(data_in, Shuju)
		assert isinstance(grid_spacing, Vector)

		# spatial frequency, in units of um^-1. Last input is spatial spacing in units of um
		freq = Vector()
		freq.y = np.fft.fftfreq(data_in.data.shape[0], grid_spacing.y)  # spatial frequency, in units of um^-1
		freq.x = np.fft.fftfreq(data_in.data.shape[1], grid_spacing.x)  # invert one of the axis to get consistency with real space rotations

		fft_data = np.fft.fft2(data_in.data)
		# fft_data = np.fft.fftshift(fft_data)  # Shift the zero-frequency component to the center of the spectrum.

		# make meshgrid
		freq_mesh = Vector()
		freq_mesh.x, freq_mesh.y = np.meshgrid(freq.x, freq.y)

		# remove horizontal and vertical bands about zero wavevector
		ind_to_discard = (np.abs(freq_mesh.x) < freq_remove) | (np.abs(freq_mesh.y) < freq_remove)

		amp_spec = np.abs(fft_data)

		amp_spec[ind_to_discard] = 0

		# write to amp_spec_out
		amp_spec_out = Shuju(amp_spec, MetaData())
		amp_spec_out.metadata.set_attrs(axes=freq_mesh, label='Amplitude spectrum', unit='A.U.', axes_meta=Vector(MetaData(), MetaData()))
		amp_spec_out.metadata.axes_meta.x.set_attrs(label='Spatial frequency', unit='1/um')
		amp_spec_out.metadata.axes_meta.y.set_attrs(label='Spatial frequency', unit='1/um')


		# write to fft_data_out
		fft_data_out = Shuju(fft_data, MetaData())
		fft_data_out.metadata.set_attrs(axes=freq_mesh, label='Complex spectrum', unit='A.U.', axes_meta=Vector(
			MetaData(), MetaData()))
		fft_data_out.metadata.axes_meta.x.set_attrs(label='Spatial frequency', unit='1/um')
		fft_data_out.metadata.axes_meta.y.set_attrs(label='Spatial frequency', unit='1/um')

		return amp_spec_out, fft_data_out

	@staticmethod
	def calc_radial_profile(data_in = None, distance_resolution=0.2, freq_cutoff = ParamsHL(float('inf'), -1), freq_norm = None, bin_resolution_boost = 5,
							fit_lognormal_curve = False):
		"""
		Calculate radial profile
		:return: r, radial_profile wrapped in Shuju object
		"""

		def lognormal_fun(x, A, sigma, mu):
			# lognormal function
			return A / (x * sigma * np.sqrt(2 * np.pi)) * np.exp(-2 * ((np.log(x) - mu) / sigma) ** 2)

		def normal_w_offset (x, amp, center, width, offset):
			return amp*np.exp(-(x-center)**2/(2*width**2)) + offset

		normal_w_offset_bounds = ([0, 0, 0, -np.inf], [np.inf]*4)

		# for fitting if required
		fitting_fun = normal_w_offset
		fitting_bounds = normal_w_offset_bounds

		# check if data_in type is correct
		assert isinstance(data_in, Shuju)
		freq_mesh = data_in.metadata.axes
		power_spec = data_in.data

		# plt.pcolor(freq_in.x, freq_in.y, np.log10(power_spec))
		# plt.colorbar()
		# plt.show()

		# scipy.stats.gaussian_kde would not work since this is NOT a histogram

		# first start with several times the desired resolution, then smooth with desired resolution

		bin_resolution = distance_resolution/bin_resolution_boost

		r = freq_mesh.distance()
		r = r / bin_resolution #distance_resolution
		r = r.astype(np.int)

		# number of data_in points with distance = r, weighted by intensity of data_in
		tbin = np.bincount(r.ravel(), power_spec.ravel())
		# normalisation factor
		nr = np.bincount(r.ravel())
		r_bins = np.arange(np.max(r) + 1) * bin_resolution #distance_resolution

		# remove nr==0 elements
		indices_to_keep = (nr != 0)
		tbin = tbin[indices_to_keep]
		nr = nr[indices_to_keep]
		r_bins = r_bins[indices_to_keep]

		radialprofile = tbin / nr  # average data_in 'intensity' at a particular distance from the centre

		# smooth data to desired resolution
		radialprofile = signal.savgol_filter(radialprofile, bin_resolution_boost, 3)

		# implement cutoff
		radialprofile = radialprofile[(r_bins > freq_cutoff.low) & (r_bins < freq_cutoff.high)]
		radialprofile = radialprofile / np.max(radialprofile)
		r_bins = r_bins[(r_bins > freq_cutoff.low) & (r_bins < freq_cutoff.high)]

		# normalise by dividing by region for normalisation
		if freq_norm:
			norm_factor = np.mean(radialprofile[(r_bins > freq_norm.low) & (r_bins < freq_norm.high)])
			radialprofile = radialprofile / norm_factor

		data_out = Shuju(radialprofile, MetaData())
		data_out.metadata.set_attrs(axes=r_bins, label='Radially averaged amplitude spectrum', unit='A.U.',
									axes_meta=Vector(MetaData(), MetaData()))
		data_out.metadata.axes_meta.x.set_attrs(label='Radial spatial frequency', unit='1/um')
		data_out.metadata.axes_meta.y.set_attrs(label='Amplitude spectrum', unit='A.U.')

		if not fit_lognormal_curve:
			return data_out

		else:
			initial_params = [np.max(radialprofile), r_bins[np.argmax(radialprofile)], 1, 0]
			popt, pcov = curve_fit(fitting_fun, r_bins, radialprofile, p0=initial_params, bounds= fitting_bounds)
			# find the result of the fit
			r_bin_dense = np.linspace(np.min(r_bins), np.max(r_bins), len(r_bins)*5)
			fun_fit = fitting_fun(r_bin_dense, *popt)
			# find the wavevector at the max probability density
			wavevector_fit = r_bin_dense[np.argmax(fun_fit)]

			data_out_fit = Shuju(fun_fit, MetaData())
			data_out_fit.metadata.set_attrs(axes=r_bin_dense, label='Radially averaged amplitude spectrum fit', unit='A.U.',
										axes_meta=Vector(MetaData(), MetaData()))
			data_out_fit.metadata.axes_meta.x.set_attrs(label='Radial spatial frequency', unit='1/um')
			data_out_fit.metadata.axes_meta.y.set_attrs(label='Amplitude spectrum', unit='A.U.')

			return [data_out, data_out_fit, wavevector_fit]

	@staticmethod
	def shift_for_plotting(data_in = None):
		"""
		Do fftshifts to FFT of image to plot nicely

		data_out = shift_for_plotting(data_in)
		"""
		data_in_copy = deepcopy(data_in)
		data_out = data_in_copy
		data_out.metadata.axes = Vector(np.fft.fftshift(data_in_copy.metadata.axes.x), np.fft.fftshift(data_in_copy.metadata.axes.y))
		data_out.data = np.fft.fftshift(data_in_copy.data)

		return data_out

	def count_zero_crossing(self, data_in, testing_mode=False):
		"""
		Count the number of horizontal zero crossings, after slight smoothing
		:param data_in:
		:return: Number of horizontal zero crossings
		"""

		# check if data_in is in the correct form
		assert isinstance(data_in, Shuju)
		assert self.zero_cross_len_scale is not None

		# first, subtract mean
		data = data_in.data[0]
		data = data - np.mean(data)
		nrow,ncol = np.shape(data)
		window_len = int(self.zero_cross_len_scale/self.grid_spacing.x)
		if window_len < 1:
			window_len = 1
		window = signal.hann(window_len)
		num_zero_cross = 0

		if testing_mode:
			vlim_fn = lambda data: ParamsHL(high=np.mean(data.data[0]) + 2 * np.std(data.data[0], ddof=1),low=np.mean(data.data[0]) - 2 * np.std(data.data[0], ddof=1))
			plotter_2d = Plotter(title='original', cmap='bwr', vlim_fn=vlim_fn)
			plotter_2d.pcolormesh(Shuju([data]))

		# loop through every row of data
		for i in range(0, nrow):
			data_row = data[i,:]
			filtered_row = signal.convolve(data_row, window, mode='same')
			# count number of zero crossings
			num_zero_cross = num_zero_cross+((filtered_row[:-1] * filtered_row[1:]) < 0).sum()

		# 	average number of crossings per row, cast in the correct units e.g. crossings/um
		num_zero_cross = num_zero_cross/(nrow*ncol)/self.grid_spacing.x

		return num_zero_cross


	def fft_filter(self, fft_data_in, freq_min, freq_max, action_retain = True):
		"""
		Filter a real space image by removing certain spatial frequencies
		:param freq_in: Spatial frequencies
		:param fft_data_in: FFT amplitudes
		:param freq_min:
		:param freq_max:
		:param action_retain: if True, only retain segment within freq_min/max. If not, only remove said segment.
		:return: Filtered image
		"""

		# check if data_in is in the correct form
		assert isinstance(fft_data_in, Shuju)

		freq_mesh = fft_data_in.metadata.axes
		fft_data = copy.copy(fft_data_in.data)

		r = freq_mesh.distance(Vector(0, 0))
		ind_to_remove = (r < freq_min) | (r > freq_max)

		if not action_retain:
			ind_to_remove = np.logical_not(ind_to_remove)

		fft_data_out = fft_data.copy()
		# remove FT amp outside defined filter
		fft_data_out[ind_to_remove] = 0
		filtered_data_out = np.fft.ifft2(fft_data_out)

		# write to filtered_data_out
		fft_data_out = Shuju(fft_data_out, MetaData())
		filtered_data_out.metadata = copy.copy(fft_data_in.metadata)

		# write to filtered_data_out
		filtered_data_out = Shuju(filtered_data_out, MetaData())
		filtered_data_out.metadata.set_attrs(label='Filtered image', unit='A.U.',
											 axes_meta=Vector(MetaData(), MetaData()))
		filtered_data_out.metadata.axes_meta.x.set_attrs(label='Pixel')
		filtered_data_out.metadata.axes_meta.y.set_attrs(label='Pixel')

		return filtered_data_out, fft_data_out
