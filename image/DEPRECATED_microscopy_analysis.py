# This code is a huge mess, most DO NOT USE DIRECTLY. There are certain useful bits that can be copied out, though.

import os as os
import re
import copy
import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d

from core.shuju import MetaImage, Shuju
from core.data_parser import ImageParser
from core.plotter import Plotter2D
from image.fourier_analysis import RadialProfileCalc
from core.util import Vector, ParamsHL, meshgrid_list


		# add another one as a function of crop
		# plotter_radial_profile2 = Plotter1D(title='Radial profile', axes_label=Params2d('Spatial frequency (1/um)', 'Power spectrum (A.U.)'), legend=True, axeslim=Params2d())

# def mumax_sim():
# 	preproc = lambda  data: np.reshape(data[:, 2], (512, 512))
# 	root_path = 'C:\\Users\\Xiaoye\\Tmp\\mumax_results'
# 	all_data_files = os.listdir(root_path)
# 	for data_file in all_data_files:
# 		# setup DataParser
# 		metafile = MetaFile()
# 		metafile.set_attrs(filename = data_file, path = root_path, delimiter=' ', processing_fn = preproc)
# 		data_parser = DataParser(metafile)
#
# 		metadata = MetaData()
# 		metadata.set_attrs(label='mumax', unit='A.U.')
#
# 		# load data
# 		data_tmp = data_parser.load_data(metadata)
# 		data_tmp.generate_axes(Vector(4 / 1000, 4 / 1000))
# 		calc_obj = RadialProfileCalc(tilt_angle=0, freq_min=freq_min, freq_max=freq_max, distance_resolution=0.2)
# 		data_tmp, amp = calc_obj.calc_power_spectrum(data_tmp)
# 		data_tmp = calc_obj.calc_radial_profile(data_tmp)
# 		plotter_radial_profile.plot(data_tmp, label='mumax')
#
# def ltem_sim():
# 	root_path = 'C:\\Users\\Xiaoye\\Tmp\\MALTS_results'
# 	all_data_files = os.listdir(root_path)
# 	for data_file in all_data_files:
# 		tilt_ang = int(re.search('(\d+) tilt', data_file).group(1))
# 		defocus_val = int(re.search('(\+*-*\d+)um', data_file).group(1))
# 		metafile = MetaFile()
# 		metafile.set_attrs(filename=data_file, path=root_path, delimiter=',')
# 		data_parser = DataParser(metafile)
# 		data_tmp = data_parser.load_data()
# 		data_tmp.generate_axes(Vector(4 / 1000, 4 / 1000))
# 		calc_obj = RadialProfileCalc(tilt_angle=tilt_ang, freq_min=freq_min, freq_max=freq_max)
# 		data_tmp, amp = calc_obj.calc_power_spectrum(data_tmp)
# 		# plot_2D_fft(data_tmp.data[0], data_tmp.data[1])
# 		data_tmp = calc_obj.calc_radial_profile(data_tmp)
# 		plotter_radial_profile.plot(data_tmp, label='MALTS defocus= ' + str(defocus_val) + ' um')

class AnalyseLTEMExp:
	def __init__(self):
		self.root_path = None
		self.file_names = None
		self.magn_defocus = None
		self.tilt_axis_dict = None
		self.radial_profile_arr_len = None
		self.freq_max_min = None
		self.plotter_radial_profile = None
		self.abs_defocus_short_list = None
		
		self.flow_ctrl = None
		
	def analyse(self):
		root_path = self.root_path
		magn_defocus = self.magn_defocus
		tilt_axis_dict = self.tilt_axis_dict
		radial_profile_arr_len = self.radial_profile_arr_len
		freq_max_min = self.freq_max_min
		plotter_radial_profile = self.plotter_radial_profile
		abs_defocus_short_list = self.abs_defocus_short_list
		r_basis = np.linspace(freq_max_min.low + 0.5, freq_max_min.high - 0.5, radial_profile_arr_len)
	
		if self.flow_ctrl['plot_magnification']:
			x = np.zeros(len(magn_defocus))
			y = np.zeros(len(magn_defocus))
			n = 0
			for i in magn_defocus:
				x[n] = float(i)
				y[n] = magn_defocus[i]
				n = n+1
			plt.figure()
			plt.plot(x,y,'.-')
			plt.xlabel('Defocus (um)')
			plt.ylabel('Magnification (um/px)')

		# open only tif files
		all_data_files = [file for file in os.listdir(root_path) if file.endswith('.tif')]
	
		if self.flow_ctrl['single_test_file']:
			all_data_files = self.file_names
			# all_data_files = ['02 -150um 0 tilt-calibrated with 0.002523.tif']
	
		# abs_defocus_short_list = [50, 100, 150, 200]
		defocus_short_list = abs_defocus_short_list+ list((-1*np.array(abs_defocus_short_list)))
		defocus_short_list.sort()
	
		# dimensions for array of subplots
		subplot_dim = Vector(len(defocus_short_list), 7)
	
		defocus_ind_dict = dict(zip(defocus_short_list,range(0, subplot_dim.x)))
		# counts the number of plots with that defocus
		defocus_ind_counter_dict = dict(zip(defocus_short_list, [0]*subplot_dim.x))
	
	
		# for radial plot
		abs_defocus_ind_counter_dict = dict(zip(abs_defocus_short_list,[0]*len(abs_defocus_short_list)))
		r_profile_ave = dict(zip(abs_defocus_short_list,[np.zeros(radial_profile_arr_len)]*len(abs_defocus_short_list)))
		r_profile_save = []
		if self.flow_ctrl['plot_all_files']:
			plotter_radial_profile_all = copy.copy(plotter_radial_profile)
		if self.flow_ctrl['fft_subplot']:
			plotter_2d_fft_subplotter = Plotter2D(axeslim=Vector(ParamsHL(freq_max_min.high, -freq_max_min.high), ParamsHL(freq_max_min.high, -freq_max_min.high)), fig_dim=Vector(4 * subplot_dim.x, 4 * subplot_dim.y))
			plotter_2d_fft_subplotter.subplot_init(num_plot = subplot_dim)
	
		if self.flow_ctrl['real_space_subplot']:
			vlim_fn = lambda data: ParamsHL(high=np.mean(data.data[0]) + 2 * np.std(data.data[0], ddof=1),low=np.mean(data.data[0]) - 2 * np.std(data.data[0], ddof=1))
			plotter_2d_real_space_subplotter = Plotter2D(cmap='bwr', vlim_fn=vlim_fn, fig_dim = Vector(4 * subplot_dim.x, 4 * subplot_dim.y))
			plotter_2d_real_space_subplotter.subplot_init(num_plot = subplot_dim)
	
		# loop through all the params to vary
		# lets vary crop factor
		# crop_arr = list(np.arange(1,0.4,-0.25))
		crop_arr = [1]
		# additional radial plot as function of changing crop
		r_profile_ave_crop = dict(zip(crop_arr, [np.zeros(radial_profile_arr_len)] * len(crop_arr)))

		###---THE BIG LOOP---###
		params_mesh = meshgrid_list(all_data_files,crop_arr)
		for params in params_mesh:
			data_file = params[0]
			tilt_ang = int(re.search('(\d+) tilt', data_file).group(1))
			defocus_val = int(re.search('(\+*-*\d+)um', data_file).group(1))
			rotation_axis_str = re.search('um (a*b*)', data_file).group(1)
	
			if ((tilt_ang == 0) and (not self.flow_ctrl['accept_zero_tilt'])) or defocus_val not in defocus_short_list:
			# if tilt_ang != 0 or np.abs(defocus_val) not in abs_defocus_short_list:
				continue
	
			#### set the plotting
			ax_ind = Vector(defocus_ind_dict[defocus_val], defocus_ind_counter_dict[defocus_val])
			# increment defocus plot counter
			defocus_ind_counter_dict[defocus_val] = defocus_ind_counter_dict[defocus_val] + 1
	
			if self.flow_ctrl['exclude_tilt']:
				tilt_ang=0
	
			tilt_axis_angle = tilt_axis_dict[defocus_val]
	
			if rotation_axis_str == 'a':
				#angle for beta axis, rotate by 90 for alpha axis
				tilt_axis_angle = tilt_axis_angle+90
	
			# load and process the image
			magn = magn_defocus[defocus_val]
			# crop from centre
			crop_frac = None
			# zero padding to improve reciprocal space resolution. A fraction as well.
			zero_padding = None
	
			if self.flow_ctrl['imag_crop']:
				crop_frac = Vector(params[1], params[1])
	
			metaimage = MetaImage()
			metaimage.set_attrs(path=root_path, filename=data_file, rotation_angle = tilt_axis_angle, crop_frac=crop_frac, zero_padding=zero_padding)
			data_parser = ImageParser(metaimage)
			data_tmp = data_parser.load_data() #TODO: add x and y axis
			data_tmp.generate_axes(Vector(magn, magn))
	
			if self.flow_ctrl['export_image']:
				data_parser.metafile.set_attrs(processing_fn=lambda x:x-np.mean(x), export_format = 'spip-asc')
				data_parser.export_data(data_tmp)
	
			if self.flow_ctrl['real_space_subplot']:
				# set color lims to be 2 std from the mean
				plotter_2d_real_space_subplotter.title = data_file
				plotter_2d_real_space_subplotter.subplot_set_axes(ax_ind)
				plotter_2d_real_space_subplotter.pcolormesh(data_tmp)
	
			if self.flow_ctrl['real_space_plot']:
				# set color lims to be 2 std from the mean
				vlim_fn = lambda data : ParamsHL(high=np.mean(data.data[0])+2*np.std(data.data,ddof=1), low=np.mean(data.data)-2*np.std(data.data,ddof=1))
				plotter_2d = Plotter2D(title='original', cmap='bwr', vlim_fn=vlim_fn)
				plotter_2d.pcolormesh(data_tmp)
	
			print('filename: %s, tilt: %d, defocus: %d'%(data_file, tilt_ang, defocus_val))
	
			calc_obj = RadialProfileCalc(tilt_angle=tilt_ang, freq_min=freq_max_min.low, freq_max=freq_max_min.high, distance_resolution=0.2)
	
			### DO FFT
			if self.flow_ctrl['do_fft']:
				data_tmp, amp = calc_obj.calc_power_spectrum(data_tmp)
	
			if self.flow_ctrl['fft_filter']:
				data_filtered, amp_filtered = calc_obj.fft_filter(amp, 7.5, 8.15, action_retain=True)
				data_filtered.arb_fn(lambda d: [np.abs(d[0])])
				plotter_2d = Plotter2D(title='filtered_7.5_8.15', cmap='Greys')
				plotter_2d.pcolormesh(data_filtered)
	
			if self.flow_ctrl['fft_plot']:
				plotter_2d_fft = Plotter2D(title=data_file)
				data_to_plot = Shuju([np.fft.fftshift(data_tmp.data[0].x), np.fft.fftshift(data_tmp.data[0].y), np.log10(np.abs(np.fft.fftshift(data_tmp.data[1])))])
				plotter_2d_fft.pcolormesh(data_to_plot)
	
			if self.flow_ctrl['fft_subplot']:
				# plot_2D_fft(data_tmp.data[0],data_tmp.data[1], freq_max, axes[ax_ind.x, ax_ind.y],subplot_title=data_file)
				plotter_2d_fft_subplotter.title = data_file
				plotter_2d_fft_subplotter.subplot_set_axes(ax_ind)
				# plotter_2d = Plotter2D(axes=axes[ax_ind.x, ax_ind.y], title=data_file, axeslim= Params2d(ParamsHL(freq_max,-freq_max),ParamsHL(freq_max,-freq_max)))
				data_to_plot = Shuju([np.fft.fftshift(data_tmp.data[0].x), np.fft.fftshift(data_tmp.data[0].y), np.log10(np.abs(np.fft.fftshift(data_tmp.data[1])))])
				plotter_2d_fft_subplotter.pcolormesh(data_to_plot)
	
			if self.flow_ctrl['radial_plot']:
				### calculate radial profile
				data_tmp = calc_obj.calc_radial_profile(data_tmp)
				if self.flow_ctrl['plot_all_files']:
					plotter_radial_profile_all.plot(data_tmp)
				# save r_profile based on its defocus
				interp_fn = interp1d(data_tmp.metadata.axes, data_tmp.data, kind='cubic')
				r_profile_interp = interp_fn(r_basis)
				r_profile_save.append(r_profile_interp)
				r_profile_ave[np.abs(defocus_val)] = r_profile_ave[np.abs(defocus_val)]+ r_profile_interp
				r_profile_ave_crop[params[1]] = r_profile_ave_crop[params[1]]+r_profile_interp
				# record the number of files per defocus
				abs_defocus_ind_counter_dict[np.abs(defocus_val)] = abs_defocus_ind_counter_dict[np.abs(defocus_val)]+1
				# plt.plot(*calc_obj.calc(), label=data_file[i])
	
		if self.flow_ctrl['fft_subplot']:
			plotter_2d_fft_subplotter.subplot_axes_labels(Vector('Spatial frequency (1/um)', 'Spatial frequency (1/um)'))
			# plotter_2d_fft_subplotter.save('C:\\Users\\Xiaoye\\Tmp\\Skyrmage_test\\output.png')
	
		if self.flow_ctrl['real_space_subplot']:
			plotter_2d_real_space_subplotter.subplot_axes_labels(Vector('Pixels', 'Pixels'))
			# plotter_2d_real_space_subplotter.save('C:\\Users\\Xiaoye\\Tmp\\Skyrmage_test\\output.png')
	
		### Plot radial profile
		if self.flow_ctrl['radial_plot']:
			for i in r_profile_ave:
				r_profile_ave[i] = r_profile_ave[i] / abs_defocus_ind_counter_dict[i]
				print('%d files at defocus of %s um' %(abs_defocus_ind_counter_dict[i], i))
				tmp_shuju = Shuju(data=r_profile_ave[i])
				tmp_shuju.metadata.set_attrs(axes=r_basis)
				plotter_radial_profile.plot(tmp_shuju, label='LTEM defocus='+str(i)+' um')
			for i in crop_arr:
				r_profile_ave_crop[i] = r_profile_ave_crop[i]/np.max(r_profile_ave_crop[i])
			# plotter_radial_profile2.plot(Shuju([r_basis, r_profile_ave_crop[i]]), label='crop='+str(i))

# def mfm_exp():
#
# 	all_root_paths = ['C:\\Users\\Xiaoye\\Tmp\\MFM\\20171130_13d_n4k', 'C:\\Users\\Xiaoye\\Tmp\\MFM\\171215_13d']
# 	all_data_files = []
# 	for root_path in all_root_paths:
# 		tmp_files = os.listdir(root_path)
# 		# add txt files
# 		for file in tmp_files:
# 			if file.endswith('.txt'):
# 				all_data_files.append(os.path.join(root_path, file))
#
# 	for data_file in all_data_files:
# 		metafile = MetaFile()
# 		metafile.set_attrs(path_and_filename=data_file, delimiter='	',  comments='#')
# 		data_parser = DataParser(metafile)
# 		data_tmp = data_parser.load_data()
# 		data_tmp.generate_axes(Vector(5 / 256, 5 / 256))
# 		calc_obj = RadialProfileCalc(tilt_angle=0, freq_min=freq_min, freq_max=freq_max, zero_cross_len_scale=0.01)
#
# 		data_tmp, amp = calc_obj.calc_power_spectrum(data_tmp)
# 		data_tmp = calc_obj.calc_radial_profile(data_tmp)
# 		interp_fn = interp1d(data_tmp.metadata.axes, data_tmp.data, kind='cubic')
# 		tmp_shuju = Shuju(data=interp_fn(r_basis))
# 		tmp_shuju.metadata.set_attrs(axes=r_basis)
# 		plotter_radial_profile.plot(tmp_shuju, label='MFM '+ os.path.basename(data_file))
# 		# plotter_radial_profile2.plot(Shuju([r_basis, interp_fn(r_basis)]), label='MFM')
