import numpy as np
cimport numpy as np
cimport cython

DTYPE = np.int64
ctypedef np.int64_t DTYPE_t

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def filter_by_labels(np.ndarray[DTYPE_t, ndim=2] labelled_data, np.ndarray[DTYPE_t, ndim=2] data_out_arr, np.ndarray[DTYPE_t, ndim=1] labels_to_keep, np.ndarray[DTYPE_t, ndim=1] label_lookup):
	#assert labelled_data.dtype == DTYPE and data_out_arr.dtype == DTYPE and labels_to_keep.dtype == DTYPE
	cdef int label_ind
	cdef int nrows = labelled_data.shape[0]
	cdef int ncols = labelled_data.shape[1]
	cdef int nlabels = labels_to_keep.shape[0]
	
	#build label_lookup table, came as all zeros
	for label_ind in range(nlabels):
		label_lookup[labels_to_keep[label_ind]]=1
	
	#scan through image, pick up labels to keep using label_lookup
	for row in range(nrows):
		for col in range(ncols):
			data_out_arr[row, col] = label_lookup[labelled_data[row, col]]

	return data_out_arr