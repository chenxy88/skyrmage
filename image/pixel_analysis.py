import numpy as np
import matplotlib.pyplot as plt
from copy import copy, deepcopy
from skimage.measure import label, profile_line
from skimage.filters import threshold_otsu, threshold_local, threshold_niblack, median, rank
from skimage.morphology import binary_erosion, binary_dilation, skeletonize, disk, selem
from scipy.ndimage.morphology import distance_transform_edt
from scipy.ndimage.filters import gaussian_filter
from scipy.interpolate import splprep, splev, interp2d
from scipy.stats import gaussian_kde
from dataclasses import dataclass
import math

from core.stats import Stats
from core.util import Vector, ParamsHL, Timer
from core.shuju import Shuju, MetaData
from core.plotter import Plotter
from image.pixel_analysis_cythonized import filter_by_labels

# this class contains a collection of utilities involving pixel level analysis

@dataclass
class LineCut:
	profile: np.ndarray=None
	angle: float=0
	length: float=0


class PixelAnalysis:
	@staticmethod
	def skimage_filters_threshold_wrapper(data_in=None, r = 51, invert = False, local_threshold = True, global_thresh_finetune = 0, manual_threshold_percentile = None):
		"""
		Wrapper function around skimage.filters.threshold_otsu
		"""
		assert(isinstance(data_in, Shuju))

		data_out = deepcopy(data_in)
		data_arr = data_out.data

		# do inversion this way so that increasing global_thresh_finetune always results in less foreground
		if invert:
			global_thresh_finetune = global_thresh_finetune*-1

		# thresh = threshold_otsu(data_arr)
		# thresh = threshold_local(data_arr, r, 10) # terrible, overemphasis the cluster of broke pixels
		# thresh = threshold_niblack(data_arr, window_size=r) # ok, works well for minority foreground, but not for majority foreground.
		if local_threshold:
			thresh = rank.otsu(data_arr, disk(r))
		else:
			# global threshold, much much faster
			if not manual_threshold_percentile is None:
				# manual threshold defined as percentile, use that
				thresh = np.percentile(data_arr, manual_threshold_percentile)
			else:
				thresh = threshold_otsu(data_arr)

		#apply global_thresh_finetune
		thresh = thresh*(1+global_thresh_finetune)

		# foreground is 1, background is 0
		if invert:
			tmp_data = (data_arr <= thresh).astype(int)
		else:
			tmp_data = (data_arr >= thresh).astype(int)


		data_out.data = tmp_data

		return data_out

	@staticmethod
	def skimage_filters_trinary_threshold(data_in=None, invert = False, global_thresh_finetune = 0,):

		assert(isinstance(data_in, Shuju))

		data_out = deepcopy(data_in)
		data_arr = data_out.data

		thresh_high = threshold_otsu(data_arr)
		thresh_low = -threshold_otsu(-data_arr)

		#apply global_thresh_finetune
		thresh_high = thresh_high*(1+global_thresh_finetune)
		thresh_low = thresh_low*(1-global_thresh_finetune)

		# set > thresh_high as 1, < thresh_low as -1, in between as 0
		tmp_data = (data_arr > thresh_high).astype(int)
		tmp_data[data_arr < thresh_low] = -1

		if invert:
			tmp_data = -tmp_data

		data_out.data = tmp_data

		return data_out

	@staticmethod
	def divide_background(data_in = None, blur_radius = 30):
		"""
		Create a low frequency background image by a gaussian filter with radius = blur_radius, then remove the background by division.
		:param data_in:
		:param blur_radius:
		:return:
		"""

		assert (isinstance(data_in, Shuju))

		data_out = deepcopy(data_in)
		data_arr = data_out.data

		# this helps when there are values very close to zero
		data_arr = data_arr + np.mean(data_arr)

		background = gaussian_filter(deepcopy(data_arr), blur_radius)

		data_out.data = data_arr/background

		return data_out

	# IDEA: np.multiply connected regions with the original image, sum it up and get a total integrated intensity, which can be another quantity for filtering
	@staticmethod
	def filter_by_connectedness(data_in=None, counts_threshold = 100, connectivity = 1, highpass = True, delete_zero = True, invert = False):
		"""
		Threshold the data by the total amount of connected pixels, which is called counts
		"""

		# TIMER
		# t1 = Timer('filter_by_connectedness')

		assert (isinstance(data_in, Shuju))

		data_out = deepcopy(data_in)
		data_arr = data_out.data

		if invert:
			data_arr = 1-data_arr

		labelled_data, max_label = label(data_arr, background = 0, return_num= True, connectivity=connectivity)

		# t1.display_time_reset('label')

		labels, counts = np.unique(labelled_data, return_counts=True)

		# t1.display_time_reset('unique')

		# filtering > or < counts_threshold
		if highpass:
			ind_to_keep = counts > counts_threshold
		else:
			ind_to_keep = counts < counts_threshold

		if delete_zero:
			# always delete [0] element, cos it is the background
			ind_to_keep[0]=0

		labels_to_keep = labels[ind_to_keep]

		data_out_arr = np.zeros(np.shape(labelled_data))

		label_lookup = np.zeros([max_label + 1], dtype='int64')

		# convert all to int64 type
		labelled_data = labelled_data.astype('int64')
		data_out_arr = data_out_arr.astype('int64')
		labels_to_keep = labels_to_keep.astype('int64')

		# t1.display_time_reset('convert')

		# call the cythonized function
		data_out_arr = filter_by_labels(labelled_data, data_out_arr, labels_to_keep, label_lookup)
		data_out.data = data_out_arr.astype(int)

		# t1.display_time_reset('filter_by_labels')

		if invert:
			data_out.data = 1-data_out.data

		return data_out

	@staticmethod
	def skimage_morphology_open_close_wrapper(data_in=None, close_first = True, open_close_rep = 2, eros_dil_seq = None):
		"""
		Wrapper function around skimage.morphology.binary_erosion/dilation
		"""
		assert(isinstance(data_in, Shuju))

		data_out = deepcopy(data_in)
		data_arr = data_out.data

		# TODO: Explore greyscale erosion/dilation

		# -1: erosion, 1: dilation
		# open = [-1,1]
		# close = [1,-1]
		# erosion removes foreground (1's), dilation increases foreground
		# erosion_dilation_sequence = open*2+close*2
		if close_first:
			erosion_dilation_sequence = [1] * open_close_rep + [-1] * (open_close_rep*2) + [1] * open_close_rep
		else:
			erosion_dilation_sequence = [-1] * open_close_rep + [1] * (open_close_rep*2) + [-1] * open_close_rep

		# if input provide direct sequence
		if not eros_dil_seq is None:
			erosion_dilation_sequence = eros_dil_seq
		# erosion_dilation_sequence = [-1,1,1,-1]+[-1] * 2 + [1] * 4 + [-1] * 2

		for action in erosion_dilation_sequence:
			if action>0:
				data_arr = binary_dilation(data_arr)
			else:
				data_arr = binary_erosion(data_arr)

		# invert if necessary
		# if invert:
		# 	data_arr = data_arr == 0

		# for i in range(0, reps):
		# 	data_arr = binary_erosion(data_arr)
		#
		# for i in range(0, reps*2):
		# 	data_arr = binary_dilation(data_arr)
		#
		# for i in range(0, reps):
		# 	data_arr = binary_erosion(data_arr)

		data_out.data = data_arr.astype(int)

		return data_out

	@staticmethod
	def skimage_filter_median_wrapper(data_in=None, median_filter_radius = 4, reps = 2):
		"""
		Wrapper function around skimage.filter.median
		"""
		assert (isinstance(data_in, Shuju))

		data_out = deepcopy(data_in)
		data_arr = data_out.data.astype(np.uint8)
		for i in range(0, reps):
			data_arr = median(data_arr, selem=disk(radius=median_filter_radius))

		data_out.data = (data_arr>0).astype(int)

		return data_out

	@staticmethod
	def skimage_morphology_skeletonize_wrapper(data_in=None):
		"""
		Wrapper function around skimage.morphology.binary_erosion/dilation
		"""
		assert(isinstance(data_in, Shuju))

		data_out = deepcopy(data_in)
		data_arr = data_out.data
		
		data_arr = skeletonize(data_arr)
		data_out.data = data_arr.astype(int)

		return data_out

	@staticmethod
	def distance_map_wrapper(data_in=None):
		"""
		Wrapper function around scipy.ndimage.morphology.distance_transform_edt
		"""
		assert(isinstance(data_in, Shuju))

		data_out = deepcopy(data_in)
		data_arr = data_out.data
		
		data_arr = distance_transform_edt(data_arr)
		data_out.data = data_arr

		return data_out

	@staticmethod
	def remove_multiconnected_pixels(data_in=None, max_neighbours = 2):
		"""
		Remove pixels that have more than max_neighbours number of neighbours, defined by 8-connectivity
		Background = 0
		Foreground is != 0
		"""
		assert(isinstance(data_in, Shuju))

		data_out = deepcopy(data_in)
		data_arr = data_out.data

		# binarise the data as 0 and 1
		data_bin = (data_arr!=0).astype(int)
		nrow, ncol = data_bin.shape

		for i in range(1, nrow-1):
			for j in range(1, ncol-1):
				#if pixel is filled
				if data_bin[i,j]:
					# the pixel and neighbour of concern
					nine_matrix = data_bin[i-1:i+2,j-1:j+2]
					
					# too many neighbours, set pixel of concern to zero
					if np.sum(nine_matrix) > max_neighbours+1:
						# data_bin[i-1:i+2,j-1:j+2] = 0
						data_bin[i, j] = 0
		
		data_out.data = np.multiply(data_arr, data_bin)
		# data_out.data = data_bin
		# return the result
		return data_out

	@staticmethod
	def skimage_measure_label_wrapper(data_in=None):
		"""
		Wrapper function around skimage.measure.label()
		"""
		assert(isinstance(data_in, Shuju))

		data_out = deepcopy(data_in)
		data_arr = data_out.data
		
		data_out.data = label(data_arr, background = 0, return_num= False, connectivity=2)

		return data_out

	@staticmethod
	def remove_small_segments(data_in = None, segment_size_threshold = 9):
		"""
		Remove segments that are smaller than the threshold size
		"""
		assert(isinstance(data_in, Shuju))

		data_out = deepcopy(data_in)
		data_arr = data_out.data
		
		labels, counts = np.unique(data_arr, return_counts=True)

		ind_to_delete = counts < segment_size_threshold
		ind_to_keep = counts >= segment_size_threshold
		labels_to_delete = labels[ind_to_delete]

		for label in labels_to_delete:
			data_arr[data_arr == label] = 0

		# delete label 0
		ind_to_keep[0]= False

		# update labels and counts
		labels = labels[ind_to_keep]
		counts = counts[ind_to_keep]

		data_out.data = data_arr

		return data_out, labels, counts



	@staticmethod
	def find_linecut_profiles(original_data_in = None, distance_map_data_in = None, labelled_segments_data_in = None,
							  labels = None,
							  spacing_btw_linecuts = 5, threshold_segment_len=0, linecut_len_px=100, linecut_len_demo_px = 5,
							  tilt_axis_angle=0,
							  lc_positions = None, lc_gradients = None,  # option to take in known linecut position and gradient
							  show_intermediate_steps= False, savefig_path='', scale = None):
		""""
		This generates linecut profiles, for segments larger than the threshold size
		"""

		# NESTED FUNCTIONS
		def sort_to_form_line(unsorted_list):
			"""
			Given a list of neighbouring points which forms a line, but in random order, sort them to the correct order
			IMPORTANT: Each point must be a neighbour (8-connected sense) to a least one other point!
			"""

			def are_neighbours(pt1, pt2):
				"""
				Check if pt1 and pt2 are neighbours, in the 8-connected sense
				pt1 and pt2 has integer coordinates
				"""
				return (np.abs(pt1[0] - pt2[0]) < 2) and (np.abs(pt1[1] - pt2[1]) < 2)

			sorted_list = [unsorted_list.pop(0)]

			while len(unsorted_list) > 0:
				i = 0
				while i < len(unsorted_list):
					if are_neighbours(sorted_list[0], unsorted_list[i]):
						# neighbours with front of list
						sorted_list.insert(0, unsorted_list.pop(i))
					elif are_neighbours(sorted_list[-1], unsorted_list[i]):
						# neighbours with rear of list
						sorted_list.append(unsorted_list.pop(i))
					else:
						i = i + 1

			return sorted_list

		def linecut_start_endpoints(position, gradient, length, tilt_axis_angle, max_limits = Vector(float('inf'),float('inf'))):
			"""
			Given the position, gradient and other details of the stripe domain, give the start and end points of the linecut

			For length > 0,  linecuts are always (from startpt -> endpt) in the direction of positive x.
			For vertical linecuts, it is in the direction of positive y.
			These directions could be reversed from negative length.
			"""

			if gradient.y == 0: # gradient = 0, linecut is vertical
				angle = 90
				delta = Vector(0, length/2)
			else:
				# this is defined to find the gradient of the linecut from gradient of stripe
				grad = -gradient.x/gradient.y
				# range of angle is 90 to -90
				angle = math.degrees(math.atan(grad))
				g2 = grad**2
				deltax = length/2/np.sqrt(1+g2)
				delta = Vector(deltax, deltax*grad)

			# ensure delta.x > 0
			# if delta.x <0:
			# 	delta = delta*-1

			startpt = position-delta
			endpt = position+delta

			# redefine linecut angle w.r.t the tilt axis
			# IMPT: tilt_axis_angle should be defined between 0 and 180
			angle = angle - tilt_axis_angle

			if angle < -90.0:
				angle += 180
				# flip the linecut
				# there should be mirror symmetry about tilt axis, to perpendicular to tilt axis, it does not
				# matter which direction to do linecut
				tmp_pt = startpt
				startpt = endpt
				endpt = tmp_pt

			origin = Vector(0,0)

			# check for out of bounds
			if Vector.any_component_greater(startpt, max_limits) or Vector.any_component_greater(origin, startpt) or Vector.any_component_greater(endpt, max_limits) or Vector.any_component_greater(origin, endpt):
				# this linecut exceeds the bounds
				return None, None
			else:
				return (startpt, endpt), angle

		assert(isinstance(original_data_in, Shuju))
		# assert(isinstance(distance_map_data_in, Shuju))
		assert(isinstance(labelled_segments_data_in, Shuju))

		segments_data = labelled_segments_data_in.data
		len_y, len_x = segments_data.shape

		# if linecut positions and gradients are already known, just use them
		if lc_positions is None or lc_gradients is None:

			mesh_x, mesh_y = np.meshgrid(np.arange(0,len_x), np.arange(0,len_y))

			lc_positions = []
			lc_gradients = []

			# loop through all segments to sort into lines, fit spline and find perpendicular direction
			for (ind,label) in enumerate(labels):
				# for testing
				# label = labels_to_linecut[78]

				# get the coordinates of each segment
				tmp_segment = segments_data == label
				coord_x = mesh_x[tmp_segment]
				coord_y = mesh_y[tmp_segment]

				# sort coordinates to form a line
				sorted_list = sort_to_form_line(np.rot90(np.array([coord_x, coord_y])).tolist())
				tmp_list = np.rot90(np.array(sorted_list),3)
				coord_x = tmp_list[0]
				coord_y = tmp_list[1]

				len_tmp_segment = len(coord_x)

				# number of linecuts to take
				num_linecuts = np.floor(len_tmp_segment/spacing_btw_linecuts)
				if num_linecuts <= 0 or len_tmp_segment < threshold_segment_len:
					# len_tmp_segment less than threshold requirement, do not proceed
					continue

				unew, step = np.linspace(0,1,num_linecuts, endpoint=False, retstep= True)
				if (np.isnan(step)):
					#true for num_linecuts, should get unew=0.5
					step=1
				unew = unew+step/2

				# prepare for spline interpolation to get the slope, smoothing depends on the spacing between linecuts
				tck, u = splprep([coord_x, coord_y], s=spacing_btw_linecuts*2)

				# evaluate interpolation
				lc_positions.append(splev(unew, tck))
				# check if any gradients are 0, inf or nan (for horizontal or vertical lines)
				lc_gradients.append(splev(unew, tck, der=1))

			# to check
			#tmp_data = (labelled_segments_data_in.data>0).astype(int)
			#Plotter.pcolormesh(data = original_data_in, vlim = ParamsHL(89,118), equal_aspect = False)

			# Get from distance map, the length of the linecuts
			distance_map = distance_map_data_in.data #interp2d(mesh_x, mesh_y, distance_map_data_in.data)
			linecut_lengths = []

		linecut_list = []
		orig_img = original_data_in.data
		startpt_endpt_list_demo = []

		# loop through all segments to get line profile
		for (ind,pos) in enumerate(lc_positions):
			tmp_x = pos[0]
			tmp_y = pos[1]
			grad = lc_gradients[ind]

			# loop through all linecut points in a particular segment
			# find coord of start and end points, or None it is out of bounds
			for i in range(0,len(tmp_x)):
				# take the reciprocal to find the perpendicular direction to the slope
				# linecut_len_float = linecut_len_px * distance_map[np.round(tmp_y[i]).astype(int), np.round(tmp_x[i]).astype(int)]
				linecut_len_float = linecut_len_px # use a constant length linecut
				startpt_endpt, angle = linecut_start_endpoints(Vector(tmp_x[i], tmp_y[i]), Vector(grad[0][i], grad[1][i]),
															   linecut_len_float,
															   tilt_axis_angle=tilt_axis_angle,
															   max_limits=Vector(len_x, len_y))
				# shorter linecuts for illustration purposes
				startpt_endpt_demo, tmp = linecut_start_endpoints(Vector(tmp_x[i], tmp_y[i]), Vector(grad[0][i], grad[1][i]),
																  linecut_len_demo_px,
																  tilt_axis_angle=tilt_axis_angle,
																  max_limits=Vector(len_x, len_y))

				# if it is not out of bounds
				if not startpt_endpt is None:
					# save this linecut
					tmp_lc = LineCut()
					startpt_endpt_list_demo.append(startpt_endpt_demo)
					tmp_lc.profile = profile_line(orig_img, (startpt_endpt[0].y, startpt_endpt[0].x), (startpt_endpt[1].y, startpt_endpt[1].x),
											   linewidth=spacing_btw_linecuts)
					tmp_lc.length = len(tmp_lc.profile)
					# range of angle is 90 to -90
					tmp_lc.angle = angle
					# linecut_len_int = len(tmp_profile)
					# linecut_lengths.append(linecut_len_int)
					linecut_list.append(tmp_lc)

		# show positions of linecuts if needed
		if show_intermediate_steps:
			assert (isinstance(scale, Vector))
			copy_original_data_in = deepcopy(original_data_in)
			copy_original_data_in.metadata.label = 'Linecut view'
			tmp_fig = plt.figure()
			# ax = plt.Axes(tmp_fig, [0., 0., 1., 1.])
			# ax.set_axis_off()
			# tmp_fig.add_axes(ax)

			Plotter.pcolormesh(data=copy_original_data_in, equal_aspect=True, image_only=True)
			for startpt_endpt in startpt_endpt_list_demo:
				# plt.plot([startpt_endpt[0].x*scale.x, startpt_endpt[1].x*scale.x], [startpt_endpt[0].y*scale.y, startpt_endpt[1].y*scale.y],
				# 		 'r-', linewidth = 1)
				#plt.plot([startpt_endpt[0].x, startpt_endpt[1].x], [startpt_endpt[0].y, startpt_endpt[1].y], 'r-', linewidth=0.5)
				dx = startpt_endpt[1].x - startpt_endpt[0].x
				dy = startpt_endpt[1].y - startpt_endpt[0].y
				plt.arrow(startpt_endpt[0].x, startpt_endpt[0].y, dx, dy, ec='r', width=0.01)

			plt.axis('off')
			tmp_fig.axes[0].get_xaxis().set_visible(False)
			tmp_fig.axes[0].get_yaxis().set_visible(False)
			plt.savefig(savefig_path, dpi=300, bbox_inches='tight', pad_inches=0)
			plt.close(tmp_fig)

		# average all linecuts of the same length
		# sort_ind = sorted(range(len(linecut_lengths)), key = lambda k:linecut_lengths[k])
		# linecut_lengths = np.array(linecut_lengths)
		# linecut_list = np.array(linecut_list)
		# linecut_lengths = linecut_lengths[sort_ind]
		# linecut_list = linecut_list[sort_ind]

		return linecut_list, lc_positions, lc_gradients

	@staticmethod
	def calc_average_domain_width(distance_map_data_in = None, skeleton_data_in = None, scaling_factor = 2):
		"""
		Calculate the average domain width, given the positions of the domain cores and the distance map.
		:param distance_map_data_in:
		:param skeleton_data_in:
		:param scaling_factor:
		:return: average domain width
		"""

		assert (isinstance(distance_map_data_in, Shuju))
		assert (isinstance(skeleton_data_in, Shuju))

		domain_core_map_bool = skeleton_data_in.data > 0
		domain_core_map = domain_core_map_bool.astype(int)

		# adjustment_factor is 2 by default since the distance map only measure half the domain width
		core_width_map = np.multiply(distance_map_data_in.data, domain_core_map)

		# pick out only non-zero values
		core_width_map = core_width_map[domain_core_map_bool]
		domain_width_arr = scaling_factor * core_width_map
		mean_domain_width = np.mean(domain_width_arr)

		# check with histogram
		# domain_width_plot = plt.figure()
		# plt.hist(domain_width_arr, bins='auto')

		domain_width_x = np.linspace(0, max(domain_width_arr), 100)
		domain_width_distr_shuju = Stats.kde_wrapper(domain_width_arr, domain_width_x, bw_method='silverman')
		domain_width_distr_shuju.metadata.set_attrs(label='Domain width distribution')
		domain_width_distr_shuju.metadata.axes_labels = Vector('Domain width (nm)', 'Distribution amplitude (A.U.)')

		# Plotter.plot(domain_width_distr_shuju)
		# plt.show()

		return domain_width_distr_shuju, mean_domain_width




