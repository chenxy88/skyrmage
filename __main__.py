# This file will aid with distributing the skyrmage library. Simply zip up the entire library, edit this file to point to the main function to run,
# and send the zip file to other people. They can run the code by running: python skyrmage.zip
#
# For reference:
# http://blog.ablepear.com/2012/10/bundling-python-files-into-stand-alone.html

from main.import_n_plot_many_files import plot_main

def main():
	plot_main()

if __name__ == '__main__':
	main()